classdef MonteCarloSimulation < handle
    % optimize with stochastic forces
    % use as theory: crude monte-carlo simulation
    
    properties
        % structure
        s
        % objective function
        f0
        % stochastic force-objectList
        sF
        % number of simulations
        sn
        % objective function of simulations
        sf0
        % mean
        mean
        % standard deviation
        sigma
        % 5./95. Percentile
        percentile
    end
    
    methods
        
        function setStructure(this, strct)
            % insert the structure-object
            this.s = strct;
        end
        
        function simulate(this, n)
            this.sn = n;
            this.sF = this.s.neumannList;
            this.f0 = this.s.solve();
            % numbero f force Objects
            nForces = max(size(this.sF));
            % generate random variables for two possible directions per
            % force
            p = rand(2*nForces, this.sn);
            % run two slopes
            for i = 1:this.sn
                cntF = 1;
                for k = 1:nForces
                    % current force object
                    f = this.sF{k};
                    val = f.getValues();
                    % init forceVector
                    q = [0, 0];
                    for l = 1:2
                        % check if force direction is not equal to zero
                        if val(l)
                            q(l) = p(cntF, i);
                            cntF = cntF + 1;
                        end
                    end
                    f.setValues(q);
                    this.s.setUP();
                end
                % objective function
                this.sf0(i) = this.s.solve();
            end
        end
        
        function p = plotHistogram(this)
            this.getData();
            p = figure;
            X = this.sf0;
            sig = this.sigma;
            m = this.mean;
            pt = this.percentile;
            sk = skewness(X);          
            
            binWidth = 3.49*sig/(nthroot(this.sn,3));
            maxEvent = max(X);
            nBin = ceil(maxEvent/binWidth);
            nString = "N : " + num2str(this.sn);
            oString = "Opt c. " + num2str(round(this.f0*10)/10) + " [Nm]";
            mString = "Mean : " + num2str(round(m*10)/10)+ " [Nm]";
            sString = "Std Dev : " + num2str(round(sig*10)/10)+ " [Nm]";
            skString = "Skewness : " + num2str(round(sk*10)/10);
            infoData{1} = nString;
            infoData{2} = oString;
            infoData{3} = mString;
            infoData{4} = sString;
            infoData{5} = skString;           
            
            [y2,x2] = ecdf(X);
            [y1,x1] = hist(X,nBin);
            plotyy(x1,y1,x2,y2,@(x,y)bar(x,y,1,'FaceColor',[0.8500 0.3250 0.0980]),'stairs');
                        
            hold on
            c = plot([this.f0, this.f0], [0, max(y1)], 'LineWidth',2, 'Color', 'g');
            hold on
            d = plot([m, m], [0, max(y1)], 'LineWidth',2, 'Color', 'r');
            hold on
            e = plot([pt(1), pt(1)], [0, max(y1)],'--', 'LineWidth',2, 'Color', 'k');
            hold on
            plot([pt(2), pt(2)], [0, max(y1)],'--', 'LineWidth',2, 'Color', 'k');
            hold off
            xlabel('Compliance');
            title('Histogram of Monte-Carlo Simulation');
            ylim([0 max(1.1*y1)]);

            lg = legend([c d e], {'Opt c.','Mean', '5./95.Percentile'});
            xx = lg.Position(1);
            yy = lg.Position(2);
            h = lg.Position(4);
            
            a = annotation('textbox',...
                [xx-0.07 yy-0.5*h 0 0],...
                'String',infoData,...
                'FontSize',12,...
                'EdgeColor',[0 0 0],...
                'LineWidth',1);
            a.FitBoxToText = 'on';
            a.BackgroundColor = [1, 1, 1];            
            
        end
        
        function plotDotDiagram(this)
            this.getData();
            figure
            plot(1:this.sn, this.sf0,'o','color','b')
            hold on
            f = plot([0, this.sn], [this.f0, this.f0],'LineWidth',2, 'Color','g');
            hold on
            g = plot([0, this.sn],[this.mean, this.mean], 'LineWidth',2, 'Color', 'r');
            hold on
            e = plot([0, this.sn],[this.percentile(1), this.percentile(1)],'--', 'LineWidth',2, 'Color', 'k');
            hold on
            plot([0, this.sn],[this.percentile(2), this.percentile(2)],'--', 'LineWidth',2, 'Color', 'k');
            hold off
            title('Dot Diagram of Monte-Carlo Simulation');
            xlabel('Simulations')
            ylabel('Compliance')
            legend([f g e], {'Opt c.', 'Mean','5./95.Percentile'});
        end      
        
    end
    
    methods(Hidden)
        function getData(this)
            X = this.sf0;
            this.sigma = std(X);
            this.mean = mean(X);
            this.percentile = prctile(X,[5 95]);
        end
    
    end
    
end

