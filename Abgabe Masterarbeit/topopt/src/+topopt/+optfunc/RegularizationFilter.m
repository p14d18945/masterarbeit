classdef RegularizationFilter < topopt.optfunc.Filter
    % REGULARIZATIONFILTER to manipulate the sensitivities, densities i.e.
    % Ole Sigmund, Heaviside...
    properties
        % radius for filtering
        rMin
    end
    
    properties(SetAccess = protected)
        % Coefficients to build Mat
        H
        Hs
    end
    
    methods (Abstract)
        dc = filterSensitivities(this, x, dc)
    end
    
    methods
        function setCoeffMatrix(this, x)
            [nely, nelx] = size(x);
            % doubleRadius to integerRadius
            intR = ceil(this.rMin)-1;
            radSize = (2*intR+1)^2;
            
            iH = ones(nelx*nely*radSize, 1);
            jH = ones(size(iH));
            hf = zeros(size(iH));
            cnt = 0;
            
            for i1 = 1:nelx
                for j1 = 1:nely
                    % elementNumber
                    elemNum = (i1-1)*nely+j1;
                    
                    % Boundaries for filtering
                    leftB = max(i1 - intR, 1);
                    rightB = min(i1 + intR, nelx);
                    upperB = max(j1 - intR, 1);
                    lowerB = min(j1 + intR, nely);
                    
                    for i2 = leftB:rightB
                        for j2 = upperB:lowerB
                            cnt = cnt+1;
                            
                            % number of element inside radius
                            elemNumF = (i2-1)*nely+j2;
                            
                            iH(cnt) = elemNum;
                            jH(cnt) = elemNumF;
                            
                            % Euclidian norm between centroids
                            % of neighbour elements
                            %dist = sqrt((i1-i2)^2+(j1-j2)^2);
                            distX = i1-i2;
                            distY = j1-j2;
                            w = this.getWeightFunction(distX, distY);
                            
                            % choose elements inside radius
                            hf(cnt) = max(0, w);
                        end
                    end
                end
            end
            
            this.H = sparse(iH,jH,hf);
            this.Hs = sum(this.H,2);
        end
    end
    
    methods (Abstract)
        w = getWeightFunction(this, dist1, dist2)        
    end
end

