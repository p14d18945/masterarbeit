classdef TopOptProblem < handle
    % Topology optimization problem Class
    % This Class ise useful to optimize a structure
    
    properties
        % Structure object
        s
        % volume fraction
        volfrac = 0.5
        % Box Constraints
        % xL : lower oj-func boundary
        xL = 1e-4;
        % xU : upper oj-func boundary
        xU = 1;
        % filterList : SIMP, oleSigmund, heaviside...
        filter{}
    end
    
    properties (Hidden)
        cntFilter = 0;
    end
    
    methods
        function this = TopOptProblem(s)
            % Constructor
            this.s = s;
        end
        
        function setVolfrac(this, vol)
            % set the volume fraction
            % useful to set the objective Vol.
            this.volfrac = vol;
        end
        
        function setBoxConstraints(this, xL, xU)
            % Box Constraints for optimization
            % xL : lower oj-func boundary
            % xU : upper oj-func boundary
            this.xL = xL;
            this.xU = xU;
        end
        
        function addFilter(this, f)
            % Sensitivity filter to remove chequerboarding
            % filter : Filter Object
            this.cntFilter = this.cntFilter + 1;
            this.filter{this.cntFilter} = f;
            this.sortFilterList();
        end
        
    end
    
    methods (Hidden)
        function sortFilterList(this)
            % sort FilterList in specific range
            % 1) RHOFilter, 2) RegularizationFilter
            % this is needed to run the optimization
            % the designvariables has to be filtered at first
            % to get the gradient which has to be regularized
            
            
            nFilter = length(this.filter);
            for k = 1:nFilter
                c = superclasses(this.filter{k});
                chr = c{end-2};
                chrName = chr(16:end);
                stringName = convertCharsToStrings(chrName);
                filterName(k) = stringName; %#ok<AGROW>
            end
            % Sort Filterlist by alphabeth 
            % i.e. 
            % 1) RegularizationFilter
            % 2) RhoFilter
            [~, i] = sort(filterName);
            unsrt = this.filter;
            
            % fliplr to realize specific 
            % 1)RHO, 2)REG -Range
            srt = unsrt(fliplr(i));
            this.filter = srt;
        end
    end
end

