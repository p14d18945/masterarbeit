classdef TTOType < topopt.optfunc.UpdateType
    % TDOType returns the updatestep xTrial
    % based on the thermo dynamic optimization by Phillip Junker
    
    properties
        % betaStar, etaStar : numerical factors
        % i.e. : etaStar = 12, BetaStar = 2
        %        etaStar = 16, BetaStar = 8
        
        betaStar
        etaStar;
    end
    
    properties (Hidden)
        % elementsize vector
        h
        % laplacian of rho
        lapl
    end
    
    methods
        function this = TTOType(betaStar, etaStar)
            % constructor
            this.betaStar = betaStar;
            if nargin == 1
                this.etaStar = 12;
            else           
            this.etaStar = etaStar;
            end

        end
                        
        function setLaplacian(this, lapl)
            % insert laplacian of rho
            this.lapl = lapl;
        end
        
        function setElementSizeVec(this, h)
            % insert elementsize h = [a , b] for increment
            this.h = h(1)* h(2);
        end
        
        function beta = getBeta(this, pw)
            % calculate beta (regularization parameter)
            % pw = weighted average...
            %        ...gradient of compliance (driving force)
            
            beta = this.betaStar*this.h*pw;
        end
        
        function eta = getEta(this, pw)
            % calculate eta (viscosity)
            % pw = weighted average...
            %        ...gradient of compliance (driving force)
            eta = this.etaStar*pw;
        end
        
        function [lambdaLower, lambdaUpper] = getLambdaBoundaries(this)
            % get lagrangian multiplier boundaries
            pw = this.getWeightFunction(this.x, this.gradC);
            beta = this.getBeta(pw);
            eta = this.getEta(pw);

            lambdaLower = min(min((this.gradC + beta * this.lapl))) - eta;
            lambdaUpper = max(max((this.gradC + beta * this.lapl))) + eta;
        end
        
        function pw = getWeightFunction(this, x, dc)
            % this function is useful to scale
            % viscosity (eta) and the regularization parameter (beta)
            % by the function g which describes amount of interdemiate ("gray") densities
            % xL, xU    : BoxConstraints (lower, upper)
            % x         : current step
            % dc        : Gradient of compliance (driving Force)
            % g         :
            g = (x-this.xL).*(this.xU-x);
            pw = (sum(sum(g.*dc))/sum(sum(g)));
        end
        
        function deltaT = getTimeIncrement(this)
           % get Time increment for update scheme
           % n : size for number of inner loops
            n = (6/this.etaStar)*(this.betaStar);
            
            % deltaT : time increment 1/n
            deltaT = 1/n;
        end
        
        function xTrial = getXtrial(this, lambda)
            % x         : current step
            % dc        : Gradient of compliance (driving Force)
            % lambda    : scalar to scale (Lagrangian multiplier)
            % xTrial    : next Step
            dc = this.gradC;
            pw = this.getWeightFunction(this.x, dc);
            beta = this.getBeta(pw);
            eta = this.getEta(pw);
            deltaT = this.getTimeIncrement();

            
            %lambdaLower = min(min((dc + beta * this.lapl))) - eta
            %lambdaUpper = max(max((dc + beta * this.lapl))) + eta
            
            % calculate next Step
            xTrial = deltaT/(eta)*(dc-lambda+beta*this.lapl) + this.x;
            
            
            % check box constraints
            xTrial = this.checkBoxConstraints(xTrial);
        end
        
    end
    
    
end

