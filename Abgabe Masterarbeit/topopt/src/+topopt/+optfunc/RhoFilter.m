classdef RhoFilter < topopt.optfunc.Filter
    % RhoFunction is useful to calcute the
    % density by a given designVariable
    % rho(x)
   
    methods (Abstract)
        x = filterDensities(x);
        dc = filterSensitivities(x, dc);               
    end
end

