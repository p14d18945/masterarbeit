classdef BrunsTortorelliFilter < topopt.optfunc.DensityFilter
    % Bruns and Tortorelli Density filter    
    methods
        function this = BrunsTortorelliFilter(rMin)
            % constructor
            % rMin i.e. 0.042*nelx
            this.rMin = rMin;
        end
        
        function xPhys = filterDensities(this, xNew)
            % filter densities
            xPhys(:) = (this.H *xNew(:))./this.Hs;
            % sparse to Matrix
            [nely, nelx] = size(xNew);
            xPhys = reshape(xPhys, nely, nelx);
        end
        
        function dc = filterSensitivities(this, xNew, dc)
            % filter sensitivites
            dc(:) = this.H *(dc(:)./this.Hs);
            % sparse to Matrix
            [nely, nelx] = size(xNew);
            dc = reshape(dc, nely, nelx);
        end
        
        function dv = filterVolumes(this, xNew, vol)
            % filter volumes
            dv(:) = this.H *(vol(:)./this.Hs);
            % sparse to Matrix
            [nely, nelx] = size(xNew);
            dv = reshape(dv, nely, nelx);
        end
        
        function w = getWeightFunction(this, distX, distY)
            norm = sqrt(distX^2+distY^2);
            w = exp((-1/2)*(norm/(this.rMin/3))^2);
        end
        
    end
    
end

