classdef DensityFilter < topopt.optfunc.RegularizationFilter
    % CLASS FOR DENSITY FILTER i.e. BrunsTortorelliFilter
    
   methods (Abstract)
        x = filterDensities(this, x)
        dc = filterSensitivities(this, x, dc)
        w = getWeightFunction(this, distX, distY)
   end
end

