classdef UpdateType < handle
    % UpdateType returns the optimizations specific
    % updatestep
    % i.e       TDO     : xTrial
    %     StandardOC    : xTrial
    properties
        % BoxConstraints (lower, upper)
        xL
        xU
        % density field
        x
        % gradient of compliance
        gradC
    end
    
    methods (Abstract)
        xTrial = getXtrial(lambda);
        [lambdaLower, lambdaUpper] = getLambdaBoundaries(this);
    end
    
    methods
        function setBoxConstraints(this, xL, xU)
            % Box Constraints for optimization
            % xL : lower oj-func boundary
            % xU : upper oj-func boundary
            this.xL = xL;
            this.xU = xU;
        end
        
        function setDensity(this, x)
            % insert density field
            this.x = x;
        end
        
        function setGradient(this, dc)
            % insert gradient of rho
            this.gradC = dc;
        end
        
        function xTrial = checkBoxConstraints(this, x)
            xTrial = max(this.xL, min(this.xU, x));
        end
    end
end

