classdef OptimizationMethod < handle
    properties
        tp                   % topOptProblem Object        
        plotInfo = 1;        % add Typename and MOD to plot
        plotFTime = 0;       % plotConvergenveTime
        showUpdate = false;  % show optimization update
    end
    
    properties (SetAccess = protected)
        nIt = 0;            % number of iterations
        cIt = [];           % compliance
        conTime = [];       % convergenceTime
        stopCrit = 1e-4;    % StopCriteria for Optimization
    end
    
    
    % Abstract SuperClass
    methods (Abstract)
        solve(this);
    end
    
    
    methods
        
        function setOptimizationProblem(this, topProb)
            % topProb :  TopOptProblem-object
            this.tp = topProb;
        end
        
        function mod = getMOD(this)
            % measure of discreteness (MOD)
            xtry = this.tp.s.getDensityMesh();
            vo = xtry;
            vo(:) = this.tp.volfrac;
            omega = sum(vo(:));
            vtry = sum(sum(xtry.*(1-xtry)));
            mod = (1/omega*vtry);
        end
        
        function setStopCriteria(this, stopCrit)
            % StopCriteria for Optimization
            this.stopCrit = stopCrit;
        end
        
        function  plotF(this)
            % plot objective function
            it = 1:this.nIt;
            
            if ~this.plotFTime
                xAxes = it;
                xLabel = 'Iterations';
            elseif this.plotFTime
                xAxes = this.conTime;
                xLabel = 'Time [s]';
            end
            
            plot(xAxes, this.cIt,'-o');
            xlabel(xLabel)   
            ylabel('Compliance')
            title('Objective Function')
            
            if this.plotInfo
            infoData = {};
            
            % optimization type
            chr = class(this.OCType);
            chrName = chr(16:end);
            type = convertCharsToStrings(chrName);
            infoData{1} = type;
            
            % regularization Filter
            if ~isempty(this.tp.filter)
                f = this.tp.filter;
                nFilter = length(f);
                for k = 1:nFilter
                    c = class(f{k});
                    chrName = c(16:end-6);
                    stringName = convertCharsToStrings(chrName);
                    infoData{1+k} = stringName; %#ok<AGROW>
                end
            else
                k = 0;
            end
            % measure of discreteness
            mod = round(this.getMOD()*100000)/100000;
            mod = "MOD = "+num2str(mod);
            infoData{2+k} = mod;
            
            % textbox
            annotation('textbox',...
                [0.66 0.82 0.22 0.1],...
                'String',infoData,...
                'FontSize',11,...
                'EdgeColor',[1 1 1],...
                'LineWidth',1); 
            end
        end
        
        
    end
    
    methods (Static)
        function xNew = applyVoidElements(voidIDs, x)
            % find void elements, rowwise
            iD = find(voidIDs);
            if  iD > 0
                % transpose because matlab fills the matrix columnwise
                xtrans = x';
                % set void elements rowwise to minDensity
                xtrans(iD') = 10^-5;
                % transpose back
                xNew = xtrans';
            else
                xNew = x;
            end
        end
    end
end

