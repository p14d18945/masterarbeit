classdef OCMethod < topopt.optfunc.MathematicalOptimization
    % SubClass of OptimizationMethod
    
    properties
        % OCType-Object
        OCType
    end
    
    properties (Hidden)
        % object to increase beta
        heavisideObj
        volumeFix
    end
           
    methods
        
        function this = OCMethod(OCType)
            % Insert specific step evolution
            % i.e. StandardOCType or TDOType
            this.OCType = OCType;
        end
        
        function solve(this)
            
            %%%%%%%%% PREPARE %%%%%%%%%
            
            % topOptProblem
            tp = this.tp;
            
            % structure Object
            s = tp.s;
            % voidList
            xVoids = s.voidElements;
            % volumefraction Mesh
            xNew = s.getDensityMesh()*tp.volfrac;
            
            
            % objective volume
            v0 = sum(xNew(:));
            % volume Mesh;
            vol = ones(size(xNew));
            
            % evolution method
            evo = this.OCType;
            % setBoxConstraints
            evo.setBoxConstraints(tp.xL, tp.xU);
            
            
            % filterList
            this.seperateFilter();
            if ~isempty(this.densityFilterList)
                filter = this.densityFilterList{1};
                filter.setCoeffMatrix(xNew)
            elseif ~isempty(this.sensitivityFilterList)
                filter = this.sensitivityFilterList{1};
                filter.setCoeffMatrix(xNew)
            end
            
             % apply regularization (oleSigmund/ Heaviside...)
             xPhys = this.applyDensityRegularization(xNew);
            
            % init history
            c = 0;
            cnt = 0;
            elapsedTime = 0;
            change = 1;
            %%%%%%%%% start optimization %%%%%%%%%            
            while change > this.stopCrit && cnt <= 1000
                tic
                cnt = cnt + 1;
                
                % save last compliance
                cold = c;
                
                % apply rhoFilter (SIMP/RAMP)
                rho = this.applyRhoFilter(xPhys);
                
                % update structure and solve
                s.setRhoMesh(rho);
                [c, dc] = s.solve();                               
                
                % apply gradRhoFilter (SIMP/RAMP)
                dc = this.applyGradRhoFilter(xPhys, dc);
                
                % apply sensitivity regularization (oleSigmund/ Heaviside...)
                [dc, dv] = this.applySensitivityRegularization(xNew, dc, vol);
                
                % check if it's StandardOCStepSize
                
                if isa(evo, 'topopt.optfunc.StandardOCType')
                    evo.setVolume(dv);
                end
                
                % check if it's TDO
                if isa(evo, 'topopt.optfunc.TTOType')
                    % insert ElementSize
                    h = s.m.getElementSize();
                    evo.setElementSizeVec(h);
                    % insert Laplacian
                    lapl = s.getDensityLaplacian();
                    evo.setLaplacian(lapl);
                end
                
                % Update Stepsize-Type Properties in evo-obj
                evo.setDensity(xNew);
                evo.setGradient(dc);
                
                % get specific lambdaBoundaries
                [l1, l2] = evo.getLambdaBoundaries();                
  
                % save old x
                x = xNew;
                
                % update x
                [xNew, xPhys] = this.updateX(xVoids, v0, l1, l2);
                
                % update structure for plot and laplacian
                s.setDensityMesh(xPhys);               
                
                % stopCriteria
                %change = (abs(c-cold)/c);
                %change = max(abs(xNew(:)-x(:)));
                
                % check if heavisidefilter is used
                % increase beta and update paramter
                if ~isempty(this.heavisideObj)
                    this.stopCrit = 0.01;
                    change = max(abs(xNew(:)-x(:)));
                    f = this.heavisideObj;
                    change = f.updateParameter(change, this.stopCrit);
                    this.volumeFix = v0;
                else
                    change = (abs(c-cold)/c);
                end
                
                
                if this.showUpdate 
                fprintf(' It.:%5i Obj.:%11.4f Vol.:%7.3f ch.:%7.4f\n',cnt,c, ...
    mean(xPhys(:)),change);
                end
                
            
                % save history
                timeIncrement = toc;
                this.conTime(cnt) = elapsedTime;
                elapsedTime = elapsedTime + timeIncrement;                
                this.cIt(cnt) = c;
                
            end
            this.nIt = cnt;
        end
    end
    
    
    methods (Hidden)
        
        function [xNew, xPhys] = updateX(this, xVoids, v0, l1, l2)
            % evolution object
            evo = this.OCType;
            % Start
            vStep = 1;
            
            cntBisec = 0;
            %while abs(vStep-v0)>10^-7
            % || cntBisec < 500
            while  (abs(vStep-v0)>10^-7 || (l2-l1)/(l1+l2) > 1e-3 )
                cntBisec = cntBisec + 1;
                % get new Step
                lmid = 0.5*(l2+l1);
                
                xNew = evo.getXtrial(lmid);
                % applyVoidElements
                xNew = this.applyVoidElements(xVoids, xNew);
                % apply regularization (oleSigmund/ Heaviside...)
                xPhys = this.applyDensityRegularization(xNew);

                % current volume
                vStep = sum(xPhys(:));
                
                % updateLambda
                if vStep > v0
                    l1 = lmid;
                else
                    l2 = lmid;
                end
                
                % in case the heaviside filter is choosen 
                % it's not possible to fullfill the volume constraint
                % volume fix:
                if ~isempty(this.volumeFix)
                    vStep = this.volumeFix;
                end
                %lmid = 0.5*(l2+l1);
            end
        end
        
        function rho = applyRhoFilter(this, xPhys)
            % RhoFilter
            if ~isempty(this.rhoFilterList)
                rfl = this.rhoFilterList;
                for k = 1:length(rfl)
                    % currentFilter
                    xPhys = rfl{k}.filterDensities(xPhys);
                end
            end
            % save filtered parameter
            rho = xPhys;
        end
        
        function dc = applyGradRhoFilter(this, xPhys, dc)
            % RhoSensitiviy
            if ~isempty(this.rhoFilterList)
                rfl = this.rhoFilterList;
                for k = 1:length(rfl)
                    % currentFilter
                    dc =  rfl{k}.filterSensitivities(xPhys, dc);
                end
            end
        end
        
        function [xPhys] = applyDensityRegularization(this, xNew)
            % Regularization/ RhoSensitiviy
            if ~isempty(this.densityFilterList)
                dfl = this.densityFilterList;
                for k = 1:length(dfl)
                    % currentFilter
                    xNew = dfl{k}.filterDensities(xNew);
                end
            end
            % save filtered paramter
            xPhys = xNew;
        end
        
        function [dcF, dvF] = applySensitivityRegularization(this, xNew, dc, vol)
            % Regularization
            
            % sensitivityFilter
            if ~isempty(this.sensitivityFilterList)
                sfl = this.sensitivityFilterList;
                for k = 1:length(sfl)
                    % currentFilter
                    dc = sfl{k}.filterSensitivities(xNew, dc);
                end
            end
            
            % DensityFilter
            if ~isempty(this.densityFilterList)
                dfl = this.densityFilterList;
                for k = 1:length(dfl)
                    % currentFilter
                    dc = dfl{k}.filterSensitivities(xNew, dc);
                    vol = dfl{k}.filterVolumes(xNew, vol);
                    % update increment (only for HeavisideF)
                    name = class(dfl{k});

                    if strcmp(name,'topopt.optfunc.HeavisideFilter') %#ok<STISA>
                        this.heavisideObj = dfl{k};                      
                    end
                end
            end
            % save filtered paramter
            dcF = dc;
            dvF = vol;
        end
    end
end

