classdef SensitivityFilter < topopt.optfunc.RegularizationFilter
    % CLASS FOR SENSITIVTY FILTER i.e. OleSigmundFilter
    
   methods (Abstract)
        dc = filterSensitivities(this, x, dc)
        w = getWeightFunction(this, distX, distY) 
   end
end

