classdef DirichletBC < topopt.fefunc.BoundaryCondition
    %DIRICHLET BOUNDARY CONDITION
    properties (Hidden)
        dir % direction of constraint
    end
    
    methods (Hidden)
               
        function  dofs = getDOFS(this, fixed, mesh)
            % get number of nodes
            nN = mesh.countNodes();
            % initialize DOF-matrix
            dofs = zeros(2*nN,1);
            for i = 1:nN
                % IDs for vertical and horizontal DOFs
                % example:
                %          dir = [0, 1], node=2
                %          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
                id = fixed.*[(i*2-1), i*2];
                
                % get specific node position
                p = mesh.getNodePosition(i);
                
                % check if node is on boundary, set dof = true/ false;
                dofs(id(find(id))) = this.isOnBoundary(p);
            end
        end
    end
    
    methods
        
        
        
        
        function setFixed(this, fixed)
            % set fixed direction
            this.dir = fixed;
        end
        
        function fixed = getFixed(this)
            % get fixed direction
            fixed = this.dir;
        end
    end
    
end

