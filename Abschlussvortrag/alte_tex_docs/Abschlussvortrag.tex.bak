%\documentclass[handout]{beamer}
\documentclass {beamer} 

\usepackage{wrapfig}
% -----------------------------------
\usepackage{apacite} 				% Literatur-Referenzen: American Psycholog. Assoc.
\usepackage{natbib}					
\usepackage{mdwlist}
\setcitestyle{round,aysep={}} 		% Indizierg. in runden Klammern, zw. Autor u. Jahr
\usepackage[ansinew]{inputenc}		% Umlaute im Text
\usepackage[ngerman]{babel} 		% Rechtschreibg.				
\usepackage[T1]{fontenc}			% Ausgabe von umlauten in PDF
\hyphenation{voll-st\"andigen}
\usepackage{lmodern}				% Wichtig für scharfe PDF-Ausgabe 
\usepackage{graphicx} 				% Grafiken einfügen (pdf,png - aber jpg vermeiden)
\graphicspath{{./Bilder/}}          % Pfad zu den Bildern
\usepackage{url}					% URL's formatieren (z.B. in Literatur) 
\usepackage{hyperref} 				% für Hyperlinks in PDF-Dokumenten   
\usepackage{array}					% Gleitumgebung
\usepackage{colortbl,xcolor}		% Farbige Tabellen erstellen 
\definecolor{mlgreen}{rgb}{.13,0.55,.13} 
\definecolor{mlviolett}{rgb}{.643,.259,.804} 



\usepackage{multirow}				%  Farben definieren für Tabellen 

\usepackage{tabularx,tabu,booktabs} % bessere Gestaltung von Tabellen
\usepackage{longtable} 				% Tabelle über 2 Seiten 
\usepackage[active]{srcltx}

%\usepackage{nccfoots}
%\usepackage[bottom]{footmisc}

\usepackage{listings} 				 

%Aufzählung und folgend Matlab-Code
\lstdefinestyle{mlab}{language=Matlab, numbers=left, numberstyle=\tiny,%5   
 basicstyle={\ttfamily},%   
  keywordstyle={\color{blue}},%   
  commentstyle=\color{mlgreen},%   
  stringstyle=\color{mlviolett},%   
  breaklines=true, 
  }  %   

\lstset{literate=%   
   {Ö}{{\"O}}1 
   {Ä}{{\"A}}1 
   {Ü}{{\"U}}1 
   {ß}{{\ss}}2 
   {ü}{{\"u}}1 
   {ä}{{\"a}}1 
   {ö}{{\"o}}1 
}

%\usepackage{enumitem}
%\setlistdepth{9}

%\setlist[itemize,1]{label=$\bullet$}
%\setlist[itemize,2]{label=$\bullet$}
%\setlist[itemize,3]{label=$\bullet$}
%\setlist[itemize,4]{label=$\bullet$}
%\setlist[itemize,5]{label=$\bullet$}
%\setlist[itemize,6]{label=$\bullet$}
%\setlist[itemize,7]{label=$\bullet$}
%\setlist[itemize,8]{label=$\bullet$}
%\setlist[itemize,9]{label=$\bullet$}

%\renewlist{itemize}{itemize}{9}



\usepackage{setspace} 				% Zeileneinstellung
%\newtheorem{mydef}{Merksatz}  		% Falls Beispiele, Merksätze m. fortl. Nr. gebr. werden

\DeclareMathSymbol{*}{\mathbin}{symbols}{"01} %ersetz* mit \cdot
\usepackage{amsfonts, amsmath, amssymb, amstext}
\usepackage{graphicx}
\usepackage{subfigure} 				% mehrere Bilder nebeneinander
\usepackage{array}
%\newtheorem{bsp}{Beispiel}
\usepackage{tikz,pgfplots}			%zum zeichnen
\pgfplotsset{compat=1.14}
% Document
% -----------------------------------


%--------- für Tabellen

\usepackage{booktabs,array}
% Document
% -----------------------------------

%---------
\usecolortheme{seagull} 
\beamertemplatenavigationsymbolsempty  %Abschalten der Navigationsleise 
\colorlet{dunkelrot}{red!80!black} 
\setbeamercolor{frametitle}{fg=dunkelrot} 
\setbeamercolor{palette sidebar primary}{fg=dunkelrot} 
\setbeamercolor{palette sidebar secondary}{fg=dunkelrot} 
\setbeamercolor{palette sidebar tertiary}{fg=dunkelrot} 
\setbeamercolor{palette sidebar quaternary}{fg=dunkelrot} 

\setbeamertemplate{footline}[frame number] 
 
\usetikzlibrary{arrows,intersections}

%\setbeamercovered{dynamic}

\begin{document}
%\let\thefootnote\relax
%\deffootnotemark{[\thefootnotemark]} 
%\deffootnote[1em]{1.5em}{1em}{\textsuperscript{[\thefootnotemark]}}
%\vspace*{0cm}
\institute{Institut f\"ur \\Grundlagen des Bauingenieurwesens und \\Computerorientierte Methoden}
\title{Formoptimierung von Stabtragwerken}

\author{Pascal Diering} 
\subtitle{Abschlussvortrag Bachelorarbeit}
\date{28.Februar 2018} 

%\frame{\titlepage} 
    %\begin{frame}[plain]
    %    \titlepage
    %\end{frame}

\begin{frame}[plain]
\vfill
\centering

  \begin{beamercolorbox}[sep=8pt,center,colsep=-4bp,rounded=true,shadow=true]{institute}
        \usebeamerfont{institute}\insertinstitute
  \end{beamercolorbox}

{\usebeamercolor[fg]{titlegraphic}\inserttitlegraphic\par}

  \begin{beamercolorbox}[sep=8pt,center,colsep=-4bp,rounded=true,shadow=true]{title}
        \usebeamerfont{title}\inserttitle\par%
        \ifx\insertsubtitle\@empty%
        \else%
        \vskip0.25em%
        {\usebeamerfont{subtitle}\usebeamercolor[fg]{subtitle}\insertsubtitle\par}%
      \fi%     
  \end{beamercolorbox}%

  \vskip1em\par

  \begin{beamercolorbox}[sep=8pt,center,colsep=-4bp,rounded=true,shadow=true]{author}
        \usebeamerfont{author}\insertauthor
  \end{beamercolorbox}

  \begin{beamercolorbox}[sep=8pt,center,colsep=-4bp,rounded=true,shadow=true]{date}
        \usebeamerfont{date}\insertdate
  \end{beamercolorbox}\vskip0.5em
\end{frame}





\addtobeamertemplate{frametitle}{}{%
\begin{tikzpicture}[remember picture,overlay]
\node[anchor=north east,yshift=-2pt] at (current page.north east) {\includegraphics[height=0.8cm]{BO-Logo_m_Wortmarke_L10cmPrint}};
\end{tikzpicture}}

%\frame{\frametitle{Inhaltsverzeichnis}\tableofcontents} 
\begin{frame}[t]
\frametitle{Gliederung}
\begin{itemize}

\vspace{4mm}
\item Einleitung\pause

\vspace{2mm}
\item Grundlagen der Strukturoptimierung\pause

\vspace{2mm}
\item Tragwerkseigenschaften
\begin{itemize}
\item Geometrische Nichtlinearit\"at
\item Geometriebeschreibung \pause
\end{itemize}

\vspace{2mm}
\item Grundlagen der mathematischen Optimierung\pause

\vspace{2mm}
\item Umsetzung in MATLAB\pause

\vspace{2mm}
\item Anwendungsbeispiele\pause

\vspace{2mm}
\item Zusammenfassung und Ausblick
\end{itemize}
\end{frame}


%__________________________________
%Nächste Folie
%__________________________________ 
\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item \color{black}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}


\end{frame}



%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Motivation}
Warum Tragwerksoptimierung? \pause
\begin{itemize}
\vspace{6mm}
\item Zur Ressourceneinsparung\pause
\vspace{4mm}
\item Als Methode zur Formfindung\pause
\vspace{4mm}
\begin{itemize}
\begin{normalsize}
\item Nachweis sekund\"ar\pause
\vspace{4mm}
\item Vorweis prim\"ar
\end{normalsize}
\end{itemize}
\end{itemize}
\end{frame}

%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{Formfindungsmethoden}
Beispielhafte Forschungsarbeit:\\ \pause
\begin{figure}[htbp]
    \centering %4
    \begin{minipage}[t]{0.46\linewidth}
	\vspace{0pt}
	%\begin{footnotesize}
	\begin{enumerate}
	\onslide<2->{\item Schalenbauer Heinz Isler:}\\ 
	\vspace{5mm}
	\onslide<3->{Formbildung aus:}
	\vspace{2.5mm}
\begin{itemize}
\onslide<4->{\item Handt{\"u}chern} 
\vspace{2mm}
\onslide<5->{\item Kopfkissen} 
\vspace{2mm}
\onslide<6->{\item Gummimembranen}
\end{itemize}	
	\end{enumerate}
	%\end{footnotesize}
	\end{minipage}
	\hfill
    \begin{minipage}[t]{0.525\linewidth}
		\vspace{0pt}        
        \centering
        \onslide<2->{\includegraphics[width=\linewidth]{Bilder/einleitung/bpstation}}
        \vspace{-0.5cm}
        \onslide<2->{\scriptsize{BP Service Station, Deitingen\footnote<2->[frame]{\scriptsize{Quelle: http://bit.ly/29plX5k}}}}
\end{minipage}
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
%\begin{frame}[t]
%\frametitle{Formfindungsmethoden}
%Weitere experimentelle Methoden zur Formfindung: \\ 
%\begin{figure}[htbp]
%    \centering %4
%\begin{minipage}[t]{0.45\linewidth}
%\vspace{0pt}

%\begin{enumerate}
%\setcounter{enumi}{1}
%\onslide<1->{\item Architekt Antoni Gaudi:}

%\vspace{5mm}
%\onslide<2->{Formbildung aus:}\\
%\vspace{2.5mm}
%\begin{itemize}
%\onslide<3->{\item H\"angemodelle}
%\end{itemize}

%\vspace{3mm}
%\onslide<4->{ F\"ur Bogen- und Schalenstrukturen}\\
%\end{enumerate}
%\end{minipage}
%\hfill
%    \begin{minipage}[t]{0.5\linewidth}
%		\vspace{0pt}        
%        \centering
 %       \onslide<1->{\includegraphics[width=\linewidth]{Bilder/einleitung/gaudim}}
%        \vspace{-0.5cm}
%       \onslide<1->{\scriptsize{H{\"a}ngemodell von Antoni Gaudi\footnote<1->[frame]{\scriptsize{Quelle: http://bit.ly/2FE0NAN}}}}
%\end{minipage}
%\end{figure}
%\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Formfindungsmethoden}
Die Computertechnologie erm{\"o}glicht es die experimentellen Formfindungsmethoden um mathematisch-numerische zu erg\"anzen.\\ \pause

\vspace{0.5cm}
%\begin{figure}[htbp]
%    \centering
%    \begin{minipage}[t]{1\linewidth}
%        \centering
%        \includegraphics[width=\linewidth]{Bilder/einleitung/num}
%    \end{minipage}% <- sonst wird hier ein Leerzeichen   
%\end{figure}
\begin{normalsize}
Vorteile:

\vspace{3mm}
\begin{itemize}
\item Die Optimierung von komplexen Strukturen ist m{\"o}glich.\\ \pause

\vspace{2mm}
\item Nebenbedingungen k{\"o}nnen ber{\"u}cksichtigt werden.\\ \pause

\vspace{2mm}
\item Maximierungsaufgaben k{\"o}nnen behandelt werden.\\ 
\end{itemize}
\end{normalsize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item \color{black}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Disziplinen der Strukturoptimierung}
\onslide<1->{Die Strukturoptimierung umfasst vier Problemklassen.}\\ \pause

\vspace{0.25cm}
\begin{figure}[htbp]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \includegraphics[width=0.95\linewidth]{Bilder/disziplinen}
        \vspace{-0.5cm}
        \onslide<2->{\scriptsize{Disziplinen in der Strukturoptimierung\footnote<2->[frame]{\scriptsize{Quelle: Computational Modelling of Concrete Structures, Hrsg. Ren\'{e} de Borst}}}}
    \end{minipage}% <- sonst wird hier ein Leerzeichen   
\end{figure}
\begin{itemize}

\vspace{2mm}
\onslide<3->{\item Behandelt: Kombinierte Form- und Querschnittsoptimierung}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Entwurfsmodell}
\begin{itemize}
\onslide<1->{\item Schnittstelle: Mathematische Optimierung und Tragwerk}\\ 
\onslide<2->{\item Entwurfsvektor: Form- und Querschnittskomponente}\\
\end{itemize}



%\begin{figure}[h!]
%        \centering
%        \onslide<2->{\includegraphics[width=0.7\linewidth]{Bilder/Entwurfsmodell/querschnitte}}\\
        %\onslide<2->{\scriptsize{Profilabmessungen}}
%\end{figure}
\vspace{-5mm}
\begin{figure}[h!]
    \centering
    \begin{minipage}[t]{1\linewidth}
       \centering
        \onslide<2->{\includegraphics[width=\linewidth]{Bilder/einleitung/num}}
    \end{minipage}% <- sonst wird hier ein Leerzeichen   
\end{figure}
%\begin{itemize}

%\vspace{2.5mm}
%\onslide<3->{\item Keine Standartprofile in der Optimierung}
%\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Analysemodell}
\onslide<1->{Dient zur mathematischen Erfassung der Modelleigenschaften.}\\ 

\vspace{3mm}
\onslide<2->{Folgendes wird ermittelt:} 
\begin{itemize}

\vspace{2mm}
\onslide<3->{\item Tragwerksantwort}
\onslide<4->{\item Funktionswerte und Sensitivit\"aten von Problemfunktion\footnote<4->[frame]{\scriptsize{Zusammenfassender Begriff f\"ur Zielfunktion und Nebenbedingungen}}} 
\end{itemize}

\vspace{-0.5cm}
\begin{figure}[htbp]
    \centering
    \begin{minipage}[t]{1\linewidth}
        \centering
        \onslide<4->{\includegraphics[width=0.6\linewidth]{Bilder/Entwurfsmodell/diff}}\\
        \onslide<4->{\scriptsize{Numerische Sensitivit\"atsanalyse}}
    \end{minipage}% <- sonst wird hier ein Leerzeichen   
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$] \color{black}{Tragwerkseigenschaften}
\begin{itemize}
\item Geometrische Nichtlinearit\"at
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Geometrische Nichtlinearit{\"a}t}
\onslide<1->{Endliche\footnote<1->[frame]{\scriptsize{Das hei{\ss}t: Nicht vernachl\"assigbar kleine}} Verformungen f\"uhren zu nichtlinearen Effekten.}
\begin{itemize}
\onslide<2->{\item Stabilit\"at von Druckbeanspruchten filigranen Konstuktionen} 
\end{itemize}
 

\vspace{-0.3cm}


\begin{figure}[t]
		\vspace{0pt}        
        \centering
        \onslide<3->{\includegraphics[width=0.9\linewidth]{Bilder/einleitung/nonlinear}}\\
        \vspace{1mm}
\onslide<3->{\scriptsize{Quelle: Klaus J{\"u}rgen Bathe, Finite Element Methode}}
\end{figure}

\vspace{-2mm}
\begin{itemize}
\onslide<3->{\item Eine Steigerung der Last $P$ um $\alpha$ f{\"u}hrt zu einer nichtlinearen Steigerung ungleich $\alpha$ der Verschiebung $U$.}\\ 
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
%\begin{frame}[t]
%\frametitle{Geometrische Nichtlinearit{\"a}t}
%\onslide<1->{Zugrundeliegender Algorithmus:}\\ 
%\vspace{0.8cm}


%\onslide<2->{\hrule}
%\begin{figure}[htbp]
%        \centering
%        \onslide<2->{\includegraphics[width=\linewidth]{Bilder/nichtlinearitaet/algo}}\\
%\end{figure}
%\vspace{-2mm} 
%\onslide<2->{\hrule}

%\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{Geometrische Nichtlinearit{\"a}t}
Schalendachkonstruktion aus Glas und Stahl, welche aufgrund vernachl\"assigter Ph\"anomene versagte.\\
\begin{figure}[htbp]
        \centering
        \includegraphics[width=0.85\linewidth]{Bilder/einleitung/knick}  
\end{figure}
\centering
\vspace{-0.1cm}
\scriptsize{Sporthalle in Halsterbek, Quelle: www.baunetz.de} 
\end{frame}
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$] \color{black}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{black}$\blacktriangleright$] \color{black}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Geometriebeschreibung}
Ein Entwurfsvektor von geringer L\"ange erh\"oht die numerische Stabilit\"at und Effizienz der Optimierung.
 
\vspace{0.3cm}
\begin{itemize}
\onslide<2->{\item Tragwerksgeometrie wird durch B-Spline-Kurven beschrieben.}
\end{itemize}

\vspace{0.3cm}
\onslide<3->{Eigenschaften:} 

\vspace{-0.7cm}
\begin{columns} 
\begin{column}{0.5\linewidth} 
\begin{flushleft}

\vspace{0pt}
\begin{itemize} 
\onslide<4->{\item Kurvenkonstruktion anhand von Kontrollpunkten.\\} 
\onslide<5->{\item  Kontrollpunkte werden nicht interpoliert.\\} 
\onslide<6>{\item  Keine Oszillation tritt auf.\\} 
\end{itemize} 
\end{flushleft} 
\end{column} 

\hfill
\begin{column}{0.45\linewidth} 
\onslide<4->{ 
\begin{flushright}

\vspace{0pt} 
\begin{figure} 
\centering 
\includegraphics[width=\linewidth]{Bilder/einleitung/spline}
\end{figure} 
\end{flushright}} 
\end{column} 
\end{columns} 

\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Geometriebeschreibung}
B-Spline-Kurve vom Polynomgrad $p$:
\begin{align*}
C(u)=\sum_{i=0}^n N_{i,p}(u)\textbf{P}_{i},\hspace{1cm} a\leq u \leq b
\end{align*}
mit: 
\begin{itemize}
\onslide<2->{\item Knotenvektor: $U=\{\underbrace{a,...,a}_{p+1},u_{p+1},...,u_{m-p-1},\underbrace{b,...,b}_{p+1}\}$}
\onslide<3->{\item Basisfunktionen: $N_{i,p}(u)$, st\"uckweise berechnen.}

\vspace{4.5mm}
\onslide<4->{\item Kontrollpunkte: $P_{i}$}
\end{itemize}


\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Geometriebeschreibung}
\onslide<1->{B-Spline-Basisfunktionen:}\\
\begin{figure} 
\centering 
\onslide<1->{\includegraphics[width=\linewidth]{Bilder//geometrie/B_SplineO}}
\end{figure} 
\begin{figure} 
\centering 
\onslide<2->{\includegraphics[width=\linewidth]{Bilder//geometrie/B_SplineP}}
\end{figure} 

\end{frame}
%__________________________________
%Nächste Folie
%__________________________________


\begin{frame}[t]
\frametitle{Geometriebeschreibung}
Wird die Tragwerksgeometrie mit B-Spline-Kurven beschrieben, so sind folgende Besonderheiten zu ber{\"u}cksichtigen: \pause
\begin{figure} 
\centering 
\includegraphics[width=0.95\linewidth]{Bilder/geometrie/Vergleich/trag}
\end{figure} 

\begin{itemize}
\item  Verkn{\"u}pfung der Tragwerksknoten\\ \pause
\item Weniger Entwurfsvariablen $\rightarrow$ Effizienzsteigerung\\ 

\end{itemize} 
\end{frame}
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$] \color{black}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}

%__________________________________
%Nächste Folie
%__________________________________


\begin{frame}[t]
\frametitle{Beschr\"anktes Optimierungsproblem}
\onslide<1->{Mathematische Schreibweise:\\
\begin{align*}
\min\limits_{\textbf{x} \in S} f_0(\textbf{x}) \text{\hspace{1.5mm} mit \hspace{1.5mm}}S=\{\textbf{x} \in \mathbb{R}^{n}\hspace{0.8mm}\vert\hspace{0.8mm} \textbf{x}_{u} \leq \textbf{x} \leq \textbf{x}_{o},\hspace{0.8mm}f_{i}(\textbf{x})\leq 0,\hspace{0.8mm}i = 1,...,m\} 
\end{align*}\\
mit:}
\begin{itemize}
\onslide<2->{\item  $f_{0}(\textbf{x}):\mathbb{R}^{n} \rightarrow \mathbb{R} $: Zielfunktion zum minimieren} 
\onslide<3->{\item  \textbf{x}: Entwurfsvektor mit der L\"ange n}
\onslide<4->{\item  $S$: Menge der zul\"assigen L\"osungen} 
\onslide<5->{\item $\textbf{x}_{o}$, $\textbf{x}_{u}$: Explizite Nebenbedingungen} 
\onslide<6->{\item  $f_{i}(\textbf{x}):\mathbb{R}^{n} \rightarrow \mathbb{R}$: Implizite Nebenbedingungen}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

%\begin{frame}[t]
%\frametitle{Numerisches L\"osungsverfahren}

%Struktur vom SQP-Verfahren:\\

%\vspace{2mm} 
%\hrule
%\begin{figure}[h!]
%\centering 
%\includegraphics<1->[width=0.9\linewidth]{Bilder/mathematische_optimierung/grundsatz}%
%\end{figure}
%\vspace{-2mm} 
%\onslide<1->{\hrule}
%\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{Numerisches L\"osungsverfahren}

\vspace{-0.5cm}
\hspace{-0.62cm} \onslide<1->{SQP-Verfahren:}

\begin{columns} 
\begin{column}{0.5\linewidth} 
\begin{flushleft}

\vspace{-1cm}
\onslide<2->{In jeder k-ten Iteration:}

\vspace{0.1cm}
\begin{itemize}
\onslide<2->{\item Zielfunktion $f_0(\textbf{x}^k)$:\\ Quadratisch approximieren}

\vspace{0.2cm}
\onslide<3->{\item Nebenbedingungen $f_i(\textbf{x}^k)$:\\ Linear approximieren}
\end{itemize}
\end{flushleft}
\end{column}


\begin{column}{0.55\linewidth} 
\begin{flushright}
\begin{figure}[h!]
\centering 
%%%%%%%%%%FÜR HANDOUT MODIFIZIEREN!!!!!!!
\vspace{-0.5cm}
\hspace{-1.5cm}\includegraphics<1>[width=\linewidth]{Bilder/mathematische_optimierung/approxS}%
\includegraphics<2>[width=\linewidth]{Bilder/mathematische_optimierung/approxSA}%
\includegraphics<3->[width=\linewidth]{Bilder/mathematische_optimierung/approxE}%
\end{figure}
\end{flushright}
\end{column}
\end{columns}
\onslide<4->{

\vspace{-0.4cm}
\hspace{-0.62cm}Quadratisches Subproblem:

\begin{small}
\vspace{-0.5cm}
\begin{align*}
&\hspace{-0.1cm} \hspace{-5.3cm}\min\limits_{s \in \mathbb{R}^n} \hspace{5mm} \frac{1}{2}s^T B^k s + \triangledown f_0(\textbf{x}^k)^T s + f_0(\textbf{x}^k)\\
&\hspace{-0.1cm} \hspace{-5.3cm} \text{u.B.v.} \hspace{3.8mm} \triangledown f_i(\textbf{x}^k)^T s + f_i(\textbf{x}^k) \leq 0 \hspace{2mm} i=1,..,m
\end{align*}
\hspace{-0.32cm} $B^k$: \hspace{1.5mm} N\"aherung der Hesse-Matrix der Lagrange-Funktion (BFGS) 
\end{small}}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

%\begin{frame}[t]
%frametitle{Numerisches L\"osungsverfahren}

%\vspace{-0.5cm}
%\begin{itemize}
%\item N\"achster Iterationsschritt: $x^{k+1}=x^k+ \alpha^k s^k$

%\vspace{0.2cm}
%\onslide<2->{\item[] mit: \hspace{0.5cm}$s^k$: k-te L\"osung des quadratischen Subproblems\\} 
%\onslide<3->{\item[] \hspace{1.1cm} $\alpha^k$: Schrittweite }
%\end{itemize}

%\vspace{0.2cm}
%\onslide<3->{Schrittweite: Armijo-Regel}\\
%\onslide<4->{
%\begin{figure} 
%\centering 
%\includegraphics[width=0.85\linewidth]{Bilder/mathematische_optimierung/armijo}
%\end{figure}
%\centering
%\vspace{-0.1cm}
%\tiny{Quelle: Optimierung - 2008, Gerdts}}
%\end{frame}
%__________________________________
%Nächste Folie
%__________________________________



\begin{frame}[t]
\frametitle{Numerisches L\"osungsverfahren}
Optimierung mit SQP-Verfahren:
\begin{columns} 
\hspace{0.5cm}\begin{column}{0.7\linewidth} 
\begin{flushleft}
\begin{itemize}
\onslide<2->{\item Verl\"asst den Zul\"assigkeitsbereich}

\vspace{1.5mm}
\onslide<3->{\item Newton-Raphson-Verfahren: Konvergenzprobleme}

\vspace{1.5mm}
\onslide<4->{\item Nebenbedingungen: $f_i(\textbf{x}^k)=1$}

\vspace{1.5mm}
\onslide<5->{\item Gradient: $\triangledown f_i(\textbf{x}^k)=0$}

\vspace{1.5mm}
\onslide<6->{\item Liniensuche scheitert}
\end{itemize}
\end{flushleft} 
\end{column} 

\hspace{-2cm}\begin{column}{0.5\linewidth} 
\onslide<4->{ 
\begin{flushright}

\vspace{-1.5cm} 
\begin{figure} 
\centering 
\includegraphics<5->[width=\linewidth]{Bilder/mathematische_optimierung/SQPProb}
\end{figure} 
\end{flushright}} 
\end{column} 
\end{columns} 


\vspace{0.6cm}
\onslide<7->{Optimierung mit FSQP-Verfahren:}

\vspace{0.1cm}
\begin{itemize}
\onslide<8->{\item Verl\"asst den Zul\"assigkeitsbereich nicht}
\end{itemize}
\end{frame}
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$] \color{black}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Umsetzung in MATLAB}

\begin{columns} 
\begin{column}{0.5\linewidth} 
\begin{flushleft}

\vspace{-0.95cm}
\begin{itemize} 
\onslide<1->{\item Optimierungsverfahren: FSQP (bereitgestellt)\\} 
\onslide<2->{\item Tragwerksanalyse: FEM-Paket BOfem\footnote<2->[frame]{\scriptsize{Entwickelt von Baitsch}}\\}  
\end{itemize} 

\vspace{0.4cm}
\onslide<3->{Selbst erarbeitet:}
\begin{itemize} 
\onslide<4->{\item Geometriebeschreibung\\} 
\onslide<5->{\item Tragwerksbildung\\}  
\onslide<6->{\item Zielfunktion\\}  
\end{itemize} 

\end{flushleft} 
\end{column} 

\hspace{-1cm}\begin{column}{0.6\linewidth} 
\onslide<2->{ 
\begin{flushright}

\vspace{0pt} 
\begin{figure} 
\centering 
\includegraphics[width=\linewidth]{Bilder/BoFem/BoFem}
\end{figure}
\centering
\vspace{0.1cm}
\scriptsize{Quelle: BOfem User's Guide} 
\end{flushright}} 
\end{column} 
\end{columns} 

\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$] \color{black}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Nebenbedingungen}

\hspace{-0.3cm}Standard Nebenbedingungen:
\vspace{0.25cm}

\hspace{-0.3cm}\begin{minipage}{0.75\textwidth}

\vspace{-2cm}
\begin{enumerate}
\onslide<2->{\item Knicknachweis der druckbeanspruchten St{\"abe}}

\vspace{0.2cm}
\onslide<3->{\item Allgemeiner Spannungsnachweis}

\vspace{0.2cm}
\onslide<4->{\item Nachweis der maximalen Verformung}
\end{enumerate}
\end{minipage}%
\hfill
\begin{minipage}{0.25\textwidth}
\centering 
\includegraphics<2->[width=\linewidth]{Bilder/Anwendungsbeispiele/Knick}
\end{minipage}

\onslide<4->{

\vspace{-1.8cm}
\begin{minipage}[l]{0.6\textwidth}
\includegraphics[width=\linewidth]{Bilder/Anwendungsbeispiele/Verform}
\end{minipage}}

\vspace{0.6cm}
\onslide<5->{\hspace{-0.3cm} Zus\"atzliche Nebenbedingungen: Siehe Beispiele}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Fachwerkbogen}


\begin{figure}[h]
\includegraphics[width=\linewidth]{Bilder/Anwendungsbeispiele/Fachwerkbogen/Fachwerkbogen}
\end{figure}
\begin{itemize}
\onslide<2->{\item Knotenlast: 40 kN}
\onslide<3->{\item Spannweite: 70 Meter}
\onslide<4->{\item H\"ohe: 12 Meter}
\onslide<5->{\item Besonderheit: Ber\"ucksichtigung von Imperfektionen}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Imperfektionen}
\begin{itemize}
\onslide<1->{\item Imperfektionsempfindliches System}
\onslide<2->{\item Imperfektionen affin zur Knickfigur}


\begin{figure}[h]
\includegraphics<2->[width=0.8\linewidth]{Bilder/Anwendungsbeispiele/Fachwerkbogen/knickfigurstart}
\end{figure}
\onslide<3->{\item In der Optimierung aufaddieren}
\onslide<4->{\item Imperfekter Startentwurf}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Startentwurf}
\vspace{-0.4cm}
\begin{figure}
\begin{center} 
\includegraphics<1->[width=\linewidth]{Bilder/Anwendungsbeispiele/Fachwerkbogen/Startentwurf}

\vspace{-5mm}
\scriptsize{Imperfekter Startentwurf:} \pause
\end{center}

\vspace{2mm}
\begin{center} 
\includegraphics[scale=0.2]{Bilder/Anwendungsbeispiele/Fachwerkbogen/loadstart}\\
\scriptsize{Last-Verformungsdiagramm:}
\end{center}
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Optimiertes Tragwerk}
\vspace{-0.4cm}
\begin{figure}
\begin{center} 
\includegraphics<1->[width=\linewidth]{Bilder/Anwendungsbeispiele/Fachwerkbogen/Opti}

\vspace{-20mm}
\scriptsize{Optimiertes Tragwerk:} \pause
\end{center}

\vspace{2mm}
\begin{center} 
\includegraphics[scale=0.25]{Bilder/Anwendungsbeispiele/Fachwerkbogen/loadopti}\\
\scriptsize{Last-Verformungsdiagramm:}
\end{center}
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Optimiertes Tragwerk}

\begin{itemize}
\onslide<1->{\item Empfindlich f\"ur Last-, oder Konfigurations\"anderungen}
\end{itemize}
%\vspace{-0.4cm}
\begin{figure}
\begin{center} 
\includegraphics<1->[width=0.8\linewidth]{Bilder/Anwendungsbeispiele/Fachwerkbogen/knickfigurOpti}

\vspace{2mm}
\scriptsize{Optimiertes Tragwerk: Knickfigur} \pause
\end{center}
\end{figure}
\begin{itemize}
\onslide<2->{\item Ber\"ucksichtigung von Imperfektionen ist notwendig}
\end{itemize}
\end{frame}
%%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Hauptbahnhof Berlin}

\vspace{-1cm}
\begin{figure}[h]
\includegraphics[width=\linewidth]{Bilder/Anwendungsbeispiele/Berlin/BogenB}
\end{figure}

\vspace{-0.5cm}
\begin{itemize}
\onslide<2->{\item Knotenlast: 40 kN}
\onslide<3->{\item Spannweite: 68,4 Meter}
\onslide<4->{\item H\"ohe: 17 Meter}
\onslide<5->{\item Besonderheit: St\"abe rechtwinklig auf den Bogen optimieren}
\onslide<6->{\item Zusatznebenbedingungen: Lichtraumprofil und maximale H\"ohe}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Normalenvektor}

In jeder Iteration:

\vspace{2mm}
\begin{minipage}[l]{0.6\textwidth}
\begin{itemize}
\onslide<2->{\item Knotendifferenzen f\"ur die Steigung:} 
\onslide<2->{\begin{align*}
m=\dfrac{\frac{\bigtriangleup_{X1}}{\bigtriangleup_{Z1}}+\frac{\bigtriangleup_{X2}}{\bigtriangleup_{Z2}}}{2}
\end{align*}}

\vspace{-1mm}
\onslide<3->{\item Inverse von Tangens f\"ur Alpha:}
\end{itemize}
\onslide<3->{\begin{align*}
\hspace{5mm}\alpha=tan^{-1}(m)
\end{align*}}

\end{minipage}%
\begin{minipage}[r]{0.5\textwidth}
\includegraphics<2->[width=0.8\linewidth]{Bilder/Anwendungsbeispiele/Berlin/Normalenvektor}
\end{minipage}
\end{frame}
%_______________________
%Nächste Folie
%__________________________________


\begin{frame}[t]
\frametitle{Startentwurf}
\vspace{-0.4cm}
\begin{figure}
\begin{center} 
\includegraphics<1->[width=\linewidth]{Bilder/Anwendungsbeispiele/Berlin/start}

\vspace{-5mm}
\scriptsize{Startentwurf:} \pause
\end{center}

\vspace{2mm}
\begin{center} 
\includegraphics[scale=0.2]{Bilder/Anwendungsbeispiele/Berlin/LoadS}\\
\scriptsize{Last-Verformungsdiagramm:}
\end{center}
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Optimiertes Tragwerk}
\vspace{-0.4cm}
\begin{figure}
\begin{center} 
\includegraphics<1->[width=\linewidth]{Bilder/Anwendungsbeispiele/Berlin/opti}

\vspace{-5mm}
\scriptsize{Optimiertes Tragwerk:} \pause
\end{center}

\vspace{2mm}
\begin{center} 
\includegraphics[scale=0.2]{Bilder/Anwendungsbeispiele/Berlin/LoadOpt}\\
\scriptsize{Last-Verformungsdiagramm:}
\end{center}
\end{figure}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________

\begin{frame}[t]
\frametitle{\"Ubersicht}
\begin{itemize}

\vspace{4mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Einleitung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$]\color{lightgray}{Grundlagen der Strukturoptimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Tragwerkseigenschaften}
\begin{itemize}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometrische Nichtlinearit\"at}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Geometriebeschreibung}
\end{itemize}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Grundlagen der mathematischen Optimierung}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Umsetzung in MATLAB}

\vspace{2mm}
\item[\color{lightgray}$\blacktriangleright$] \color{lightgray}{Anwendungsbeispiele}

\vspace{2mm}
\item[\color{black}$\blacktriangleright$]\color{black}{Zusammenfassung und Ausblick}
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Zusammenfassung}

Formoptimierung: 
\vspace{4mm}

\begin{itemize}
\onslide<2->{\item Kann einen Beitrag zur Erlangung eines wirtschaftlichen Tragwerks leisten.}

\vspace{2mm}
\onslide<3->{\item Geometrische Nichtlinearit\"at ist bei druckbeanspruchten filigranen Konstruktionen zu ber\"ucksichtigen.}

\vspace{2mm}
\onslide<4->{\item Freiformkurven zur effizienten Beschreibung der
Tragwerksgeometrie.} 

\vspace{2mm}
\onslide<5->{\item Einbindung von Imperfektionen f\"ur imperfektionsempfindliche Tragwerken.} 
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\begin{frame}[t]
\frametitle{Ausblick}

Folgendes wurde nicht ber\"ucksichtigt und ist dementsprechend noch zu untersuchen: 
\vspace{5mm}

\begin{itemize}
\onslide<2->{\item Weitere  Nebenbedingungen, z.B.: Beulnachweis}

\vspace{4mm}
\onslide<3->{\item Imperfektionen anpassen}

\vspace{4mm}
\onslide<4->{\item Optimierung auf Standardprofile}
 
\end{itemize}
\end{frame}
%__________________________________
%Nächste Folie
%__________________________________
\end{document}
