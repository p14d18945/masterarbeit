clear
%Principle of least action
%
%Data
n = 6;          % number of variations
p  =  8;        % linear variation of flight path in m/s^2
te =  2;        % flight-time
t1 =  0;        % startpoint
t2 = t1+te;     % endpoint
t = t1 : 0.05 : t2; % time-steps

m = 1;    %mass
s = 10;   %point distance
te = te;  %flight-time
g = 9.78; %graviation m/s^2
h0 = 0;   %start-point hight
%

%natural path
y = @(p, t)  -p.*t.*(t-te);
%position
x = @(t)  s*t./te;
%least action
wmin = @(m, s, te, g, h0)  m.*(s.^2./2*te.^(-1)-g.*h0.*te-g.^2./(24).*te.^3);
%action
w = @(p, m, s, te, g, h0)  m./6.*(te.^3.*p.^2-te.^3.*g.*p+3.*te.*((s./te).^2-2.*g.*h0));


c = ['y', 'm', 'c', 'g', 'b', 'k'];
q = 1:10/n:10;

a = 1;
b = 10;
q = a-1 + ceil((b-a + 1)*rand(1, n));
n_equal = 1;
duplicate_ind = 1;
delta = 0;
while n_equal > 0 || delta  < 1.2
    if delta < 1.2
        lower = delta + g/2;
        upper = abs(delta - g/2);
        id1 = find(q == lower);
        id2 = find(q == upper);
        if isempty(id1)
            id = id2;
        else
            id = id1;
        end
        change = a-1 + ceil((b-a + 1)*rand(1, 1));
        q(id) = change;
    end
    change = a-1 + ceil((b-a + 1)*rand(1, n_equal));
    q(duplicate_ind) = change;
    % indices to unique values in column 3
    [~, ind] = unique(q(:));
    % duplicate indices
    duplicate_ind = setdiff(1:length(q), ind);
    % duplicate values
    duplicate_value = q(duplicate_ind);
    n_equal = length(duplicate_ind);
    delta = min(abs(q - g/2));
end

h = figure('position', [100, 100, 750, 500]);
set(h, 'color', 'white');
ylabel ('$y(t)[m]$','interpreter','latex');
xlabel ('$x(t)[m]$','interpreter','latex');
title('Hamilton-Prinzip')
set(gca, 'FontSize', 18)
names = {'x(t_0)'; 'x(t_1)'};
high = sort(q);
high
set(gca,'xtick',[0,2],'xticklabel',names)
set(gca,'ytick',round((high.*100)./100),'yticklabel',round((high.*100)./100))
xlim([-0.2, 2.2])
ylim([0, max(q)])
%axis tight manual % this ensures that getframe() returns a consistent size
filename = 'animatedPrincipleOfLeastAction.gif';
%formular{1} '' %'Wirkung: $w_i$';
%annotation('textbox',...
%                    [0.59 0.36 0.22 0.1],...
%                    'interpreter','latex',...
%                    'String',formular,...
%                    'FontSize',15,...
%                    'EdgeColor',[1 1 1],...
%                    'LineWidth',1);
for p = 1:n+1
    if p < 6
        col = c(end-p);
    else
        col = [rand, rand, rand];
    end
    hold on
    for i = 1:length(t)      
        if  p == 1
            px(i) = t(i);
            py_1(i) = y(g/2, t(i));
            if i == 1
                entrie(1) = plot(px, py_1,'Color', 'r', 'LineWidth',3);
                action = num2str(round(wmin(m, s, te, g, h0)*100)/100);
                lg(1) = " $w_{min} =$ " + action;
                lg_obj = legend(entrie, lg,'interpreter','latex', 'Location','northeastoutside');
                %lg_obj.NumColumns=2;
            elseif i < length(t)+1 && p == 1
                plot(px, py_1 ,'Color', 'r', 'LineWidth',3,'HandleVisibility','off');
                if i == length(t)
                px = [];
                end
            end
        elseif  p > 1
            px(i) = t(i);
            py_n(i) = y(q(p-1), t(i));
            if i == 1
            entrie(p) = plot(px, py_n, 'LineWidth',1, 'Color', col);
            num = num2str(p-1);
            curve = "$w_{" + num + "} \hspace{3.2mm} = $ ";
            wm = w(q(p-1), m, s, te, g, h0);
            action = num2str(round(wm*100)/100);
            lg(p) = curve + action;
            lg_obj = legend(entrie, lg,'interpreter','latex', 'Location','northeastoutside');
            elseif i < length(t)+1 
            plot(px, py_n, 'LineWidth',1, 'Color', col,'HandleVisibility','off')        
            if i == length(t)
               px = 0;
               py_n = 0;
            end
            end
           
        end
        drawnow;
        %Capture the plot as an image
        frame = getframe(h);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        %Write to the GIF File
        if p == 1 && i == 1
            imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',1/10);
        else
            imwrite(imind,cm,filename,'gif', 'WriteMode','append','DelayTime',1/10);
        end
        
    end
end
hold off



