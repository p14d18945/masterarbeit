classdef SIMPFilter < topopt.optfunc.RhoFilter
    % SIMP : Solid Isotropic Material with Penalization 
    properties
     % p : penaltyfactor i.e: p = 3
     p
    end
    
    methods
        function this = SIMPFilter(p)
            % Constructor
            this.p = p;
        end
        
        function x = filterDensities(this, x)
        % calculate rho
        x = x.^this.p;
        end
        
        function dc = filterSensitivities(this,x, dc)
        % calculate rhoPrime and gradDC
        rhoPrime = this.p.* x.^(this.p-1);
        dc = dc.*rhoPrime;
        end
      
    end
end

