classdef MathematicalOptimization < topopt.optfunc.OptimizationMethod
    % Superclass of i.e. OCMethod
    properties
        % seperated Filterlists to 
        % increase optimization performance
        densityFilterList = {}
        sensitivityFilterList = {}
        rhoFilterList = {}
    end
    methods (Abstract)
        solve(this)
    end
    
    methods
        function seperateFilter(this)
            % topOptProblem
            tp = this.tp;
            % init counter
            cntRF = 0;
            cntDF = 0;
            cntSF = 0;
            % DENSITY FILTER
            if ~isempty(tp.filter)
                for i = 1:length(tp.filter)
                    currentFilter = tp.filter{i};
                    % check filter type
                    if isa(currentFilter, 'topopt.optfunc.RhoFilter')
                        cntRF = cntRF + 1;
                        this.rhoFilterList{cntRF} = currentFilter;
                    elseif isa(currentFilter, 'topopt.optfunc.DensityFilter')
                        cntDF = cntDF + 1;
                        this.densityFilterList{cntDF} = currentFilter;
                    elseif isa(currentFilter, 'topopt.optfunc.SensitivityFilter')
                        cntSF = cntSF + 1;
                        this.sensitivityFilterList{cntSF} = currentFilter;
                    end
                end
            end
        end
    end
end

