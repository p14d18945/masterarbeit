classdef RAMPFilter < topopt.optfunc.RhoFilter
    % RAMPFILTER
    properties
     % q : rationalfactor
     q
    end
    
    methods
        function this = RAMPFilter(q)
            % Constructor
            this.q = q;
        end
        
        function x = filterDensities(this, x)
        % calculate rho
        x = x./(1+this.q.*(1-x));
        end
        
        function dc = filterSensitivities(this, x, dc)     
        % calculate rhoPrime and gradDC
        rhoPrime = (this.q+1)./((x*this.q-this.q-1).^2);
        dc = dc.*rhoPrime;
        end
      
    end
end

