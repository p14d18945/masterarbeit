classdef Filter < handle
    % SUPERCLASS of RegularizationFilter && RhoFilter
    methods (Abstract)
        dc = filterSensitivities(this, x, dc)
    end
end

