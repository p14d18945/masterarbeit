classdef StandardOCType < topopt.optfunc.UpdateType
    % STANDARDOCTYPE
    % update stepsize of standard optimality criteria method
    
    properties
        % stepsize boundary for iteration 
        % i.e. move = 0.2
        move
        % volume
        vol;
    end
    
    methods
        function this = StandardOCType(move)
            this.move = move;
        end
        
        function setVolume(this, vol)
            this.vol = vol;
        end
        
        function xTrial = getXtrial(this, lambda)
            % x         : current step
            % dc        : Gradient of compliance (driving Force)
            % lambda    : scalar to scale (Lagrangian multiplier)
            % xTrial    : next Step
            % steplimits
            maxStep = this.x+this.move;
            minStep = this.x-this.move;
            % current steptrial
            xTrial = this.x.*sqrt(this.gradC./this.vol/lambda);
            
            % move limit
            xTrial = max(minStep, min(maxStep, xTrial));
            
            % check box constraints
            xTrial = this.checkBoxConstraints(xTrial);
        end
    end
    
    methods (Static)
            function [lambdaLower, lambdaUpper] = getLambdaBoundaries()
            % get lagrangian multiplier boundaries            
            lambdaLower = 0;
            lambdaUpper = 1e9;
        end
    end
end

