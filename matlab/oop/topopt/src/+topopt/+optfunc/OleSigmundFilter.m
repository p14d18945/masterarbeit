classdef OleSigmundFilter < topopt.optfunc.SensitivityFilter
    % Ole Sigmund sensitivity filter    
    methods
        function this = OleSigmundFilter(rMin)
            % constructor
            % rMin i.e. 0.042*nelx
            this.rMin = rMin;
        end
         
        function dcf = filterSensitivities(this, x, dc)            
            % filter sensitivites
            dc(:) = this.H *(x(:).*dc(:))./this.Hs./max(1e-4,x(:));            
            
            % sparse to matrix            
            [nely, nelx] = size(x);
            dcf = reshape(dc, nely, nelx);
        end
        
        function w = getWeightFunction(this, distX, distY)
            norm = sqrt(distX^2+distY^2);
            w = this.rMin-norm;
        end
        
    end
end

