classdef HeavisideFilter < topopt.optfunc.DensityFilter
    % Heaviside Density filter  
    properties
        beta = 1;
        loopbeta =  -3;
    end
    
    methods       
        function this = HeavisideFilter(rMin)
            % constructor
            % rMin i.e. 0.03*nelx
            this.rMin = rMin;
        end
        
        function xPhys = filterDensities(this, xNew)
            if this.loopbeta == -3
                % first iteration
                xTilde = xNew; 
                xPhys = 1 - exp(-this.beta*xTilde) + xTilde * exp(-this.beta);
                this.loopbeta = this.loopbeta + 1; % loopbeta = -2
            else
                % remaining iteration
                xTilde = this.getXTilde(xNew);                
                xPhys = 1-exp(-this.beta*xTilde)+xTilde*exp(-this.beta);
                % sparse to Matrix
                [nely, nelx] = size(xNew);
                xPhys = reshape(xPhys, nely, nelx);
            end
            
            
        end
        
        function dc = filterSensitivities(this, xNew, dc)
            if this.loopbeta == -2
                % first iteration -> loopbeta = -2
                this.loopbeta = this.loopbeta + 1; % -> loopbeta = -1
                xTilde = xNew;
            else
                % remaining iteration
                xTilde = this.getXTilde(xNew);
            end
            dx = this.getDX(xTilde);
            
            % filter sensitivites
            dc(:) = this.H * (dc(:).*dx(:)./this.Hs);
            %dc(:) = dc(:).*dx(:);
            % sparse to Matrix
            [nely, nelx] = size(xNew);
            dc = reshape(dc, nely, nelx);
        end
        
        function dv = filterVolumes(this, xNew, vol)
            if this.loopbeta == -1
                % first iteration -> loopbeta = -1
                xTilde = xNew;
                this.loopbeta = this.loopbeta + 1; % -> loopbeta = 0
            else
                % remaining iteration
                xTilde = this.getXTilde(xNew);
            end
            dx = this.getDX(xTilde);
            % filter volumes
            dv(:) = this.H * (vol(:).* dx(:)./this.Hs);
            %dv(:) = vol(:).*dx(:);
            
            % sparse to Matrix
            [nely, nelx] = size(xNew);
            dv = reshape(dv, nely, nelx);
        end
        
        
        function changeUpdate = updateParameter(this, change, stopCrit)
            % update counter
            this.loopbeta = this.loopbeta + 1;
            
            % update beta
            if this.beta < 512 && (this.loopbeta >= 50 || change <= stopCrit)                
                this.beta = 2 * this.beta;
                this.loopbeta = 0;
                changeUpdate = 1;
                b = this.beta;
                fprintf('Parameter beta increased to %g.\n',b);
            else
                % keep change, if beta is not increased
                changeUpdate = change;
            end
        end
        
        
        
        function w = getWeightFunction(this, distX, distY)
            norm = sqrt(distX^2+distY^2);
            w = this.rMin-norm;
           
%             norm = sqrt(distX^2+distY^2);
%             w = exp((-1/2)*(norm/(this.rMin/3))^2);
        end
        
    end
    
    
    methods (Hidden)
        
        function xTilde = getXTilde(this, xNew)
            xTilde(:) = (this.H * xNew(:))./this.Hs;
        end
        
        
        function dx = getDX(this, xTilde)
            dx = this.beta*exp(-this.beta*xTilde)+exp(-this.beta);
            %dx = 1-exp(-this.beta*xTilde)+xTilde*exp(-this.beta);
            
        end       
    end
    
end

