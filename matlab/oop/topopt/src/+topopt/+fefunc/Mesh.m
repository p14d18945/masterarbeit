classdef Mesh < handle
    % MESH computes all needed infos for the given Domain
    
    properties (Hidden)
        % length in x- and y-direction
        lx        
        ly        
        % wanted elementSize
        h
        % Computed elementSize
        % eS(1)  : elementsize in x-direction
        % eS(2)  : elementsize in y-direction  
        eS
        % number of elements in x- and y-direction
        nelx
        nely
        tMatrix
    end
    
    methods
        function this = Mesh(lx,ly,h)
            % set propertoes
            this.lx = lx;
            this.ly = ly;
            this.h = h;
            
            % initialize properties computation
            this.init;
        end
        
        
        function nN = countNodes(this)
            % compute number of Nodes
            nN = (this.nelx + 1) * (this.nely +1);
        end
        
        function nE = countElements(this)
            % compute number of Elements
            nE = this.nelx * this.nely;
        end
        
        function s = getSize(this)
            % return domainsize
            s = [this.lx, this.ly];
        end
                
        function s = getElementSize(this)
            % return elementsize based on domainsize
            s = this.eS;
        end
        
        function a = getElementAmount(this)
            % return number of Elements in both directions
            a = [this.nelx, this.nely];
        end
        
        function p = getNodePosition(this, i)
            % compute the position for node i
            % eX  : elementsize in x-direction
            % eY  : elementsize in y-direction            
            % p   : position (x,y)
                    
            eX = this.eS(1);
            eY = this.eS(2);
            
            % compute row and column for i
            info = this.getMatrixInfos(i, false);
            column = info(1);
            row = info(2);
                                   
            p = [(column-1) * eX, (row-1) * eY];            
        end
        
        function c = getElementCenter(this,i)
            % computes the center of the i'th element
            % eX  : elementsize in x-direction
            % eY  : elementsize in y-direction            
            % p   : position (x,y)
          
            eX = this.eS(1);
            eY = this.eS(2);
            
            % compute row and column for i
            info = this.getMatrixInfos(i, true);
            column = info(1);
            row = info(2);
                                   
            c = [(column-1) * eX + eX/2, (row-1) * eY + eY/2];  
            
        end
        
        % better: call ones this method and save it
        function T = getTMatrix(this)
            T = this.tMatrix;
        end
    
        
        % possible to optimize with this method, but it's too slow
        function Te = getElementTVector(this, e, Nf)
            nodes = this.computeElementNodes(e);
            % compute T-Vector
            
            % 2D T-Vector (usually)
            if nargin <3
                Nf = 2;
            end
            
            nn =  length(nodes);
            Te = zeros(1, Nf * nn);
            % first sequence 
            subtr = 0:3;
            s1 = (1:4) + subtr.*Nf-subtr;
            
            % second sequence 
            s2 = (1:4)*Nf;
            subtr = Nf-1;
            Te(s1) = nodes*Nf-subtr;
            Te(s2) = nodes.*Nf;
        end
        
        
    end
        
    methods (Hidden)
        
        function init(this)
            % initialize computions of:
            
            % - elementsize based on domain dimensions
            % - number of elements in x and y direction
            
            % lX: length in X-Direction
            % lY: length in Y-Direction
            % lH: desired elementsize
                                    
            lX = this.lx;
            lY = this.ly;
            lH = this.h;
            
            % check if domainsize is possible with choosed elementsize
            % if not, calc. rest of division
            rx = mod((1/lH) * lX, 1);
            ry = mod(1/lH*lY,1);
                        
            % calc. elementsize in x- and y-direction
            sx  = (rx * lH)/(1/lH * lX-rx)+lH;
            sy  = (ry * lH)/(1/lH * lY-ry)+lH;
                                    
            % elementsize
            this.eS = [sx, sy];
            
            % calc. number of elements in x- and y direction
            this.nelx = round(lX / sx);
            this.nely = round(lY / sy);  
            
            % compute T-Matrix once to save time
            nE = this.nelx * this.nely;
            t = zeros(nE,8);
            for e = 1:nE
                t(e,:) = this.getElementTVector(e);
            end
            this.tMatrix = t;            
        end 
        
        function info = getMatrixInfos(this, i, e)
            % calculates the row and column of the mesh-matrix
            % for the i'th element or node
            
            % e: boolean
            %       true = element, calc. with number of Elements 
            %       false = node, calc. with number of Nodes 
                                
            if~e
            k = 1;
            else
            k = 0;
            end
            
            % number of nodes or elements in x-direction   
            nx = this.nelx+k;
             
            % rest after division
            rx  = mod(i/nx, 1);
            
            % filtering row for last node in column
            if rx > 0
                row = i/nx - rx + 1;                
            else
                row = i/nx;                
            end
            
            column = i - (row-1) * nx;  
            
            info = [column, row];
        end  
        
        function n = computeElementNodes(this, e)
            nx = this.nelx;
            i = e-1;
            row  = i/nx - mod((i/nx), 1);
            n = zeros(4,1);
            n(1:2) = row + i + [1,2];
            n(3:4) = row + i + [nx+3,nx+2];
        end
        
    end
end
