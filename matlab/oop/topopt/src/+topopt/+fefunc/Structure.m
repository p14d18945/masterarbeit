classdef Structure < handle
    %STRUCTURE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % m  : mesh object
        m
        % r  : Load vector
        r = [];
        
        % voidElements : Elements with no density
        voidElements = [];
        
        % DOFs with dirichlet Conditions
        dofs = [];
        
        aCe
        
        % Plot Data
        plotTitle = 'System';
        plotMesh = true;
        plotInfo = true;
        plotDisplacement = false;
        plotUScale = 1.0;
    end
    
    properties (Access = private)
        % DirichletCounter
        cntDir = 0;
        % dirchilet entries
        ndofs = [];
        % NeumannCounter
        cntNeu = 0;
        % neumann entries
        nLoadVectors = [];
        % poisson's ration
        mu = 0.3;
        
        % density vector
        x = [];
        % sensitivity vector
        rho = [];
        % mesh color grey = 0.5 / black = 0.1
        meshColor = 0.1;
    end
    
    
    properties(SetAccess=protected, GetAccess=public)
        % dirichlet object-List
        dirichletList = {};
        % neumann objects
        neumannList = {};
        % element stiffness
        ke
    end
    
    methods
        
        function this = Structure(lx, ly, h)
            % initialize Mesh-Object
            this.m = topopt.fefunc.Mesh(lx, ly, h);
            % initialize properties
            this.init();
        end
        
        function addVoid(this, v)
            % v: void-object
            % add current voids to existing voids
            this.voidElements = this.voidElements + v.getElementIDs(this.m);
        end
        
        function dBC = addDirichletBC(this, p1, p2, fixed)
            % update dirichletCounter
            this.cntDir = this.cntDir + 1;
            % initialize BoundaryCondition-Object
            dBC = topopt.fefunc.DirichletBC(p1,p2);
            % set fixed direction
            dBC.setFixed(fixed);
            % save current dirichlet objects
            this.dirichletList{this.cntDir} = dBC;
        end
        
        
        function nBC = addNeumannBC(this, p1, p2, values)
            % update neumannCounter
            this.cntNeu = this.cntNeu + 1;
            % initialize BoundaryCondition-Object
            nBC = topopt.fefunc.NeumannBC(p1,p2);
            % set values
            nBC.setValues(values);
            % save current neumann objects
            this.neumannList{this.cntNeu} = nBC;
        end
        
        function setUP(this)
            % clear Load Vector if setUp is called twice
            this.clearLoadVector();
            % mesh object
            mesh = this.m;
            
            % assemble LoadVector
            if ~isempty(this.neumannList)
                % number of objects
                nObjects = length(this.neumannList(1,:));
                
                for i = 1 : nObjects
                    % initialize load vector
                    rStep = zeros(mesh.countNodes*2,1);
                    % current object
                    nBC = this.neumannList{i};
                    % get Values
                    q = nBC.getValues();
                    for j = 1:length(rStep)/2
                        % IDs for vertical and horizontal DOFs
                        % example:
                        %          dir = [0, 1], node=2
                        %          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
                        id = [(j*2-1), j*2];
                        
                        % get specific node position
                        pos = mesh.getNodePosition(j);
                        if nBC.isOnBoundary(pos)
                            % check if node is on boundary, set dof = true/ false;
                            rStep(id(1),1) =  q(1);
                            rStep(id(2),1) =  q(2);
                        end
                    end
                    % save different entries to plot
                    this.nLoadVectors(i, :) = rStep;
                    % update global load vector
                    this.r = rStep + this.r;
                end
            end
            
            % getDOFs
            if ~isempty(this.dirichletList)
                % number of objects
                nObjects = length(this.dirichletList(1,:));
                
                for i = 1 : nObjects
                    % initialize DOF vector
                    dofsStep = zeros(mesh.countNodes*2,1);
                    dBC = this.dirichletList{i};
                    % get Values
                    dir = dBC.getFixed();
                    for j = 1:length(dofsStep)/2
                        % IDs for vertical and horizontal DOFs
                        % example:
                        %          dir = [0, 1], node=2
                        %          id = [0, 1] .* [(2*2-1), 2*2] = [0, 4]
                        id = dir.*[(j*2-1), j*2];
                        
                        % get specific node position
                        pos = mesh.getNodePosition(j);
                        
                        % check if node is on boundary, set dof = true/ false;
                        dofsStep(id(find(id)),1) = dBC.isOnBoundary(pos); %#ok<FNDSB>
                    end
                    % save different entries to plot
                    this.ndofs(i, :) = dofsStep;
                    % update global DOFs
                    this.dofs(find(dofsStep),1) = true; %#ok<FNDSB>
                end
            end
            
            % find void elements, rowwise
            voidId = find(this.voidElements);
            
            % transpose because matlab fills the matrix columnwise
            xVoid = this.x';
            
            % set void elements rowwise to minDensity
            xVoid(voidId') = 10^-5;
            
            % transpose back
            this.x = xVoid';
        end
        
        function setPoissonsRatio(this, mu)
            % set possions ration
            this.mu = mu;
        end
        
        function setDensityMesh(this, x)
            % insert density mesh to update the structure
            this.x = x;
        end
        
        
        function setRhoMesh(this, rho)
            % insert rho mesh to update the structure
            this.rho = rho;
        end
        
        function x = getDensityMesh(this)
            % return density mesh
            x = this.x;
        end
        
        function laplacianX = getDensityLaplacian(this)
            % elementsize
            eS = this.m.getElementSize();
            
            % DERIV-PART
            xC = this.x;
            
            % NeighbourElements
            [xNorth, xEast, xSouth, xWest] = this.getNeighbourElements(xC);
            
            % Laplacian
            % finite difference scheme laplacian (3 Points)
            ddW = 2*(xWest-xC)/(2*eS(1)^2);
            ddE = 2*(xEast-xC)/(2*(eS(1))^2);
            ddN = 2*(xNorth-xC)/(2*eS(2)^2);
            ddS = 2*(xSouth-xC)/(2*(eS(2))^2);
            
            % apply neumann conditions
            ddW(:,1) = 0;
            ddE(:,end) = 0;
            ddN(1,:) = 0;
            ddS(end,:) = 0;
            
            % compute laplacian
            dd1 = ddE+ddW;
            dd2 = ddN+ddS;
            laplacianX = dd1+dd2+1e-5;
        end
        
        function k = assembleK(this)
            %% initialize datas
            % ke :  elementStiffnessMatrix
            ke = this.stiffnessMatrix();
            % m : meshData
            mesh = this.m;
            
            nN = mesh.countNodes * 2;
            nE = mesh.countElements;
            
            % Assemble K column and rowwise (way faster than elementwise)
            t = mesh.getTMatrix();
            % duplicate 8-times number of nodes per element
            tx = repmat(t',8,1);
            % build column vector
            colT = tx(:);
            
            % transpose
            tt = t';
            % duplicate 8-times number of nodes per element
            tx = repmat(tt(:),1,8).';
            % build row vector
            rowT = tx(:);
            
            % duplicate number of elements-time
            knE = repmat(ke',1,nE);
            % build vector;
            keVec = knE(:);
            
            rho = this.getRhoMesh();
            % transpose
            volTrans = rho';
            
            % duplicate 8x8-times elementstiffness size
            volNodal = repmat(volTrans(:),1,64).';
            % build vector
            volVec = volNodal(:);
            % approximate
            density = volVec.*keVec;
            
            % build sparsed matrix
            k = sparse(rowT ,colT,density,nN,nN);
            
            % pick up true DOFs
            j = find(this.dofs);
            % check if any nodes are fixed
            if isempty(j)
                error('No fixed nodes, check Dirichlet-BCs!')
            end
            % fix DOFs
            for i = j'
                k(i,:) = 0; %#ok<SPRIX>
                k(:,i) = 0; %#ok<SPRIX>
                k(i,i) = 1; %#ok<SPRIX>
            end
        end
        
        function u = computeDisplacement(this)
            % u : displacement vector
            
            kGlob = this.assembleK();
            
            % check if load vector exists
            if isempty(find(this.r, 1))
                error('Loadvector is empty, check Neumann-BCs!')
            end
            
            % compute displacement vectors
            u = kGlob \ this.r;
        end
        
        function [c, gradC] = solve(this)
            % c: compliance
            % gradC: compliance gradient
            
            % displacement
            u = this.computeDisplacement();
            
            % number of elements in both directions
            [nely, nelx] = size(this.x);
            
            % import TMatrix
            t = this.m.getTMatrix();
            
            % import elementstiffness
            ke = this.stiffnessMatrix();
            
            % getRho
            rho = this.getRhoMesh(); %#ok<*PROP>
            % calculate compliance factor
            aC = (u(t)*ke.*u(t));
            % build compliance mat
            cMat = (reshape(sum(aC,2), nelx, nely))';
            % compute piecewise compliance
            aCe = rho.*cMat;
            % summarize all entries to get structures compliance
            c = 1/2 * sum(sum(aCe,2));
            % gradient of compliance
            gradC = 1/2 * cMat;
            
            this.aCe = gradC;
        end
        
        
        function plot(this)
            % get number of elements in x-direction
            % nEdir (nelx, nely)
            nEdir = this.m.getElementAmount();
            l = this.m.getSize();
            % number of elements
            nN = this.m.countNodes();
            cord = zeros(nN, 2);
            
            if this.plotDisplacement
                u = this.computeDisplacement();
                fac = max(u)/max(this.m.getSize());
                uMat = reshape(u, 2, length(u)/2);
                uTran = uMat'.*this.plotUScale*1/(10*fac);
            else
                uTran = zeros(nN, 2);
            end
            
                
            % pick element positions to draw the mesh
            for i = 1:nN
                pos = this.m.getNodePosition(i);
                cord(i,1) = pos(1) + uTran(i,1);
                cord(i,2) = l(2) - (pos(2) + uTran(i,2));
            end
            
            % create matrix [1:nely, 1:nelx] out of vector [1:nE]
            X = reshape(cord(:,1),nEdir(1)+1, nEdir(2) + 1);
            X = X';
            
            Y = reshape(cord(:,2),nEdir(1)+1, nEdir(2) + 1);
            Y = Y';
            
            
            % problem:
            % density matrix is computed for elements
            % coords matrix is computed for nodes
            % -> different size
            
            % solution:
            % add one row and column
            % fill up boundary elements with neighbour density
            mesh = this.x;
            
            B = size(mesh);
            xIDs = 1:B(1);
            yIDs = 1:B(2);
            flipxIDs =xIDs;% fliplr(xIDs);
                                                
            v(xIDs,yIDs) = mesh(flipxIDs,yIDs);
            v(B(1)+1,1:B(2)) = mesh(B(1),:);
            v(1:B(1),B(2)+1) = mesh(:,B(2));
            v(B(1)+1,B(2)+1) = mesh(B(1),B(2));
             

%             POSTPROCESS
%             u = this.computeDisplacement();
%             ux = u(1:2:end-1);
%             uy = u(2:2:end);
%             
%             nE = this.m.getElementAmount();
%             uxMat = reshape(ux, nE(1)+1, nE(2)+1);
%             ux = uxMat';
%             uyMat = reshape(uy, nE(1)+1, nE(2)+1);
%             uy = uyMat';
            
            
            
            % draw System, -v to view the massive elements
            h = figure;
            
            % custom Materialcolor
            
            c = this.meshColor:.1:1;
           % c = .5:.05:1;
            h1 = pcolor(X, Y, -v);
            
            if this.plotMesh == 0
                set(h1, 'EdgeColor', 'none');
            end
            
            map = [c', c', c'];
            colormap(map)
            
            %POSTPROCESS
            %colormap('default');
            
            if this.plotInfo == true
                % computation to draw dirichlets
                dofList  = this.ndofs;
                
                % check if dirichlet boundariess exist
                if ~isempty(dofList)
                    hold on
                    
                    % draw dirichlet line
                    h2 = this.drawLine(dofList, 0);
                    hold on
                end
                
                % computation to draw neumanns
                rList  = this.nLoadVectors;
                
                % check if neumann boundarys exist
                if ~isempty(rList)
                    hold on
                    
                    % draw neumann line
                    h3 = this.drawLine(rList, 1);
                    hold on
                end
            end
            
            axis equal
            axis off
            % set background to white
            set(h,'Color',[1 1 1])
            
            if this.plotInfo == true
                if ~exist('h2','var') && ~exist('h3','var')
                    % plot only mesh if boundary conditions are empty
                    lg = legend(h1, 'MATERIAL');
                    
                elseif exist('h2','var') && ~exist('h3','var')
                    % plot mesh and dirichlet if neumann is empty
                    lg = legend([h1, h2], 'MATERIAL','DIRICHLET');
                elseif ~exist('h2','var') && exist('h3','var')
                    % plot mesh and neumann if dirichlet is empty
                    lg = legend([h1, h3], 'MATERIAL', 'NEUMANN');
                else
                    % plot system with boundary conditions
                    lg = legend([h1, h2, h3], 'MATERIAL','DIRICHLET', 'NEUMANN');
                end
                
                % set legendary background to white
                set(lg,'color',[1 1 1]);
            end
            title([this.plotTitle])
            hold off
        end
        
        
    end
    
    methods (Hidden)
        
        function init(this)
            % number of elements in x- and y-direction
            nE = this.m.getElementAmount;
            % initialize density field
            this.x = zeros(nE(2), nE(1));
            % set density field to 'material'
            this.x(:) = 1; %this.rho = 0.5
            % initialize dofs
            this.dofs = zeros(this.m.countNodes*2,1);
            % initialize voidElements to false
            this.voidElements = zeros(this.m.countElements,1);
            % initialize load vector
            this.r = zeros(this.m.countNodes*2,1);
        end
        
        function clearLoadVector(this)
            % clear load vector
            this.r = zeros(this.m.countNodes*2,1);
        end
        
        function ke = stiffnessMatrix(this)
            % Calculate Stiffnessmatrix for Quad4-Elements
            % Emod: young's modulus
            % h: thickness
            % a: elementhigh
            % b: elementwidth
            Emod = 1.0;
            %Emod = 2.1e11
            v = this.mu;
            eSize = this.m.getElementSize();
            a = eSize(1);
            b = eSize(2);
            h = 1;
            
            
            kloc = [4*a^2+2*b^2*(1-v)   ,  3*a*b*(1+v)/2      , -4*a^2+b^2*(1-v)   , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)   , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v)  ,  3*a*b*(1-3*v)/2   ;
                3*a*b*(1+v)/2       ,  4*b^2+2*a^2*(1-v)  ,  3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2    , -4*b^2+a^2*(1-v)   ;
                -4*a^2+b^2*(1-v)     ,  3*a*b*(1-3*v)/2    ,  4*a^2+2*b^2*(1-v) , -3*a*b*(1+v)/2     ,  2*a^2-2*b^2*(1-v) , -3*a*b*(1-3*v)/2   , -2*a^2-b^2*(1-v)    ,  3*a*b*(1+v)/2     ;
                -3*a*b*(1-3*v)/2     ,  2*b^2-2*a^2*(1-v)  , -3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2      , -2*b^2-a^2*(1-v)   ;
                -2*a^2-b^2*(1-v)     , -3*a*b*(1+v)/2      ,  2*a^2-2*b^2*(1-v) ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v) ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)    , -3*a*b*(1-3*v)/2   ;
                -3*a*b*(1+v)/2       , -2*b^2-a^2*(1-v)    , -3*a*b*(1-3*v)/2   , -4*b^2+a^2*(1-v)   ,  3*a*b*(1+v)/2     ,  4*b^2+2*a^2*(1-v) ,  3*a*b*(1-3*v)/2    ,  2*b^2-2*a^2*(1-v) ;
                2*a^2-2*b^2*(1-v)   , -3*a*b*(1-3*v)/2    , -2*a^2-b^2*(1-v)   ,  3*a*b*(1+v)/2     , -4*a^2+b^2*(1-v)   ,  3*a*b*(1-3*v)/2   ,  4*a^2+2*b^2*(1-v)  , -3*a*b*(1+v)/2     ;
                3*a*b*(1-3*v)/2     , -4*b^2+a^2*(1-v)    ,  3*a*b*(1+v)/2     , -2*b^2-a^2*(1-v)   , -3*a*b*(1-3*v)/2   ,  2*b^2-2*a^2*(1-v) , -3*a*b*(1+v)/2      ,  4*b^2+2*a^2*(1-v)];
            
            
            ke = kloc *(Emod*h/(12*a*b*(1-v^2)));
            this.ke = ke;
        end
        
        function rho = getRhoMesh(this)
            % return rho mesh
            if ~isempty(this.rho)
                rho = this.rho;
            else
                rho = this.x;
            end
        end
        
        function [xN, xE, xS, xW] = getNeighbourElements(this, xC)
            % number of elements in both directions
            [nely, nelx] = size(this.x);
            % number of elements
            nE = nely*nelx;
            
            % initialize
            init = zeros(nely,nelx);
            xN = init;
            xE = init;
            xS = init;
            xW = init;
            
            % ID Arrays
            elemMat = reshape(1:nE, nelx, nely).';
            idNorth = elemMat(1:nely-1,1:nelx);
            idEast =   elemMat(1:nely,2:nelx);
            idSouth =   elemMat(2:nely,1:nelx);
            idWest =   elemMat(1:nely,1:nelx-1);
            
            xTranspose = xC';
            
            % Arrays
            xN(2:nely,1:nelx) = xTranspose(idNorth);
            xE(1:nely,1:nelx-1) = xTranspose(idEast);
            xS(1:nely-1,1:nelx) = xTranspose(idSouth);
            xW(1:nely,2:nelx)  = xTranspose(idWest);
        end
        
        
        function [grad] = approximateGrad(this, xC)
            % Gradient for mesh xC
            
            % elementsize
            eS = this.m.getElementSize();
            % NeighbourElements
            [xNorth, xEast, xSouth, xWest] = this.getNeighbourElements(xC);
            
            % finite difference scheme grad (3 points)
            % W: West, E: East, N: North, S: South
            dW = (xWest-xC)/eS(1);
            dE = (xEast-xC)/eS(1);
            dN = (xNorth-xC)/eS(2);
            dS = (xSouth-xC)/eS(2);
            
            % apply neumann conditions
            dW(:,1) = 0;
            dE(:,end) = 0;
            dN(1,:) = 0;
            dS(end,:) = 0;
            
            % compute grad
            d1 = (dE+dW);
            d2 = (dN+dS);
            grad = (d1.^2+d2.^2).^0.5;
            
        end
        
        function p = drawLine(this,entryList, type)
            % domainSize
            l = this.m.getSize;
            
            % compute number of entries
            nEntries =  length(entryList(:,1));
            
            if this.plotDisplacement
            u = this.computeDisplacement();
            fac = max(u)/max(this.m.getSize());
            ux = u(1:2:end-1).*this.plotUScale*1/(10*fac);
            uy = u(2:2:end).*this.plotUScale*1/(10*fac);
            else
            nN = this.m.countNodes();
            ux = zeros(1, nN);
            uy = ux;
            end
            
            % draw entries
            for j = 1 : nEntries
                % DOFs for current line
                iDs = entryList(j,:);
                
                % filter horizontal and vertical iDs
                h = iDs(1:2:end-1);
                v = iDs(2:2:end);
                
                
                nH = find(h~=0);
                nV = find(v~=0);
                
                % initialize arrays
                % check if horizontal entries exist
                if ~isempty(nH)
                    hPos = zeros(length(nH),2);
                end
                
                % check if vertical entries exist
                if ~isempty(nV)
                    vPos = zeros(length(nV),2);
                end
                
                % node positions
                for i = 1:length(nH)
                    hPos(i,:) = this.m.getNodePosition(nH(i));
                    hPos(i, 1) = hPos(i,1) + ux(nH(i));
                    hPos(i, 2) = uy(nH(i)) + hPos(i,2);
                end
                
                for n = 1:length(nV)
                    vPos(n,:) = this.m.getNodePosition(nV(n));
                    vPos(n, 1) = vPos(n,1) + ux(nV(n)); 
                    vPos(n, 2) = uy(nV(n)) + vPos(n,2);
                end
                
                if type == 0
                    c = 'r';
                else
                    c = 'g';
                end
                
                % check if vertical entries exists in current entry
                if ~isempty(nV)
                    p =  line(vPos(:,1), l(2)-vPos(:,2), 'Color', c,'LineWidth',5);
                    hold on
                end
                
                % check if horizontal entries exists in current entry
                if ~isempty(nH)
                    p = line(hPos(:,1), l(2)-hPos(:,2), 'Color', c,'LineWidth',5);
                    hold on
                end
                
            end
        end
    end
end

