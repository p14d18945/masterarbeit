classdef BoundaryCondition < handle
    % SUPERCLASS for Boundary Conditions
    
    properties
        % p1:   First Point of line
        % p2:   Second Point of line
        p1
        p2
    end
    
    methods
        % Constructor
        function this = BoundaryCondition(p1, p2)
            % set properties
            this.p1 = p1;
            this.p2 = p2;            
        end
      
        function dofs = setDirichletBC(this, fixed, mesh)
            % create dirichlet object
            d = topopt.fefunc.DirichletBC(this);            
            % get fixed degrees of freedom
            dofs = d.getDOFS(fixed, mesh);
        end
        
        function r = setNeumannBC(this, values, mesh)
            % create neumann object
            d = topopt.fefunc.NeumannBC(this);
            % get load vector
            r = d.getR(values, mesh);
        end
       
        function b = isOnBoundary(this, p)
            % check if point is on Boundaryline
            
            % output b: boolean
            % input p: testpoint
            x = [this.p1(1), this.p2(1)];
            y = [this.p1(2), this.p2(2)];
            % acceptable difference
            diff = 1e-4;
            
            if (x(1)-diff <= p(1) && p(1) <= x(2)+diff)  && (y(1)-diff <= p(2) && p(2)<= y(2)+diff)
                b = 1;
            else
                b = 0;
            end
        end        
    end  
end






