classdef Factory < handle
    
    methods (Static)
        function s = getMBBeam(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.05;
            end
            
            s = topopt.fefunc.Structure(3,1,h);
            s.addDirichletBC([3-h,1],[3,1],[0,1]);
            s.addDirichletBC([0,0],[0,1],[1,0]);
            s.addNeumannBC([0,0],[h,0],[0,1]);
            s.setUP();
            s.solve();
        end
        
        function s = getSimpleBeam(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.025;
            end
            ly = 0.4;
            lx = 4*ly;
            
            s = topopt.fefunc.Structure(lx,ly,h);
            s.addDirichletBC([0,ly],[h,ly],[1,1]);
            s.addDirichletBC([lx-h,ly],[lx,ly],[0,1]);
            s.addNeumannBC([lx/2-h,0],[lx/2+h,0],[0,1]);
            s.setUP();
            s.solve();
        end
        
        function s = getSimpleBeamWithVoid(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.01;
            end
            
            s = topopt.fefunc.Structure(2,0.4,h);
            s.addDirichletBC([0, 0.4], [h, 0.4], [1, 1]);
            s.addDirichletBC([2-h, 0.4], [2, 0.4], [0, 1]);
            s.addNeumannBC([0.7, 0], [1.3, 0], [0, 1]);
            v1 = topopt.fefunc.RectangularVoid([0.8, 0.15], [1.2, 0.25]);
            v2 = topopt.fefunc.CircularVoid([0.45,0.2], 0.1);
            v3 = topopt.fefunc.CircularVoid([1.55,0.2], 0.1);
            s.addVoid(v1);
            s.addVoid(v2);
            s.addVoid(v3);
            s.setUP();
            s.solve();
        end
        
        function s = getBendingProblem(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.015;
            end
            lx = 3;
            ly = 3;
            s = topopt.fefunc.Structure(lx,ly,h);
            s.addDirichletBC([lx-1,ly],[lx,ly],[1,1]);
            s.addNeumannBC([0,ly-2-2*h],[0,ly-2],[0,1]);
            v = CircularVoid([0,ly], 2);
            s.addVoid(v);
            s.setUP();
            s.solve();            
        end
        
        function s = getSimpleCanteliver(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.04;
            end
            
            s = topopt.fefunc.Structure(3,1,h);
            s.addDirichletBC([0, 0], [0.0, 1.0], [1, 1]);
            s.addNeumannBC([3, .5-h], [3, .5], [0, 1]);
            s.setUP();
            s.solve();
        end
        
        
        function s = getLShapedCanteliver(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.02;
            end
            lx = 1.3;
            ly = 0.6;
            s = topopt.fefunc.Structure(lx,ly,h);
            s.addDirichletBC([0, 0], [0.4, 0], [1, 1]);
            s.addNeumannBC([lx, 0.2], [lx, 0.2], [0.5, 1]);
            v = topopt.fefunc.RectangularVoid([0.4, 0], [lx, 0.2]);
            s.addVoid(v);
            s.setUP();
            s.solve();
        end
        
        function s = getTShapedCantilever(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.025;
            end
            ly = 1.8;
            lx = 4;
            l1 = 1;
            l2 = 0.8;
            l3 = 0.01*lx;
            l4 = 0.25;
            
            s = topopt.fefunc.Structure(lx,ly,h);
            s.addDirichletBC([lx/2-l1/2,ly],[lx/2+l1/2,ly],[0,1]);
            
            s.addNeumannBC([l3,ly-l2-l4],[l3+h,ly-l2-l4],[0,1]);
            s.addNeumannBC([lx-l3-h,ly-l2-l4],[lx-l3,ly-l2-l4],[0,1]);
            v1 = topopt.fefunc.RectangularVoid([0,ly-l2], [lx/2-l1/2, ly]);
            v2 = topopt.fefunc.RectangularVoid([lx/2+l1/2,ly-l2], [lx, ly]);
            
            s.addVoid(v1);
            s.addVoid(v2);
            s.setUP();
            s.solve();
        end
        
        function s = getDoublyClampedBeam(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.05;
            end
            
            s = topopt.fefunc.Structure(6,1,h);
            s.addDirichletBC([0,0],[0,1],[1,1]);
            s.addDirichletBC([6,0],[6,1],[1,1]);
            s.addNeumannBC([3-h,0.5],[3+h,0.5],[0,1]);
            s.setUP();
            s.solve();
        end
        
        function s = getBridgeLikeProblem(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.05;
            end
            r = 0.15;
            s = topopt.fefunc.Structure(3,1,h);
            s.addDirichletBC([3-1,1],[3-1,1],[1,1]);
            s.addDirichletBC([0,0],[0,1],[1,0]);
            s.addNeumannBC([0,0.5],[3-(1+r),0.5],[0,1]);
            s.addNeumannBC([3-(1-r),0.5], [3,0.5],[0,1]);
            v = topopt.fefunc.CircularVoid([2,0.5], r);
            s.addVoid(v);
            s.setUP();
            s.solve();
        end
        
        function s = getShellLikeProblem(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.05;
            end
            
            ly = 1;
            lx = 3*ly;
            lf1 = 0.35*lx;
            
            
            s = topopt.fefunc.Structure(lx,ly,h);
            s.addDirichletBC([0,ly],[h,lx],[1,1]);
            s.addDirichletBC([lx-h,ly],[lx,ly],[0,1]);
            s.addNeumannBC([lf1-h,2*ly/3],[lf1,2*ly/3],[0,1]);
            s.addNeumannBC([lx-lf1,2*ly/3],[lx-lf1+h,2*ly/3],[0,1]);
            s.setUP();
            s.solve();
        end
        
        function s = getMichellCanteliver(h)
            % h: Mesh-Size
            if nargin < 1
                h = 0.01;
            end
            lx = 1;
            ly = 0.5;
            s = topopt.fefunc.Structure(lx,ly,h);
            
            nn = s.m.countNodes();
            r = 0.175;
            cx = lx-lx/4;
            cy = ly/2;
            for i = 1:nn
                pos = s.m.getNodePosition(i);
                if sqrt((pos(2)-cy)^2+(pos(1)-cx)^2) < r && sqrt((pos(2)-cy)^2+(pos(1)-cx)^2) > r-1.5*h
                    s.addDirichletBC(pos,[pos(1)+h,pos(2)], [1,1]);
                end
            end
            s.addNeumannBC([0.01, ly/2], [0.01+h, ly/2], [0, 1]);
            s.setUP();
            s.solve();
        end
        
    end
end

