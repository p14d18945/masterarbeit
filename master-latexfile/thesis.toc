\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Motivation}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Zielsetzung}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Gliederung der Arbeit}{2}{subsection.1.3}
\contentsline {section}{\numberline {2}Grundlagen der Topologieoptimierung}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Klassifizierung in der Strukturoptimierung}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Mathematisches Optimierungsproblem}{9}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Konventionelle Ans\IeC {\"a}tze}{9}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Finite Element-Methode}{11}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Formulierung der Materialdichte}{11}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}Optimalit\IeC {\"a}tskriterienverfahren}{13}{subsection.2.4}
\contentsline {section}{\numberline {3}Regularisierungsverfahren}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Motivation zur Einf\IeC {\"u}hrung von Filtertechniken}{17}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Sensitivit\IeC {\"a}tenfilter}{18}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Dichtefilter}{20}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Gegen\IeC {\"u}berstellung der Implementierung}{22}{subsection.3.4}
\contentsline {section}{\numberline {4}Thermodynamische Topologieoptimierung}{25}{section.4}
\contentsline {subsection}{\numberline {4.1}Einleitung}{25}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Hamilton-Prinzip}{26}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Evolutionskomponente der internen Variablen}{28}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Variationsrechnung}{31}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Programmablauf}{32}{subsection.4.5}
\contentsline {section}{\numberline {5}Implementierung}{35}{section.5}
\contentsline {subsection}{\numberline {5.1}Grundlegendes}{35}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Objektorientiertes Paradigma}{36}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Architektur des Programms}{37}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Funktionsweise der Klassen}{38}{subsection.5.4}
\contentsline {section}{\numberline {6}Numerische Auswertung}{45}{section.6}
\contentsline {subsection}{\numberline {6.1}Allgemeines}{45}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}MBB-Balken}{45}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Beidseitig eingespannter Balken}{48}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}L-f\IeC {\"o}rmiger Kragbalken}{51}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}SIMP vs. RAMP: Einfacher Balken mit \IeC {\"O}ffnungen}{54}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Zusatzthema: Stochastische Lasten}{57}{subsection.6.6}
\contentsline {subsubsection}{\numberline {6.6.1}T-f\IeC {\"o}rmiger Kragbalken}{58}{subsubsection.6.6.1}
\contentsline {subsubsection}{\numberline {6.6.2}Scheibenf\IeC {\"o}rmige Problemstruktur}{59}{subsubsection.6.6.2}
\contentsline {section}{\numberline {7}Zusammenfassung und Ausblick}{61}{section.7}
\contentsline {subsection}{\numberline {7.1}Zusammenfassung}{61}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Ausblick}{62}{subsection.7.2}
\contentsline {section}{Literaturverzeichnis}{63}{subsection.7.2}
\contentsline {section}{\numberline {A}Herleitungen}{69}{appendix.A}
\contentsline {subsection}{\numberline {A.1}Sensitivit\IeC {\"a}tsanalyse}{69}{subsection.A.1}
\contentsline {subsection}{\numberline {A.2}Thermodynamische Triebkraft}{70}{subsection.A.2}
\contentsline {section}{\numberline {B}Matlab-Code}{72}{appendix.B}
\contentsline {subsection}{\numberline {B.1}Modellierug des L-f\IeC {\"o}rmigen Kragbalkens}{72}{subsection.B.1}
\contentsline {subsection}{\numberline {B.2}Optimierung des L-f\IeC {\"o}rmigen Kragbalkens}{74}{subsection.B.2}
\contentsline {subsection}{\numberline {B.3}Monte-Carlo-Simulation des T-f\IeC {\"o}rmigen Kragbalkens}{77}{subsection.B.3}
\contentsline {section}{Eidesstattliche Erkl\IeC {\"a}rung}{79}{lstnumber.-20.5}
