\section{Thermodynamische Topologieoptimierung}

\begin{quote} 
\textit{Das vierte Kapitel umfasst die Erläuterung eines neuartigen Ansatzes zur Topologieoptimierung, unter Berücksichtigung einer Reduktion der Verzerrungsenergie. Basierend auf den thermodynamischen Prinzipien, wird die Evolutionsgleichung des Verfahrens so formuliert, dass die zusätzliche Implementierung eines Checkerboard Filters hinfällig wird, da die resultierenden optimierten Strukturen ohnehin netzunabhängig sind und keine Schachbrettmuster aufweisen. Darüber hinaus werden grundlegende Komponente des Verfahrens, unter Verweis der dafür relevanten Quellen, zusammengeführt und beschrieben. Weiter wird abschließend gezeigt, dass die Implementierung ähnlich des OC-Verfahrens wesentlich kompakt gehalten werden kann und Parallelen aufweist.} 
\end{quote}

\vspace{0.5cm}
\subsection{Einleitung}
Das Auffinden der optimalen Materialverteilung bei gegebenen Randbedingungen ist das Ziel jeder Topologieoptimierung. Während gängige Ansätze die Topologieoptimierung mit heuristischen Verfahren bewältigen, wird nachfolgend gezeigt, dass eine variationelle Problemformulierung zu einer Evolutionsgleichung führt, die mithilfe einer zeitlichen Entwicklung die Topologieoptimierung vollzieht. Die Grundidee der thermodynamischen Topologieoptimerung (TTO) basiert auf der Umkehrung eines aus der Schadensmechanik resultierenden Materialmodells zur Reduzierung der strukturellen Steifigkeit. Folglich ergibt sich ein Verfahren, durch das die Struktursteifigkeit, wie bei den gängigen Ansätzen  durch eine Materialoptimierung auf makroskopischer Ebene maximiert wird. Während nachfolgend die Grundzüge und der aktuelle Stand der TTO vorgestellt wird, sei an dieser Stelle ausdrücklich erwähnt, dass weitere relevante  Informationen aus den Veröffentlichungen \cite{Junker.2015}, \cite{Junker.2016}, \cite{Jantos.2016}, \cite{Jantos.2018b}, \cite{Jantos.2018}, \cite{Jantos.2019} und \cite{Jantos.2019b}, zur thermodynamischen Topologieoptimierung zu entnehmen sind.\\[1.em]


Da es sich bei der iterativen Topologieoptimierung, wie auch bei der zeitlichen Schadenbeschreibung, um einen dissipativen Ablauf handelt, ist es naheliegend die Evolutionsgleichung der Entwurfsvariablen (wie die internen Variablen zur zeitlichen Schadensbeschreibung) durch das Hamilton-Prinzip für dissipative Kontinua zu bestimmen. 

\subsection{Hamilton-Prinzip}
In Anlehnung an \cite{Junker.}, \cite{Hackl.2008}, \cite{bedford}, \cite{Holzapfel.2010} und \cite{Gelfand.2012} erfolgt eine kompakte Erläuterung des klassischen Hamilton-Prinzips und die Betrachtung relevanter Erweiterungen zur Bestimmung der thermodynamischen Evolutionsgleichung für die Topologieoptimierung.\\[1.em]

Das \textbf{Hamilton-Prinzip} unterliegt Aristotelischer Denkweise hinsichtlich der Bewegung. Jede natürliche Bewegung der irdischen Natur bestrebt stets den Pfad, dessen Wirkung einen stationären Zustand annimmt. Vereinfacht gesagt: Jeder natürlich fallende Stein durchläuft den kürzesten Verbindungsweg zwischen zwei zeitlich bekannten Punkten $\textbf{q}(t_0)$ und $\textbf{q}(t_1)$. Wird ein System in positionsbekannte Starrkörper untergliedert ergibt sich die Wirkung (häufig auch Wirkungsfunktional) 
\begin{align}
\mathcal{F} \left[\textbf{q}(t)\right] = \int\limits_{t_0}^{t_1} \mathcal{L}(t, \dot{\textbf{q}}(t), \textbf{q}(t)) \ dt
\end{align}
 durch das zeitliche Integral der Lagrangefunktion
\begin{align}
\mathcal{L}(t, \dot{\textbf{q}}(t), \textbf{q}(t)) = K(t,\dot{\textbf{q}}(t)) - U(t, \textbf{q}(t))
\end{align}
mit der kinetischen Energie $K$ und der potentiellen Energie $U$. Weiter muss gelten, dass die Starrkörperpositionen des Systems für die fixierten Zeitpunkte $t_0$ und $t_1$ bekannt sind und eine zulässige Variation der Positionen zu null laufen muss (also die Randpositionen auch von der Variation $\delta \textbf{q}$ durchlaufen werden). Um die Aussage treffen zu können, wann das Funktional aus (4.1) einen stationären Zustand in $\textbf{q}(t)$ besitzt, ist es notwendig das G$\hat{a}$teaux Differential (Richtungsableitung) zu bilden und die daraus resultierenden Gleichungen zu null zu setzen (ähnlich der Kurvendiskussion zur Auffindung einer Extremalstelle).
\begin{align}
\delta \mathcal{F} = \int\limits_{t_0}^{t_1} \left( \dfrac{\partial\mathcal{L}}{\partial \dot{\textbf{q}}}\delta \dot{\textbf{q}} + \dfrac{\partial \mathcal{L}}{\partial \textbf{q}}\delta \mathbf{q} \right) dt = 0
\end{align}
Schließlich führt die partielle Integration des zweiten Terms von (4.3) 
\begin{align}
\delta \mathcal{F} &= \int \limits_{t_0}^{t_1} \dfrac{\partial \mathcal{L}}{\partial \textbf{q}}  \delta \textbf{q} \hspace{1mm} dt + \int \limits_{t_0}^{t_1} \dfrac{\partial \mathcal{L}}{\partial \dot{\textbf{q}}}  \delta \dot{\textbf{q}} \hspace{1mm} dt =0\\
&= \int \limits_{t_0}^{t_1} \dfrac{\partial \mathcal{L}}{\partial \textbf{q}}  \delta \textbf{q} \hspace{1mm} dt + \left | \dfrac{\partial \mathcal{L}}{\delta {\dot{\textbf{q}}}}  \delta \textbf{q} \right |_{t_0}^{t_1} - \int\limits_{t_0}^{t_1} \dfrac{\partial}{\partial t}\left( \dfrac{\partial \mathcal{L}}{\partial \dot{\textbf{q}}}\right)  \delta \textbf{q} \hspace{1mm} dt =0
\end{align}
unter Berücksichtigung der zu null laufenden Variation $\delta \textbf{q}(t_0) = \delta \textbf{q}(t_1) = 0$ in den Randpunkten zum verschwinden des Zwischenterms
\begin{align}
\delta \mathcal{F} &= \int \limits_{t_0}^{t_1} \left( \dfrac{\partial \mathcal{L}}{\partial \textbf{q}} -  \dfrac{\partial}{\partial t}\left(\dfrac{\partial \mathcal{L}}{\partial \dot{\textbf{q}}}\right) \right)  \delta \textbf{q} \hspace{1mm} dt = 0 \text{.}
\end{align}
Bei einer genauen Betrachtung von (4.6) wird ersichtlich, dass die Gleichung gemäß des Fundamentallemmas der Variationsrechnung lediglich dann verschwindet, wenn der Integrand null beträgt. Folglich ist es zielführend statt (4.6) den Integranden ohne die Variation $\delta \textbf{q}$
\begin{align}
\dfrac{\partial \mathcal{L}}{\partial \textbf{q}} -  \dfrac{\partial}{\partial t}\left( \dfrac{\partial \mathcal{L}}{\partial \dot{\textbf{q}}}\right)  = 0
\end{align}
zu betrachten und dadurch die bekannte Euler-Lagrange-Gleichung 2.Art (4.7) zu erhalten. Schließlich ist es möglich mithilfe der Euler-Lagrange-Gleichung den zeitlichen Punkt $\textbf{q}$ zu bestimmen, der die Stationarität von (4.1) erzielt.\\[1.em]

Werden \textbf{kontinuierliche Systeme} betrachtet, so empfiehlt es sich in Anlehnung an \cite{Waimann.} und \cite{Junker.} einige thermodynamische Größen einzuführen. Das totale Potential von kontinuierlichen Systemen lässt sich durch die Einführung der Gibbs-Energie
\begin{align}
\mathcal{G} = \Pi_{in} + \Pi_{ex}
\end{align}
elegant formulieren. Weiter folgt, gemäß der thermodynamischen Sichtweise, dass das interne Potential (Formänderungsarbeit) 
\begin{align}
\Pi_{in} = \int \limits_{\Omega} \Psi dV
\end{align} 
durch eine Integration der freien Helmholtz-Energie $\Psi$, über die Umgebung $\Omega$ resultiert, während  das Potential infolge der äußeren Kräften (äußere Arbeit)
\begin{align}
\Pi_{ex} = - \int \limits_{\Omega} \textbf{f} * \textbf{u} \,dV - \int \limits_{\partial\Omega} \textbf{t} * \textbf{u} \,dA
\end{align}
gemäß der mechanischen Arbeit notiert wird. Eine Modifikation der Lagrangefunktion (4.2),  durch das Ersetzen von $U$ $\rightarrow$ $\mathcal{G}$ und das Betrachten des statischen Systemzustandes ($K = 0$) führt letztlich zum \textbf{Hamilton-Prinzip für kontinuierliche und konservative Kontinua}.
\begin{align}
\delta \mathcal{G} = 0 \hspace{2cm} \forall \delta \textbf{u}
\end{align}
An dieser Stelle sei erwähnt, dass für konservative Kontinua die Formänderungsarbeit (4.9) und das Potential der äußeren Kräfte (4.10) zwischen Zustandsbetrachtungen, nicht vom geleisteten Weg abhängen (keine Energie dissipiert).\\[1.em]

Soll die zeitliche Entwicklung der Mikrostruktur eines nicht-konservativen Kontinuums beschrieben werden, so ist es erforderlich, die Gleichung (4.11)
\begin{align}
\delta \mathcal{G} - \delta \mathcal{D} = 0 \hspace{2cm} \forall \delta \textbf{u}\text{,} \hspace{1mm} \delta \textbf{x}
\end{align}
um den Dissipationsanteil $\mathcal{D}$ zu erweitern.
Das führt schließlich zum \textbf{Hamilton-Prinzip für kontinuierliche und nicht-konservative Kontinua} 
\begin{align}
\delta \mathcal{G}  = \delta\mathcal{D} \hspace{2cm} \forall \delta \textbf{u}\text{,} \hspace{1mm} \delta \textbf{x}
\end{align}
mit dem Verschiebungsfeld $\textbf{u}$ und den internen Variablenfeld $\textbf{x}$.

\subsection{Evolutionskomponente der internen Variablen}
Während Abschnitt 4.2 noch einen freien Raum für bestimmte Formulierungen lässt, werden in diesem Abschnitt Bestandteile der Evolutionsgleichung für die internen Variablen $\chi(x) \in \left[ \chi_{min}, 1\right]$ mit $\chi_{min} = 1*10^{-4}$ , unter Berücksichtigung der zuvor eingeführten Begriffe, konkret bestimmt. Die Besonderheit an der variationellen Problemformulierung liegt darin, dass (4.13) problemlos um energetische Funktionale erweitert werden kann, die wiederum die zeitliche Variablenentwicklung beeinflussen. Das für die Topologieoptimierung erweiterte Hamilton-Prinzip für kontinuierliche und nicht-konservative Kontinua 
\begin{align}
\delta \mathcal{G} + \int \limits_{\Omega} \dfrac{\partial \mathcal{D}}{\partial \dot{\chi}} *  \delta \chi\hspace{1mm}dV  + \delta \mathcal{C} + \delta \mathcal{R} = 0 \hspace{2cm} \forall \delta \textbf{u}\text{,} \hspace{1mm} \delta \chi
\end{align}
ergibt sich aus der zusätzlichen Berücksichtigung eines Restriktions- und Regularisierungsfunktionals $\mathcal{C}$, $\mathcal{R}$. Wie zuvor erwähnt liegt der wesentliche Unterschied zwischen der Topologieoptimierung und der Schadensmodellierung darin, dass die Steifigkeit im zeitlichen Entwicklungsprozess maximiert wird, währenddessen defekte in der Mikrostruktur zu einer Reduktion der Steifigkeit führen würden. Folglich wird die freie Helmholtz-Energie als Komponente der Gibbs-Energie (vgl. Abschnitt 4.2)
\begin{align}
\Psi(\chi) = \dfrac{1}{2} * \mathbf{\sigma} : \left[ \mathbb{E}(\chi) \right] ^{-1} :  \mathbf{\sigma}
\end{align}
durch die Materialnachgiebigkeit $\left[ \mathbb{E}(\chi) \right] ^{-1}$ (inverses Schädigungsmodell $\Rightarrow$ inverse Materialsteifigkeit) und den Spannungstensor $\mathbf{\sigma}$ für ein linear elastisches Material formuliert. In Analogie zur SIMP-Dichtefunktion aus Abschnitt 2.3.2 werden die internen Variablen einer Interpolation
\begin{align}
\mathbb{E}(\chi) = \chi^p * \mathbb{E}_0
\end{align}
unterzogen, sodass möglichst keine Zwischenwerte aus dem Evolutionsprozess resultieren.  Währenddessen der Exponent $p$ aus (4.16), für eine Materialinterpolation den Wert $p>1$ besitzen muss, wird mit $\mathbb{E}_0$ der Steifigkeitstensor des Vollmaterials beschrieben. Da der Dissipationsanteil aus (4.14) die Evolutionsgleichung für die internen Variablen $\chi$ unmittelbar beeinflusst, liegt besonders hier ein großes Interesse an der zielführenden Formulierung. In Anlehnung an  \cite{Jantos.2016} wird für die Bestimmung des Dissipationsanteils
\begin{align}
\mathcal{D}(\chi) = \dfrac{1}{2}* \eta * \dot{\chi}^2
\end{align}
ein viskoser Ansatz verwendet, der die  Evolutionsgeschwindigkeit mithilfe der Viskosität 
\begin{align}
\begin{aligned}
\dfrac{\eta}{\Delta t} &= \dfrac{\eta^{\star}}{\Delta t} * p_w\\
\Leftrightarrow \hspace{1cm} \eta &= \eta^{\star} * p_w * \Delta t
\end{aligned}
\end{align}
dämpft. Zur Beschreibung der Viskosität (4.18) dient der viskose Parameter $\eta^{\star}$, die gewichtete Triebkraft $p_w$ und die zeitlichen Schrittweite $\Delta t = 1/n$, als Kehrwert der erforderlichen Schritte 
\begin{align}
n = \left\lceil \dfrac{6}{\eta^{\star}} * \dfrac{\beta^{\star}}{h^2} \right\rceil \text{.} 
\end{align}
Numerische Versuche haben gezeigt, dass zur Bestimmung der Schrittweite (4.19) eine möglichst hohe Effizienz erreicht wird, wenn $\eta^{\star} = 12$ beträgt und der später vorgestellte Regularisierungsparameter $\beta^{\star}$, in Abhängigkeit der quadrierten Elementlänge $h^2$ folgende Bedingung $\beta^{\star} \geq 2 * h^2$ erfüllt. Um mögliche Nebenbedingungen in den Evolutionsprozess der internen Variablen einzubinden, wird zunächst das Restriktionsfunktional, 
\begin{align}
\mathcal{C} = \lambda *  \left(\int_{\Omega} \chi \hspace{1mm} dV - \varrho * \Omega\right) + \int_{\Omega} \gamma * \chi  \hspace{1mm} dV
\end{align}
bestehend aus einem Zielvolumen- und einem  Intervallterm formuliert. Der Zielvolumenterm weist große Ähnlichkeiten zu dem des OC-Verfahrens aus Abschnitt 2.3 - GL.(2.8) auf und wird entsprechend mit den Lagrangemultiplikator für Gleichheitsnebenbedingungen $\lambda$  gekoppelt. Durch das Einbringen des KKT-Parameters 
\begin{align}
\gamma = \begin{cases} \gamma^{\star} &: \dot{\chi} > 0 \hspace{2mm} \wedge \chi = 1 \\ -\gamma^{\star} &: \dot{\chi} > 0 \hspace{2mm} \wedge \chi =\chi_{min} \\ 0 &:  \hspace{5mm} \text{else}\\ \end{cases}
\end{align}

innerhalb des Intervallterms aus (4.20) werden die Karush-Kuhn-Tucker Bedingungen und die Grenzen der Internen Variablen berücksichtigt. Um netzunabhängige und checkerboardfreie Strukturen aus der zeitlichen Variablenentwicklung zu erhalten, hat es sich als zielführend erwiesen, ein Tikhonov-Regularisierungsfunktional
\begin{align}
\mathcal{R}(\chi) = \int_{\Omega} \dfrac{1}{2} * \beta(\chi) * {\mid \nabla \chi \mid}^2 \hspace{1mm} dV
\end{align}
an das Hamilton-Prinzip für kontinuierliche und nicht konservative Kontinua zu koppeln. 
Eine ähnliche Strategie wird bei der gradientenerweiterten Schadensmodellierung vollzogen (siehe \cite{damage} oder \cite{Dimitrijevic.2011}), wobei die Feldregularisierung dort durch eine Erweiterung der freien Helmholtz-Energie innerhalb des Funktionals der Gibbs-Energie erfolgt. Um das Regularisierungsfunktional (4.22) in der Schreibweise kompakt und übersichtlich zu halten, beinhaltet der Faktor
\begin{align}
\beta(\chi) = \beta^{\star} * p_w(\chi)
\end{align}
neben den in GL. (4.19) vorgestellten Regularisierungsparameter $\beta^{\star}$, ebenfalls die gewichtete thermodynamische Triebkraft  
\begin{align}
p_w = \dfrac{\int_{\Omega}g(\chi) * p_x(\chi_e)}{\int_{\Omega}g(\chi)}\text{,}
\end{align}
um die Tikhonov-Regularisierung für die TTO zu vervollständigen. Die Idee hinter der Triebkraftwichtung liegt darin, mithilfe einer Wichtungsfunktion
\begin{align}
g(\chi) = \left( \chi - \chi_{min} \right)*\left(1 - \chi \right)
\end{align} 
die Triebkraft $p_x(\chi)$ der Elemente anhand ihrer Dichte zu skallieren. Die Wichtungsintensität erreicht den maximalen Wert für zwischenverteilte Variablen und nimmt zum 0/1-Randbereich ab, da Elemente mit einer derartigen Dichte weniger Modifikationsbedarf besitzen. Die Herleitung der Triebkraft
\begin{align}
p_\chi(\chi) = \dfrac{1}{2*\Omega_e} *  \textbf{u}_e^T(\chi)\dfrac{\partial \textbf{K}_e}{\partial \chi}\textbf{u}_e(\chi)
\end{align}
lässt sich aus dem Anhang (A.2 Thermodynamische Triebkraft) entnehmen.

\newpage
\subsection{Variationsrechnung}
Nach dem erfolgreichen erläutern des Hamilton-Prinzips für kontinuierliche und nicht-konservative Kontinua (vgl. Abschnitt 4.2) und dem kompakten zusammenführen relevanter Evolutionskomponente (siehe Abschnitt 4.3), ist es nun erforderlich die Evolutionsgleichung mithilfe der Variationsrechnung zu bestimmen. Eine Betrachtung, des in (4.14) notierten Problems offenbart, dass die Variation für das unbekannte Verschiebungsfeld $\textbf{u}$ und für die interne Variable $\chi$ berechnet werden muss. Da weder das Restriktionsfunktionfunktional, noch die Regularisierungskomponente aus (4.14) eine Abhängigkeit von $\textbf{u}$ besitzen, ergibt sich folglich die Variation des Verschiebungsfelds 
\begin{align}
\delta_\textbf{u} \mathcal{G} = \int_{\Omega} \dfrac{\partial \Psi}{\partial \mathbf{u}}  \delta \mathbf{u} \hspace{1mm} dV - \int_{\Omega} \mathbf{f} * \delta \mathbf{u} \hspace{1mm} dV - \int_{\partial \Omega} \mathbf{t} * \delta \mathbf{u} \hspace{1mm} dA = 0 
\end{align}
analog zum Prinzip der virtuellen Arbeit. Die Gleichung (4.27) lässt sich ohne weiteres mithilfe eines Finite-Element Programms lösen und wird dementsprechend nicht genauer untersucht. Analog zu \cite{Jantos.2019} wird die Variation der internen Variable, der Übersicht halber, komponentenweise dargestellt.
\begin{align}
\delta_\chi \mathcal{G} &= \int_{\Omega} \dfrac{\partial \Psi}{\partial \chi} * \delta \chi \hspace{1mm} dV\\
&\backup\begin{aligned}
\int_{\Omega} \dfrac{\partial \mathcal{D}}{\partial \dot{\chi}} * \delta \chi \hspace{1mm} dV  &= \int_{\Omega} \dfrac{\partial}{\partial \dot{\chi}} \left( \dfrac{1}{2} \eta * \dot{\chi}^2\right) * \delta \chi \hspace{1mm} dV\\
&= \int_{\Omega} \eta * \dot{\chi} * \delta \chi \hspace{1mm} dV\\
\end{aligned}\\
&\backupC\begin{aligned}
\delta_{\chi}\mathcal{C} &= \dfrac{\partial}{\partial \chi}\left( \lambda * \left( \int_{\Omega} \chi \hspace{1mm} dV - \varrho*\Omega\right)+ \int_{\Omega} \gamma* \chi\hspace{1mm}dV\right) * \delta \chi\\
&=\int_{\Omega} \left(\lambda * 1 + \gamma * 1 \right) * \delta \chi \hspace{1mm} dV\\
&=\int_{\Omega} \left(\lambda  + \gamma \right) * \delta \chi \hspace{1mm} dV\\
\end{aligned}\\
&\backupR\begin{aligned}
\delta_{\chi}\mathcal{R} &=\int_{\Omega} \dfrac{\partial}{\partial \chi}\left( \dfrac{1}{2} * \beta * {\vert \nabla \chi \vert}^2 \right)  \nabla \delta \chi \hspace{1mm} dV\\
&= \int_{\Omega} \beta * \nabla \chi  \nabla \delta \chi \hspace{1mm} dV\\
&= \int_{\partial \Omega} \beta * \nabla \chi  \mathbf{n} * \delta \chi \hspace{1mm} dA  - \int_{\Omega} \beta  * {\nabla}^2\chi * \delta_{\chi} \hspace{1mm} dV
\end{aligned} 
\end{align}
Die Gleichung (4.31) resultiert aus der partiellen Integration von $\delta_\chi \mathcal{R}$ und ergibt sich unter Einführung des Normalenvektors $\textbf{n}$ sowie des Laplace-Operators ${\nabla}^2$. Eine Zusammenführung der Gleichungen (4.28) - (4.31) in einem zu null gestellten Term ergibt schließlich die Evolutionsgleichung für die interne Variable $\chi$, resultierend aus der schwache Form des Hamilton-Prinzips für kontinuierliche und nicht-konservative Kontinua
\begin{align}
\begin{aligned}
\int_{\Omega} \dfrac{\partial \Psi}{\partial \chi} * \delta \chi \hspace{1mm} dV &+ \int_{\Omega} \eta * \dot{\chi} * \delta \chi \hspace{1mm} dV + \int_{\Omega} \left(\lambda + \gamma \right) * \delta \chi \hspace{1mm} dV +\\ 
&+ \int_{\partial \Omega} \beta * \nabla \chi  \mathbf{n} * \delta \chi \hspace{1mm} dA  - \int_{\Omega} \beta  * {\nabla}^2\chi * \delta_{\chi} \hspace{1mm} dV = 0\text{.}
\end{aligned}
\end{align}
Aufgrund der Unabhängigkeit des Volumen- und Oberflächenintegrals, lässt sich die Gleichung (4.32) in zwei separat zu verschwindende Gleichungen zerlegen.
\begin{align}
&0 \in \dfrac{\partial \Psi}{\partial \chi} + \eta * \dot{\chi} + \lambda + \gamma - \beta  * {\nabla}^2\chi  &\forall \textbf{x} \in \Omega\\
&0 = \beta * \nabla \chi  \mathbf{n} &\forall \textbf{x} \in \partial\Omega
\end{align}
Während die Gleichung (4.33) für den Entwicklungsprozess der internen Variable genutzt werden kann, bietet (4.34) die zu erfüllende Neumann-Randbedingung. Um die Gleichung (4.33) mithilfe eines FE-Ansatzes berechnen zu können, ist es vorab notwendig die Gleichung für diskrete Systeme zu notieren. Schließlich folgt, in Anlehnung an \cite{Jantos.2019}, die diskrete Schreibweise für die Evolutionsgleichung der internen Variable 
\begin{align}
\chi \in \dfrac{1}{\eta}\left(p_\chi-\lambda - \gamma + \beta * {\nabla}^2\chi \right)\text{.}
\end{align}
Weiter sei erwähnt, dass der Kuhn-Tucker Parameter $\gamma$ nicht explizit berechnet werden muss, sondern durch min/max-Funktionen ersetzt werden kann und folglich aus der Gleichung (4.35) entfällt.\\

\subsection{Programmablauf}
Ein Merkmal der thermodynamischen Topologieoptimierung liegt darin, dass die optimale Materialverteilung des diskretisierten Kontinuums durch einen natürlichen Wachstumsprozess bestimmt wird und nicht infolge eines Optimierungsverfahrens resultiert. Lediglich die Berücksichtigung eines zu erbringenden Zielvolumens führt dazu, dass der Lagrangemultiplikator $\lambda$ in Gleichung (4.35) einfließt und ähnlich des OC-Verfahrens (siehe Abschnitt 2.4) mithilfe eines Bisektionsverfahrens bestimmt wird. Entsprechend der Abbildung 4.1 wird ersichtlich, dass die äußere Schleife zur FE-Analyse dient, während in der inneren Schleife die internen Variablen berechnet werden. Außerdem empfiehlt es sich, die Nachbarbeziehungen der Elemente einmalig vor der Iteration zu berechnen, um  während des Evolutionsprozesses den Laplace mithilfe eines finiten Differenzenschemas effizient zu approximieren. Aufgrund der Tatsache, dass das die Evolutionsgleichung in der inneren Schleife lediglich für variierende Lagrangemultiplikatoren  mehrfach bestimmt wird, werden die Triebkraft $P_x(\chi^j)$ und der Laplace ${\nabla}^2 \chi^j$ vorab berechnet und als Konstanten im Wachstumsprozess der inneren Schleife betrachtet.\\[0.5em]

\hrule 
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.564\textwidth]{pictures/section_4/flowchart}
	 	
	\caption[Flowchart: TTO]{Flowchart TTO}
\end{figure}
\vspace{-2mm} 
\hrule	
\vspace{4mm}
Vergleicht man den Programmablauf der TTO (Abbildung 4.1) gegenüber dem des OC-Verfahrens (Abbildung 2.8) verdeutlichen sich noch einmal die Gemeinsamkeiten der aus unterschiedlicher Motivation hergeleiteten Ansätze. Um Oszillationen im Konvergenzbereich zu reduzieren wird in \cite{Jantos.2019} vorgeschlagen nach zwei aufeinanderfolgenden annähernd gleichwertigen Iterationen, bezogen auf der freien Helmholtz-Energie (4.15), den Wachstumsprozess der internen Variablen abzubrechen.
\begin{align}
\dfrac{\mid c_i - c_{i-1} \mid}{c_i} < c_{tol}
\end{align}
Gegenüber des Abbruchkriteriums aus (2.18) stellt (4.36) ein weniger striktes Kriterium dar und führt folglich auch beim OC-Verfahren zu einer Minimierung der Konvergenzzeit. Die innere Schleife des Programmablaufs (Abbildung 4.1) untergliedert sich in zwei wesentliche Abschnitte (siehe Algorithm 3, gemäß \cite{Jantos.2019}).

\vspace{4mm}
  \begin{algorithm}[H]\small
\textbf{Initialze:}\\ $\lambda = 0$; \\ $\lambda_{lower} = \text{min}\lbrace P_{x} + \beta * {\nabla}^2\chi^j  \rbrace - \eta$;\\ $\lambda_{upper} = \text{max}\lbrace P_{x} + \beta * {\nabla}^2\chi^j  \rbrace + \eta$; \\
		\While{$\mid\varrho_{trial} - \varrho \mid$ > $10^{-7}$ }
			{ 
			\ForEach{element}{%
			$\chi_{trial} = \dfrac{1}{\eta}\left(p_\chi-\lambda + \beta * {\nabla}^2\chi \right)$;\\	
			$\chi^{j+1}$ = max$\lbrace \chi_{min}$, min$\lbrace 1$, $\chi_{trial}\rbrace \rbrace$;\\		
			}
			$\varrho_{trial} = \dfrac{1}{\Omega}  {\displaystyle \int_{\Omega}}\chi^{j+1} \hspace{1mm}dV$;\\	
			\uIf{$\varrho_{trial} > \varrho$}{
			$\lambda_{lower} = \lambda$}%
			\ElseIf{$\varrho_{trial} < \varrho$}{
			$\lambda_{upper} = \lambda$
			}
			$\lambda = \dfrac{1}{2}\left(\lambda_{upper} - \lambda_{lower} \right)$ 				
			}
			    
    \caption{Inner loop with evolution equation and bisection method}
\end{algorithm}
\vspace{4mm}
Der erste Abschnitt dient zur Ermittlung der internen Variablen, während der zweite Abschnitt den Lagrangemultiplikator mithilfe eines Bisektionsverfahrens modifiziert, bis das Zielgewicht erreicht ist.

\newpage 
