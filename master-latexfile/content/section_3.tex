\section{Regularisierungsverfahren}
\begin{quote} 
\textit{Das vorliegende Kapitel umfasst die Erläuterung und eine anschließende Vorstellung von Regularisierungsverfahren zur Handhabung numerisch-, instabilitätsgesteuerter Optimierungsergebnisse.} 
\end{quote}

\vspace{0.5cm}
\subsection{Motivation zur Einführung von Filtertechniken}
Wie in Kapitel 2 erläutert, basiert das OC-Verfahren auf einer heuristischen Vorgehensweise zur Auffindung des Optimums der Zielfunktion. Bei der Verwendung dieses Ansatzes wurde bereits früh festgestellt, dass gerade im Konvergenzbereich numerische Instabilitäten auftreten und die Zielfunktion osziliert. Detaillierte Informationen zu der Problematik lassen sich beispielsweise aus \cite{checkerboard} oder \cite{Diaz.1995} entnehmen. Beobachtungen zeigen, dass gerade die Tragwerksmodellierung durch 4-knotige Elemente mit linearen Ansatzfunktionen dazu führt, dass die optimierte Struktur ein Schachbrettmuster (\textit{Checkerboard}), also hochgradig alternierende Dichteverhältnisse zwischen den Nachbarelementen aufweist (siehe Abbildung 3.1). Die Wahl von hochwertigen Ansatzfunktionen oder mehr als 4-knotigen Elementen führt zwar zur Verhinderung von Checkerboards, jedoch ist dies mit einer erheblichen Steigerung der Rechenzeit verbunden. Eben diese Tatsache motiviert zur Einführung von Regularisierungverfahren, mit dessen Hilfe die Schachbrettmuster in jeder Iteration der äußeren Optimierungsschleife sukzessiv regularisiert werden.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.5\textwidth]{pictures/section_3/checkerboard}
	 	
	\caption[Checkerboard-Phänomen]{Checkerboard-Phänomen}
\end{figure}
\vspace{-2mm} 
\hrule	
\vspace{4mm}

In Hinblick zur effizienten Bewältigung des Checkerboard-Phänomens dient der Ansatz aus der Arbeit \cite{sigmund.}, also die Nutzung von Pixelfiltern in Anlehnung an die digitale Bildverarbeitung. Dort ist es gängig, Grafikfehler in Fotos, wie zum Beispiel Bildrauschen oder defekte Pixel mithilfe von Filtern zu beheben, um die Fotografie im gesamten zu verbessern. Bezogen auf die Checkerboard-Problematik aus der Topologieoptimierung, bedeutet die Nutzung von Pixelfiltern, dass anstelle der Pixel eines Bildes, Elementzellen einer Struktur gefiltert werden. Bevor eine detaillierte Erläuterung erfolgt, gilt es zu erwähnen, dass die Pixelfilter sich in zwei wesentliche Unterkategorien klassifizieren lassen. An Dieser Stelle sei explizit erwähnt, dass die in Abschnitt 3.2 und 3.3 vorgestellten Ansätze lediglich einen Teilbereich der bis heute entwickelten Verfahren darstellen. In Anlehnung, an \cite{Sigmund.2007} wird dies jedoch als ausreichend und repräsentativ empfunden, da eine nahezu vollständige Erfassung der bis heute entwickelten Pixelfilter, einer sehr intensiven Recherchearbeit bedarf, wobei selbst dann keine Garantie zur Gesamterfassung gegeben ist. 

\subsection{Sensitivitätenfilter}
Die erste und wesentliche Unterkategorie von Pixelfiltern in der Topologieoptimierung bildet der Sensitivitätenfilter nach \cite{sigmund.} und \cite{sensitivityF}. Die Grundidee des Sensitivitätenfilters liegt darin, dass infolge des gradientenbasierten Optimierungsverfahrens die Sensitivitäten der Zielfunktion unmittelbar als Maß zur Modifikation des Entwurfsvektors genutzt werden können. Regularisiert werden die Vektoreinträge insofern, dass die alternierenden Sensitivitäten in einem vorbestimmten Radius $r_{min}$ zusammengefasst und gleichmäßig verteilt werden (vgl. Abbildung 3.2), um nachfolgend mithilfe des heuristischen Update-Schemas den Entwurfsvektor zu bestimmen.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.55\textwidth]{pictures/section_3/weightingFunc}
	 	
	\caption{Filterradius $r_{min}$ und Wichtungsfunktion/-kegel $w(x_i)$ vom Element $x_e$}
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}
Zur Berücksichtigung, inwieweit ein Nachbarelement $x_i$ für die Sensititätenmodifikation des betrachteten Elements $x_e$ infrage kommt, dient die Wichtungsfunktion 
\begin{align}
w(x_i) = max\begin{cases}r_{min} - \Vert x_i-x_e \Vert\\0 \end{cases} \hspace{2cm} w(x_i):\mathbb{R} \rightarrow \mathbb{R}\text{.}
\end{align}
Die in der Gleichung 3.1 zu berechnende euklidische Norm bewerkstelligt die Erfassung der jeweiligen Distanz zwischen den Zentren des i-ten Elements $x_i$ und des betrachteten Elements $x_e$. Für den Fall, dass die euklidische Norm zwischen den betrachteten Elementen größer als der Filterradius wird, nimmt die Wichtungsfunktion $w(x_i)$ den Wert null an und vernachlässigt demzufolge Elemente, außerhalb des Radius für die Modifikation der Sensitivitäten wie folgt
\begin{align}
\dfrac{\widehat{\partial f_0}}{\partial x_e} = \dfrac{\sum_{i \in N_e} w(x_i)*x_i*\dfrac{\partial f_0}{\partial x_i}}{x_e * \sum_{i \in N_e} w(x_i)}\text{.}
\end{align}
Ein weiterer Grund zur Einführung des Sensitivitätsfilters liegt in der Generierung von optimierten, netzunabhängigen (\textit{mesh-independence}) Strukturen. Während die ungefilterten Entwürfe (Abbildung 3.3, linke Seite) zwar grundsätzlich die gleichen Konturen, bei einer variierenden Netzdichte aufweisen, besitzen sie bei genauerer Betrachtung unterschiedliche Verzweigungen. Dies hat zur Folge, dass das gleiche System bei einer variierenden Netzdichte mehrere Lösungen besitzen kann und es nicht direkt ersichtlich wird, welche Lösung eine höhere Qualität besitzt. Wie zu erwarten, zeichnen sich hingegen die gefilterten Strukturen (Abbildung 3.3, rechte Seite) durch netzunabhängige Entwürfe, also prinzipiell gleiche Materialpfade bei unterschiedlichen Netzdichten aus.\\[1.em]

\hrule
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.47\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_3/checkerboard}
\vspace{2mm}       

       \includegraphics[width=\linewidth]{pictures/section_3/unfiltered_checkerboard}
    \end{minipage}% <- sonst wird hier ein Leerzeichen eingefügt
    \hspace{0.7cm}
    \begin{minipage}[t]{0.47\linewidth}
         \centering
        \includegraphics[width=\linewidth]{pictures/section_3/sensFilter1}
        \vspace{2mm}       
        
        \includegraphics[width=\linewidth]{pictures/section_3/sensFilter}
    \end{minipage}    
    \caption{Sensitivitätenfilter zur Regularisierung des Schachbrettmusters}
\hrule
\vspace{4mm} 
\end{figure} 
  
\subsection{Dichtefilter}
Bevor eine Erläuterung von bestimmten Dichtefiltern erfolgt, ist es notwendig, etwas grundlegendes zu erwähnen, denn wie in \cite{Sigmund.2007} beschrieben, handelt es sich bei den gefilterten Größen, entgegen der Namensgebung, nicht um die Elementsdichte, sondern lediglich um die Einträge des Entwurfsvektors. Letztlich hat sich die Namensgebung dennoch durchgesetzt, da die gefilterten Designvariablen als Eingangsparameter für die Dichtefunktion $\rho(x_i)$ dienen und infolgedessen das Dichtefeld regularisieren. Aufgrund dieser Tatsache und zum Verständnis im Hinblick auf die Konvention, wird folglich ebenfalls von Dichtefiltern gesprochen.\\[0.5em]

\textbf{Bruns-Tortorelli Dichtefilter:}
Die Grundidee, die Ansätze des Sensitivitätenfilters direkt auf den Entwurfsvektor zu übertragen und infolgedessen eine Regularisierung durch die Filterung der Designvariablen zu bezwecken wurde insbesondere durch die Arbeit  \cite{densityF} eingeführt und in mathematischer Sicht durch \cite{bourdin} verifiziert. Gemäß des Ansatzes ergibt sich die gefilterte Entwurfsvariable des $e$-ten Elements
\begin{align}
\widehat{x}_e = \dfrac{\sum_{i \in N_e} w(x_i)*x_i*v_{0i}}{\sum_{i \in N_e} w(x_i)*v_{0i}}
\end{align}
unter Einbindung der Grundvolumina $v_{0i}$, der i-ten Elemente. Des Weiteren empfielt \cite{densityWeight} eine Modifikation der linearen Wichtungsfunktion $w(x_i)$ aus (3.1), unter der Anwendung der Gaußschen Glockenkurve

\begin{align}
w(x_i) = max\begin{cases} e^{-\dfrac{1}{2}\left( \dfrac{\Vert x_i-x_e \Vert}{\sigma_d}\right)^2}\\0 \end{cases}
\end{align}
mit der Varianz $\sigma_d = r_{min}/3$. Die Abwandlung hat zur Folge, dass die Nachbarelemente für den Regularisierungsprozess nicht linear einfließen und dementsprechend eine schärfere Wichtung stattfindet. Da die i-ten Sensitivitäten im heurischtischen Update, also in der inneren Iteration (vgl. Abschnitt 2.4, Abbildung 2.8), zur Ermittlung des (i+1)-ten Entwurfsvektors dienen, ist es vor dem Update erforderlich, die Sensitivitäten der Ziel- und Restriktionsfunktion  
\begin{align}
\dfrac{\partial f_k}{\partial x_e} = \sum_{i \in N_e} \dfrac{ w(x_i)*v_{0i}*\dfrac{\partial f_k}{\partial \widehat{x}_i}}{\sum_{i \in N_e} w(x_i)*v_{0i}} \hspace{3cm} k \in [0,1]
\end{align}
unter Anwendung der Kettenregel neu zu bestimmen. Abschließend gilt zu erwähnen, dass zur Ermittlung des Strukturvolumens $V(\textbf{x})$, aus der Restriktionsfunktion (siehe Abschnitt 2.3, Gleichung 2.8), anstelle des Entwurfsvektors $\textbf{x}$ der modifizierte Vektor $\widehat{\textbf{x}}$ verwendet werden muss.\\[1.em]

\textbf{Heaviside Dichtefilter:}
Eine Klassifikation des Bruns-Tortorelli Ansatzes bildet der Heaviside Filter nach \cite{heaviSideFilter}. Charakterisierend für die Namensgebung wird eine approximierte Heaviside Funktion
\begin{align}
\overline{x}_e = 1-e^{-\beta*\widehat{x}_e}+\widehat{x}_e*e^{-\beta}
\end{align}
zur Modifikation der mit Gl. (3.3) gefilterten Designvariablen $\widehat{x}_e$ genutzt. Die Approximation der Funktion wird so vollzogen, dass $\beta \in [1, 2, 4, ..., 512]$ in der Optimierung inkrementell gesteigert wird, um schließlich hinreichend genau gegen der Heaviside Funktion zu konvergieren (vgl. Abbildung 3.4).  
\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.9	\textwidth]{pictures/section_3/heavisidefunc}
	 	
	\caption{Approximation der Heaviside Funktion}
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}
Die jeweiligen Sensitivitäten nach den Entwurfsvariablen 
\begin{align}
\dfrac{\partial f_k}{\partial x_e} = \sum_{i \in N_e} \dfrac{\partial f_k}{\partial \overline{x}_i} \dfrac{\partial \overline{x}_i}{\partial \widehat{x}_i}\dfrac{\partial \widehat{x}_i}{\partial x_e} \hspace{3cm} k \in [0,1]
\end{align}
ergeben sich durch die Berücksichtigung der Kettenregel und die Einführung der partiellen Ableitung von $\overline{x}_i$
\begin{align}
\dfrac{\partial \overline{x}_i}{\partial \widehat{x}_i} = \beta * e^{-\beta*\widehat{x}_i}+ e^{-\beta}\text{.}
\end{align}

Eine weitere Eigenschaft des Heaviside-Ansatzes liegt darin, dass mit größer werdenden $\beta$ die Einhaltung des Zielvolumens (berücksichtigt als Konvergenzkriterium des heuristischen Updateverfahrens) fehlschlägt. Um die Konvergenz der inneren Iterationsschleife dennoch zu gewährleisten wird ein Verhältnis der Lagrange-Multiplikatoren 
\begin{align}
\dfrac{\lambda_2-\lambda_1}{\lambda_1+\lambda_2} > 1*10^{-3}
\end{align}
als Konvergenzkriterium, in Anlehnung an \cite{.d}, eingeführt.

\subsection{Gegenüberstellung der Implementierung}
Wie in den vorangestellten Abschnitten des 3. Kapitels angedeutet unterscheiden sich die vorgestellten Regularisierungsverfahren nicht wesentlich. Letztlich lassen sich die meisten der entwickelten Checkerboard-Filter als Modifikation des Sensitivitätenfilters aus Abschnitt 3.2 darstellen. Die Ähnlichkeit des Sensitivitäten- und Bruns-Tortorelli Dichtefilters verdeutlicht sich noch einmal bei einer Betrachtung der Implementierung in dem Optimalitätskriterienverfahren aus Abschnitt 2.4 (gemäß Algorithm 1).

\vspace{4mm}
  \begin{algorithm}[H]\small
\textbf{Initialze:} Designvector: $\textbf{x}$, $cnt = 1$, $change = 1$\\
		\While{change > stopCrit}
			{ 
			- apply: $\rho(\textbf{x})$, see (2.10) or (2.12)\\
			- solve: FE-Problem\\
			- calc.: $f_{0,1}$, $\dfrac{\partial f_{0,1}}{\partial x_i}$, see (2.4) and (2.6)\\
			- apply: $\dfrac{\partial \rho}{\partial x}$, see (2.11) or (2.13)\\
			- filter: Sensitivities, see (3.2) or (3.5)
			
			\While{innerloop did not converge}{
			- calc.: Designvariables $x^{i+1}$\\
			(- filter: Densities, see (3.3), \textit{only for densityfilter})\\
			- calc.: Lagrange multiplier $\lambda$\\				}			
			- update: change, see (2.18)\\
			- update: $cnt = cnt + 1$\\			
			}    
    \caption{OC-Method with Sensitivity- or Bruns-Tortorelli Densityfilter}
\end{algorithm}
\vspace{4mm}
Der Pseudocode unterscheidet sich in der zehnten Zeile insofern, dass die Entwurfsvariablen unter Regularisierung des Dichteansatzes dort gefiltert werden müssen, während die Zeile für den Sensitivitätenfilter vollständig entfällt.

\newpage

Wird der Regularisierungsprozess im OC-Verfahren mithilfe des Heaviside Dichtefilters vollzogen (siehe Algorithm 2) ergibt sich eine nicht wesentlich kompliziertere Implementierung gegenüber des Bruns-Tortorelli Dichtefilters.

\vspace{4mm}
\begin{algorithm}[H]\small
\textbf{Initialze:} Designvector: $\textbf{x}$, $cnt = 1$, $change = 1$, $\beta = 1$, $betaloop = 0$\\
		\While{change > stopCrit}
			{ 
			- apply: $\rho(\textbf{x})$, see (2.10) or (2.12)\\
			- solve: FE-Problem\\
			- calc.: $f_{0,1}$, $\dfrac{\partial f_{0,1}}{\partial x_i}$, see (2.4) and (2.6)\\
			- apply: $\dfrac{\partial \rho}{\partial x}$, see (2.11) or (2.13)\\
			- filter: Sensitivities, see (3.7) with (3.8)
			
			\While{innerloop did not converge}{
			- calc.: Designvariables $x^{i+1}$\\
			- filter: Densities, see (3.3)\\
			- apply: Heavisidefunc., see (3.6)\\
			- calc.: Lagrange multiplier $\lambda$\\				}			
			- update: change, see (2.18)\\
			- update: $cnt = cnt + 1$\\	
			- update: $betaloop = betaloop + 1$\\
			\If {$\beta \leq 512$ \textbf{and} ($betaloop \geq 50$ \textbf{or} $change \leq stopCrit$)}{
			- increase: $\beta = \beta * 2$\\
			- set: $betaloop = 0$\\
			- set: $change = 1$\\		
			}		
			}    
    \caption{OC-Method with Heaviside Densityfilter}
\end{algorithm}
\vspace{4mm}
Vielmehr lässt sich sagen, dass die Heavisidefunktion in der elften Zeile, den Bruns-Tortorelli Filter erweitert und einzig die inkrementelle Steigerung von $\beta$ (Zeile 17-21) sowie die dafür erforderlichen variablen ($\beta$, $betaloop$) neu hinzu kommen. Eine rekursive Duplizierung von $\beta$ wird nur dann vollzogen, wenn der Zwischenschrittzähler $betaloop$ fünfzig Iterationen überschreitet oder die äußere Schleife das Konvergenzkriterium erreicht. Weiter gilt zu beachten, dass $\beta = 512$ als Obergrenze eingeführt wird, da weitere Steigerungen von $\beta$ zu keiner signifikanten Effizienzsteigerung der Optimierung führen. Um den Optimierungsprozess nach 50 Iterationen oder beim erreichen des Konvergenzkriteriums weiter aufrecht zu erhalten, werden beide Parameter ($betaloop$, $change$) der if-Bedingung (Zeile 17) nach einer Steigerung von $\beta$ gezielt verletzt.
\newpage