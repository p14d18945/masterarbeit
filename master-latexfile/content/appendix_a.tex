\section{Herleitungen}
\subsection{Sensitivitätsanalyse}
Die Sensitivätsanalyse erfolgt in der Topologieoptimierung typischerweise in Anlehnung an 
\cite{Bendse.2004} mithilfe der Adjungierten Methode.
Anstelle dieses Verfahrens wird hier gezeigt, dass die Differenzierung der Zielfunktion $f_{0}(\textbf{x}):\mathbb{R}^{n} \rightarrow \mathbb{R}$, also die Ableitung der Verzerrungsenergie nach den Entwurfsvariablen ebenso durch fundamentale Ansätze der Mathematik hergeleitet werden kann.\\

Ausgehend von der FE-Grundgleichung in Abhängigkeit des Entwurfsvektors $\textbf{x}$,

\begin{align}
\textbf{K}(\textbf{x}) \hspace{1mm} \textbf{u}(\textbf{x}) = \textbf{f}
\end{align}

wird die partielle Differenzierung nach den Entwurfsvariablen $x_i$ unter Anwendung der Produktregel auf beiden Seiten der Identität (A.1) vollzogen

\begin{align}
\left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x}) + \textbf{K}(\textbf{x})\left(\dfrac{\partial \textbf{u}}{\partial x_i}\right) = 0\text{.}
\end{align}

Die Umformung von (A.2) ergibt 
\begin{align}
\textbf{K}(\textbf{x})\left(\dfrac{\partial \textbf{u}}{\partial x_i}\right) = - \left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x})
\end{align}
durch die beidseitige Multiplikation mit der inversen Steifikeitsmatrix, die partielle Ableitungen der Verschiebungen nach den Entwurfsvariablen $x_i$
\begin{align}
\left(\dfrac{\partial \textbf{u}}{\partial x_i}\right) = -\textbf{K}^{-1}(\textbf{x}) \left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x})\text{.}
\end{align}

Die Sensitivitäten der Zielfunktion 

\begin{align}
\begin{aligned}
\dfrac{\partial f_0}{\partial x_i} &= \dfrac{1}{2} * \dfrac{\partial \textbf{f}^T \textbf{u}}{\partial x_i}\\ 
&= \dfrac{1}{2} * \textbf{f}^T \dfrac{\partial \textbf{u}}{\partial x_i}\\
&= \dfrac{1}{2} * \textbf{u}^T(\textbf{x}) \textbf{K}(\textbf{x}) \dfrac{\partial \textbf{u}}{\partial x_i}
\end{aligned}
\end{align}
lassen sich durch das Einsetzen der Gleichung (A.4) in (A.5) zu der konventionelle Darstellung umformen 

\begin{align}
\begin{aligned}
\dfrac{\partial f_0}{\partial x_i} &=\dfrac{1}{2} *  \textbf{u}^T(\textbf{x}) \textbf{K}(\textbf{x}) \left( -\textbf{K}^{-1}(\textbf{x}) \left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x}) \right)\\
&=\dfrac{1}{2} *  \textbf{u}^T(\textbf{x}) \left(-\left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x}) \right)\\
&=-\dfrac{1}{2} *  \textbf{u}^T(\textbf{x})\left(\dfrac{\partial \textbf{K}}{\partial x_i}\right)\textbf{u}(\textbf{x})\text{.}\\
\end{aligned}
\end{align} 

Das Einführen einer Dichtefunktion $\rho(x_i)$ erleichtert die Ableitung der globalen Steifigkeitsmatrix
\begin{align}
\dfrac{\partial f_0}{\partial x_i} = -\dfrac{1}{2} * \sum_{i \in N_e}\textbf{u}_i^T(x_i) \hspace{1mm} * \dfrac{\partial \rho}{\partial x_i}* \textbf{k}_0 \hspace{1mm} \textbf{u}_i(x_i)
\end{align} 
im Assemblierungsprozess über alle Elemente $N_e$, durch die Multiplikation mit der Elementsteifigkeitsmatrix $\mathbf{k_0}$.\\[1.em]

\subsection{Thermodynamische Triebkraft}
Die in der thermodynamischen Topologieoptimierung (Kapitel 4) verwendete Triebkraft $px$ lässt sich unmittelbar aus dem zweiten Hauptsatz der Thermodynamik herleiten. Da der thermodynamische Optimierungsansatz die Grundsätze der Schadensmodellierung auf die Topologieoptimierung erweitert, lässt sich folglich die Herleitung der Triebkraft aus den zweiten Hauptsatz der Thermodynamik detailliert in Publikationen zur Materialmodellierung \cite{Junker.}, \cite{Waimann.}, \cite{Schwarz.} wiederfinden und wird an dieser Stelle nicht ein weiteres mal notiert. Grundsätzlich gilt jedoch, dass die Triebkraft des e-ten Elements
\begin{align}
\begin{aligned}
p_\chi(\chi) &= \dfrac{1}{\Omega_{e}}\int_{\Omega_e} - \dfrac{\partial \Psi}{\partial \chi} \hspace{1mm} dV\\
&= \dfrac{1}{\Omega_{e}}\int_{\Omega_e} -\dfrac{1}{2} * \mathbf{\sigma} *    \dfrac{\partial \left[ \mathbb{E} \right] ^{-1}}{\partial \chi} * \mathbf{\sigma} \hspace{1mm} dV
\end{aligned}
\end{align} 
aus dem Integral der negativen Ableitung der freien Helmholtz-Energie (gemäß Kapitel 4 - Gl. 4.15) nach der internen Variable $\chi$ erfolgt. Unter Berücksichtigung des linear-elastischen Materialverhaltens lässt sich (A.8) weiter spezifizieren 
\begin{align}
\begin{aligned}
p_\chi(\chi) &=  \dfrac{1}{2 * \Omega_{e}}\int_{\Omega_e} \mathbf{\epsilon} * \mathbb{E} * \left[ \mathbb{E} \right] ^{-1} * \dfrac{\partial \left[ \mathbb{E} \right]}{\partial \chi} * \left[ \mathbb{E} \right] ^{-1} * \mathbb{E} * \mathbf{\epsilon} \hspace{1mm} dV \\
&= \dfrac{1}{2*\Omega_e} *  \textbf{u}_e^T(\chi)\dfrac{\partial \textbf{K}_e}{\partial \chi}\textbf{u}_e(\chi_e)\text{.}\\
\end{aligned}
\end{align}
Entgegen klassischen Optimierungsansätzen handelt es sich bei (A.9) nicht um eine Zielfunktion zum minimieren, sondern um eine Größe um einen natürlichen Wachstumsprozess zu steuern.