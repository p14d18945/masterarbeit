\section{Implementierung}

\begin{quote} 
\textit{Das folgende Kapitel dient als Erläuterung des entwickelten Konzeptes zur Implementierung der vorangestellten Vorgehensweisen für die Topologieoptimierung, insbesondere der unterschiedlichen Regularisierungsverfahren.} 
\end{quote}

\vspace{0.5cm}
\subsection{Grundlegendes}
Im Rahmen einer Softwareentwicklung gibt es elementare Phasen, die zu durchlaufen sind. Verdeutlicht wird dies mit Hilfe eines sogenannten Wasserfallmodells (siehe Abb. 5.1). Es beruht auf dem Grundgedanken, dass die Fließrichtung ähnlich wie bei einem Wasserfall klar vorgegeben ist und die Phasen aufeinander folgen (vgl. \cite{BrandtPook.2015}, S. 23).\\ 

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.62\textwidth]{pictures/section_5/informatik/waterfall}
	 	
	\caption[Wasserfallmodell in der Softwareentwicklung]{Wasserfallmodell in der Softwareentwicklung aus \cite{BrandtPook.2015}}	
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}


Während der \textbf{Initialisierung} wird festgehalten, welche Ziele die zu entwickelnde Software besitzt. Die \textbf{Analyse} dient zur Erfassung der zu erbringenden Leistungen, um diese Ziele umzusetzen (siehe \cite{Boles.2013}, S. 23). Folglich dient die Phase des \textbf{Entwurfs} zur Modellierung des Problems und zur Konzipierung der  zugrundeliegenden Softwarearchitektur. Eine gängige Architekturbeschreibungssprache (ADL) ist hierbei die Unified Modelling Language, kurz UML (seit 2005 UML 2.0) (siehe \cite{Ernst.2015}, S. 766). Bei der \textbf{Implementierung} wird die Software-Architektur in eine Programmiersprache umgesetzt und dadurch erstmalig die Software erstellt. Zum Prüfen, ob das Programm mangelhaft arbeitet (durch eine fehlerhafte Implementierung oder ähnliches) wird eine \textbf{Test}-Phase durchlaufen, in der jede Komponente des Programms Minimaltests erfolgreich zu absolvieren hat. Durch die fortlaufende Verbesserung und \textbf{Wartung} des Programms wird der problemfreie \textbf{Betrieb} gewährleistet.  
Jeder dieser einzelnen Komponente trägt mehr oder weniger zur Entscheidung welche Programmiersprache für das Projekt Anwendung findet mit bei. Da die zu bewerkstelligende Problemstellung dieser Arbeit stark mathematisch-numerisch orientiert ist, wird es als naheliegend empfunden die Programmiersprache und gleichnamige -umgebung \textsc{Matlab} (MATrix LABoratory) zu verwenden. Dabei handelt es sich um eine Programmiersprache der vierten Generation (4GL). Gegenüber Sprachen der dritten Generation (3GL) (Java, C, C$^{++}$, ...) ist es hiermit möglich durch weniger, jedoch prägnanten Zeilen Code die gleiche Programmkomplexität wie mit einer 3GL zu erhalten (siehe \cite{Pietruszka.2014}, S.6). Dies verdeutlicht sich vornehmlich bei größeren Projekten mit immer wiederkehrenden Kontrollstrukturen. Darüber hinaus bietet \textsc{Matlab} gegenüber den 3GLs den Vorteil, dass Variablen eines bestimmten Datentyps nicht zu deklarieren sind, sondern unmittelbar nacheinander eines Wertes unterschiedlichen Typs zugewiesen werden können (vgl. \cite{Pietruszka.2014}, \cite{Thuselt.2013}).

\subsection{Objektorientiertes Paradigma}
Maßgebend für die Effizienz der Software ist das zugrundeliegende Programmierparadigma. Eine Recherche in dem Gebiet verdeutlicht direkt, welches Paradigma ab teils komplexen Problemstellungen Oberhand gewinnt. So sind sich die meisten Autoren in diesem Punkt einig und schreiben unter anderem folgendes: \frqq Eine Methode des Systementwurfs, die heute weit verbreitet ist, ist der objektorientierte Entwurf.\flqq \hspace{1mm}(aus \cite{Muller.2015}, S. 137) oder \frqq Die objektorientierte Programmierung (OOP) ist heute das am weitesten verbreitete Programmierparadigma.\flqq \hspace{1mm}(siehe \cite{Ernst.2015}, S. 667). Die weite Verbreitung der objektorientierten Programmierung lässt sich unter genauer Betrachtung der folgenden Eigenschaften (vgl. \cite{CarstenHeniger.21.6.2000}, S. 10) und aus den implizierenden Vorteilen gegenüber der prozeduralen Programmierung  schlussfolgern.

\begin{itemize}
\item \textbf{Flexibilität:} Die Dekomposition eines Hauptproblems in Teilprobleme und Teilkomponente ermöglicht eine evolutionäre Arbeitsweise. Darüber hinaus wird auch die parallele Problembewältigung durch mehreren Personen realisierbar.
\item \textbf{Geringere Fehleranfälligkeit:} Da das Problem nicht mehr als Ganzes bewältigt werden muss, hat dies zur Folge, dass die zu durchlaufende Testphase objektspezifisch formuliert und bewältigt werden kann. Dies führt zu einer drastischen Reduktion der Fehlanfälligkeit. 
\item \textbf{Modularität:} Die zur Verfügung stehenden Abstraktionsprinzipien der OOP (Klassifizierung, Kapselung, Aggregation, Vererbung und Polymorphismus) (vgl. \cite{MatthiasBaitsch.2003}, S. 82) ermöglichen die Formulierung einer kompakten Struktur, die problemlos erweitert werden kann. Treten an diversen Stellen mehrfach ähnliche Codefragmente auf, so sollte einmalig eine Methode dafür formuliert werden die an den etwaigen Stellen durch einen Aufruf den Code ersetzt. Handelt es sich um eine Ansammlung mehrere Methoden mit der gleichen Eigenschaft, wie zum Beispiel Vektoren, Matrizen und lineare Gleichungssysteme, so wäre es denkbar, diese in einer Klasse lineare Algebra zusammenzufassen.  
\end{itemize}

Das in dieser Arbeit entwickelte Konzept zum Vergleich verschiedener Regularisierungsverfahren unterliegt aufgrund der oben genannten Vorteile der OOP und der zeitgemäßen Behandlung komplexer Probleme, typischerweise eines objektorientierten Paradigmas.  


\subsection{Architektur des Programms}
Aufgrund des Umfangs der zu implementierenden Software untergliedert sich die Architektur des Programms im wesentlichen in zwei Bestandteile (siehe Abbildung 5.2).\\ 

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.38\textwidth]{pictures/section_5/informatik/topopt_package}
	 	
	\caption{Progammarchitektur in Komponentendarstellung}	
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}

Die \textbf{Systemkomponente} beinhaltet typische Klassen zur Erstellung eines FE-Modells (Mesh, BoundaryConditions, Structure,...). In der dazugehörigen Analysephase (siehe 5.1) wurde viel Wert darauf gelegt, dass der Funktionalitätsumfang keine Oberhand gewinnt, da die Komplexität der zu modellierenden Systeme sich in Grenzen hält. Um die zuvor erstellte Struktur zu optimieren wird dessen Instanz an der \textbf{Optimierungskomponente} übergeben. Die Optimierungskomponente untergliedert sich wiederum, im wesentlichen in eine Filterklasse (\textit{Filter}) und einer Klasse zur Definition der Optimierungsmethode (\textit{OptimizationMethod}). Während mithilfe der Filterklasse festgelegt wird, welcher Dichte- und Regularisierungsfilter Anwendung findet, fließen alle relevanten Randbedingungen zum Optimierungsverfahren und zur Schrittweitenvorschrift in der \textit{OptimiziationMethod} ein.\\

\subsection{Funktionsweise der Klassen}
Um die objektorientierte Programmierung zu bewerkstelligen ist es vorab notwendig, das Hauptproblem (Topologieoptimierung) so zu zerlegen und zu reduzieren, dass die wesentlichen Merkmale in Klassen zusammengefasst werden können. Mit größer werdender Komplexität des Hauptproblems bietet es sich wiederum an, viele Klassen zur Formulierung von Teilproblemen in sogenannten Paketen zusammenzufassen. Dem  Abschnitt  5.3 zu entnehmen untergliedert sich das Hauptproblem der vorliegenden Arbeit in ein System-Paket und ein Optimierungs-Paket. Die dem Systempaket unterliegenden Klassen sind im Klassendiagramm der Abbildung 5.3 dargestellt.\\[0.5em]

\textbf{Structure:} Fundamentale Merkmale und erforderliche Prozesse zum Berechnen einer FE-Struktur, wie beispielsweise das Assemblieren der Steifigkeitsmatrix oder auch das Berechnen der Knotenverschiebungen, sind in der Strukture-Klasse in einzelnen Methoden festgehalten. Aufgrund der in Abschnitt 2.3.1 gezeigten Definition, dass eine Struktur über eine Rechteckumgebung definiert wird, vereinfacht sich die Formulierung der Strukturvisualisierung insofern, dass keine weitere Visualizer-Klasse benötigt wird und folglich die Plot-Methode zur Systemdarstellung in der Structure-Klasse mit einfließt.\\[0.5em]

\textbf{Mesh:} Mithilfe der Mesh-Klasse wird die Rechteckumgebung der Struktur definiert und die Netzdichte bestimmt. Weiter bietet die Klasse Methoden an, um interne Netzinformationen abzurufen, wie beispielsweise die Anzahl der Elemente oder auch Knoten, um interne Prozesse (Assemblierung der Steifigkeitsmatrix, Visualisierung des Systems,...) zu beschleunigen.\\ [.5em]

\textbf{Void:} Um Hohlräume in der Struktur zu ermöglichen, wird mithilfe der Void-Klasse festgelegt, welche Elemente (\textit{void elements}) für die zuvor definierten Hohlräume reserviert werden und für die zielführende Materialverteilung der Struktur nicht herangezogen werden dürfen. Während ein Rechteckhohlraum (\textit{RectangularVoid}) über den unteren linken Eckpunkt $\textbf{p1}$ und den oberen rechten Eckpunkt $\textbf{p2}$ definiert wird, lässt sich ein kreisförmiger Hohlraum (\textit{CircularVoid}) durch den Mittelpunkt $\textbf{c}$ und einen Radius $r$ bestimmen.\\[.5em]

\textbf{BoundaryCondition:} Um eine möglichst hohe Flexibilität der Software zu gewährleisten liegt ein besonderes Interesse an der abstrakten Formulierung der Klasse zur Konstruktion der Randbedingungen (\textit{BoundaryConditions}). Um Punkt- und Linienbedingungen auf dem Randgebiet und auch in der Struktur definieren zu können, wird in einem ersten Schritt eine gerade durch die Punkte $\textbf{p1}$ und $\textbf{p2}$ konstruiert. In dem darauffolgenden Schritt wird geprüft, ob Elementknoten auf der zuvor konstruierten Linie liegen. Ist dies der Fall, wird der Knoten entsprechend der Randbedingung (\textit{DirichletBC}, \textit{NeumannBC}) behandelt (Freiheitsgrad sperren, Eintrag im Lastvektor,...). \\


\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.92\textwidth]{pictures/section_5/klassen/system}
	 	
	\caption{Klassendiagramm Systempaket}	
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}
Um die Klassen des Optimierungspakets zu erläutern, ist es der Übersicht halber empfehlenswert, die Filterklassen und die Klassen der Optimierungsmethode separat zu betrachten. Die Abbildung 5.4 zeigt den Informationsfluss der wesentlichen Hauptklassen vom Optimierungspaket.\\


\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.9\textwidth]{pictures/section_5/klassen/topOptProblem}
	 	
	\caption{Aufbau des Optimierungspakets}	
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}
Das der Optimierung unterliegende Problem wird mithilfe der TopOptProblem-Klasse definiert und durch eine unidirektionale Assoziation für die OptimizationMethod-Klasse zugänglich gemacht. Darüber hinaus enthält die Problemklasse eine Methode zur Ergänzung von Dichte- und Regularisierungsfiltern, um verschiedene Filtertechniken gezielt vergleichen zu können. Wird das Zielvolumen der Struktur nicht explizit definiert, so beträgt dies standardmäßig 50 Prozent des Startvolumens. Weiter gilt zu erwähnen, dass die oberen und unteren Schranken der Entwurfsvariablen (oder auch interne Variablen gemäß TTO aus Kapitel 4) ebenfalls vordefiniert sind und folglich nicht zwingend neu festgelegt werden müssen. Entsprechend des UML-Diagramms aus der Abbildung 5.5, werden die Filterklassen implementiert. Um mögliche Gemeinsamkeiten der jeweiligen Filter nicht mehrfach zu kodieren und folglich die Gesamtstruktur kompakt zu halten, werden OOP-typisch, mehrfach auftretenden Methoden einmalig in der Oberklasse kodiert und mithilfe der Vererbung an die Unterklassen weitergegeben. Um die Effizienz des Regularisierungsprozesses zu steigern, erfolgt der Filterprozess mit einer vor der Optimierung initialisierten Koeffizientenmatrix. Folglich wird in den Unterklassen der Regularisierungsfilter, entgegen \cite{.e}, keine Filterschleife benötigt, da der Filterprozess durch eine Multiplikation mit der Koeffizientenmatrix erfolgt.\\[1.em]


\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.94\textwidth]{pictures/section_5/klassen/filterclasses}
	 	
	\caption{UML-Diagramm der Filterklassen}	
\end{figure}
\hrule   


\newpage
Um die in Abschnitt 2.4 und Kapitel 4 vorgestellten Ansätze zur Bewerkstelligung einer Topologieoptimierung in der gleichen Umgebung zu programmieren, empfiehlt es sich vorab zu prüfen, welche Gemeinsamkeiten die unterschiedlichen Ansätze aufweisen.\\

\hrule
\begin{figure}[!h]
	\centering	
	 	\includegraphics[width=0.86\textwidth]{pictures/section_5/klassen/optimizationMethod}	
	 	 	
	\caption{Klassendiagramm der OptimizationMethod}	
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}  

Letztlich führt ein Vergleich der jeweiligen Flowcharts (siehe Abbildung 2.8 und Abbildung 4.1) dazu, dass der thermodynamische Evolutionsprozess als modifiziertes OC-Verfahren angesehen werden kann. Selbstverständlich unterliegen die Bestandteile der TTO und des OC-Verfahrens zwar anderen Herleitungen, allerdings ist es ohne weiteres möglich den 99-Zeilen Code aus \cite{.e} so zu modifizieren, dass die TTO, statt dem OC-Verfahren Anwendung findet. Aufbauend auf dieser Idee, wird folglich nur noch hinsichtlich der Schrittweite kategorisiert und die, der Optimierung unterliegende Struktur (in Anlehnung an den 99 Zeilen Code), als \textit{OCMethod} anerkannt. Ein vollständiges Klassendiagramm der OptimizationMethod-Klassen bietet die Abbildung 5.6.\\
