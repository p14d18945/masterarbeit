\section{Grundlagen der Topologieoptimierung}

\begin{quote} 
\textit{In diesem Kapitel wird Grundlegendes zur Topologieoptimierung erläutert. Dies beinhaltet unter anderem die mathematische Darstellung des zugrunde liegenden Optimierungsproblems, die konventionelle Vorgehensweise zur Lösung sowie mögliche Probleme die dabei entstehen können. Speziell verwiesene Quellen dienen hierbei dem Leser zur umfangreicheren Recherche, da einige Bausteine als bereits bekannt vorausgesetzt werden.} 
\end{quote}

\vspace{0.5cm}
\subsection{Klassifizierung in der Strukturoptimierung}
Zur allgemeinen Aufarbeitung der Topologieoptimierung ist es vorweg notwendig, die übergeordnete Strukturoptimierung anhand der untergeordnete Optimierungsstrategien zu charakterisieren. Prinzipiell gilt, jede Klassifikation der Strukturoptimierung hat das Ziel einen Startentwurf hinsichtlich der Zielfunktion und Nebenbedingungen zu optimieren, bis das Abbruchkriterium eintritt und im besten Fall ein Optimum erreicht wird. Bei den Klassifikationen der Strukturoptimierung existieren folgende Konzepte:\\[1.1em]

\textbf{Topologieoptmierung} (\textit{Topology optimization}): Diese Optimierungsstrategie entscheidet über die grundsätzlichen Konfiguration des Tragwerks. Bei der Topologieoptimierung wird unterschieden zwischen heuristischen, geometrischen und materiellen Methoden (vgl. Abbildung 2.1), wobei der letztere Ansatz sich noch einmal in materiell diskrete und materiell kontinuierliche Verfahren aufteilt (vgl. \cite{Schwarz.2001}, S. 31). Die heuristik-basierte Topologieoptimierung findet besonders im Bereich der Crashoptimierung Anwendung. Fachwissen darüber lässt sich unter anderem aus \cite{ortmann.}, \cite{Ortmann.2014} oder auch \cite{Dienemann.} entnehmen. Geometrisch-basierte Methoden bewerkstelligen die Optimierung durch das Einbringen von Löchern in der Struktur. Die Hohlräume werden mit Hilfe der Formoptimierung solange variiert,  bis eine Verbesserung der Zielfunktion eintritt (siehe dazu auch \cite{maute} oder \cite{geometricTopOpt}). Die in dieser Arbeit angewandte Methode zur Optimierung, also die materielle Topologieoptimierung, respektive des kontinuierlichen Ansatzes (oder auch kontinuierliche Layout-Optimierung genannt, wie beispielsweise in \cite{sigmund.} oder \cite{Bendsoe_Kikuchi}), hat das zugrundeliegende Ziel, die optimale Materialverteilung zu einem Zielvolumen mit gegebenen Randbedingungen (Neumann- und Dirichlet-Bedingungen) im vordefinierten Entwurfsraum zu finden. Der indiskrete Optimierungsansatz kann mit Scheiben, Platten, Schalen und dreidimensionaler Kontinua angewendet werden. In der vorliegenden Arbeit wird analog, zu \cite{maute}, \cite{sigmund.}, \cite{Bendsoe_Kikuchi}, \cite{.e} und \cite{.d} eine Tragwerksmodellierung mit Scheibenelemente vollzogen, wobei das zugrunde liegende Ziel der Optimierung die Minimierung der Verzerrungsenergie (\textit{Compliance}) umfasst. Das minimieren der Verzerrungsenergie wird aufgrund der kompakteren Implementierung und der Äquivalenz in statischer Hinsicht, zur Maximierung der Systemsteifigkeit, in dieser Arbeit, wie auch typischerweise in der Topologieoptimierung bevorzugt.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.78\textwidth]{pictures/section_2/topologieoptimierung/topOpt}
	 	
	\caption[Methoden der Topologieoptimierung]{Methoden der Topologieoptimierung aus \cite{maute}}
\end{figure}
\vspace{-2mm} 
\hrule	

\vspace{1.25cm}
\textbf{Formoptmierung} (\textit{Shape optimization}): Im Grunde genommen verfolgt diese Strategie den Ansatz, dass zu einem Tragwerk mit bekannter Topologie parametrische Größen der Geometrie modifiziert werden, um eine optimierte Form zu erhalten. Zur nachfolgenden Verdeutlichung wird die Eingrenzung getroffen, dass sich die Formoptimierung auf Stabtragwerke und nicht auf Schalenstrukturen bezieht. Als geometrische Größen, zur Modifikation der Tragwerksform dienen hierbei üblicherweise Gelenke. Wird ein Gelenk in seiner Position verändert, so hat dies zur Folge, dass zugleich die Länge der anschließenden Stäbe variiert und infolgedessen eine neue Systemgeometrie entsteht. Typischerweise ist das zugrunde liegende Optimierungsziel die Reduktion der Verzerrungsenergie unter der Berücksichtigung von weiteren Nebenbedingungen. Die Abbildung 2.2 zeigt den Startentwurf, also das Ausgangstragwerk und stellt das aus der Formoptimierung erhaltene System gegenüber. Wie in der Grafik zu sehen ist, werden sechs Entwurfsvariablen benötigt um das Tragwerk zu optimieren. Bei komplexen Strukturen wird empfohlen die Variablen zu koppeln, Symmetrien zu berücksichtigen und zur Geometriebeschreibung Freiformkurven zu verwenden (siehe \cite{MatthiasBaitsch.2003}, S. 20).

\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/shapeOptimizationStart}
        Startentwurf
    \end{minipage}% <- sonst wird hier ein Leerzeichen eingefügt
    \hspace{0.8cm}
    \begin{minipage}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/shapeOptimizationFinished}
     Optimiertes Tragwerk    
    \end{minipage}    
    \caption[Darstellung einer Formoptimierung mit BOfem]{Darstellung einer Formoptimierung mit BOfem\footnotemark}
\hrule 
\end{figure}
\footnotetext{Hierbei handelt es sich um ein MATLAB-basiertes FEM-Paket, welches von Baitsch entwickelt wird. Die genaue Vorgehensweise zur Tragwerksmodellierung ist in \cite{MatthiasBaitsch.09.01.2018} erläutert.}

\vspace{0.78cm}
\textbf{Querschnittsoptmierung} (\textit{Size optimization}): Wird der optimale Querschnitt bei einer bekannten Geometrie gesucht, spricht man von einer Querschnittsoptimierung (siehe Abbildung 2.3). Gerade die Optimierung von überwiegend druckbeanspruchten Stabtragwerken profitiert von der Querschnittsoptimierung, da es möglich ist, Nebenbedingungen wie unter anderem einen Beulnachweis oder auch einen Stabilitätsnachweis (Knicknachweis) im Optimierungsprozess zu berücksichtigen. Als Zielfunktion ist es hier ebenfalls möglich, die Verzerrungsenergie oder auch gegenüber der reinen Formoptimierung das Tragwerksgewicht zu verwenden.\\[0.5em]

\hrule
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.4\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/sizeOptimizationStart}
        Startentwurf
    \end{minipage}% <- sonst wird hier ein Leerzeichen eingefügt
    \hspace{0.8cm}
    \begin{minipage}[t]{0.4\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/sizeOptimizationFinished}
     Optimiertes Tragwerk    
    \end{minipage}    
    \caption[Darstellung einer Querschnittsoptimierung mit BOfem]{Darstellung einer Formoptimierung mit BOfem}
\hrule
\vspace{4mm} 
\end{figure}

\newpage
\textbf{Materialoptmierung} (\textit{Material optimization}): Werden materielle Parameter, wie die Faseranordnung oder auch die Materialauswahl (Stahl, Aluminuium, Holz) variiert, um eine Optimierung des Tragwerks, beziehungsweise der zugrunde liegenden Zielfunktion zu erzielen, so spricht man von einer Materialoptimierung.\\[1em] 

Gerade die Formoptimierung von Stabtragwerken wird in der Praxis häufig mit der Querschnittsoptimierung kombiniert. Das hat unter anderem den Vorteil, dass das System im gänzlichen effizienter gegenüber der alleinigen Form- oder Querschnittsoptimierung ausgenutzt wird. Die Grundlagen hierfür können unter anderem aus \cite{MatthiasBaitsch.2003}, \cite{Lawo.1987}, \cite{Papageorgiou.2015} oder auch \cite{Schumacher.2013} entnommen werden. Die Abbildung 2.4 zeigt eine kombinierte Form- und Querschnittsoptimierung eines Fachwerkbogens. Das besondere hierbei ist, dass zur Geometriebeschreibung Methoden aus den \textbf{C}omputer \textbf{A}ided \textbf{G}eometric \textbf{D}esign verwendet werden. Aufgrund der schlanken druckbeanspruchten Bauweise weist das Tragwerk eine nicht
vernachlässigbare Imperfektionsempfindlichkeit auf. Um eine effiziente Optimierung zu vollziehen, wird in jeder Iteration eine skallierte Imperfektion äquivalent zur ersten Knickfigur des Startentwurfs sukzessiv auf den Optimierungsentwurf addiert.\\[0.5em]

\hrule
\begin{figure}[h]
    \centering
    \begin{minipage}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/trussOptimizationStart}
        Startentwurf und imperfektes System
    \end{minipage}% <- sonst wird hier ein Leerzeichen eingefügt
    \hspace{0.8cm}
    \begin{minipage}[t]{0.45\linewidth}
        \centering
        \includegraphics[width=\linewidth]{pictures/section_2/formoptimierung/trussOptimizationFinished}
     Optimiertes- und imperfektes Tragwerk  
    \end{minipage}    
    \caption[Optimierung eines Fachwerkbogens mit BOfem]{Optimierung eines Fachwerkbogens mit BOfem}
\hrule
\vspace{4mm} 
\end{figure} 
Letztlich führt diese Vorgehensweise dazu, dass auch das optimierte Tragwerk hochgradig imperfektionsempfindlich ist. Um dem entgegenzuwirken sollte die Ermittlung der Knickfigur in jeder Iteration vollzogen und aufaddiert werden, anstatt die des Startentwurfs fortlaufend zu nutzen (für genaueres sei an dieser Stelle auf \cite{MatthiasBaitsch.2003} verwiesen).
\newpage



\subsection{Mathematisches Optimierungsproblem}
Da die Strukturoptimierung eine Disziplin der mathematischen Optimierung darstellt, wird im folgenden Unterkapitel das der Optimierung unterliegende mathematische Problem erläutert. Prinzipiell untergliedert sich die mathematische Optimierung in eine beschränkte- und eine unbeschränkte Optimierung. Die unbeschränkte Optimierung hat in der Strukturoptimierung den Nachteil, dass parameterbezogenen Grenzen (Querschnittsabmessungen, ...) oder auch materialorientierte Vorgaben (zulässige Spannungen, ...) nicht berücksichtigt werden können. Aufgrund der Problematik ist es sinnvoll, den Lösungsraum ($S = \mathbb{R}^n$) mit Hilfe von Nebenbedingungen  einzuschränken ($S \subseteq \mathbb{R}^n$) und ein beschränktes Optimierungsproblem zu formulieren. Folglich ergibt sich das Optimierungsproblem
\begin{align}
\min\limits_{\textbf{x}} f_{0}(\textbf{x}) \text{\hspace{1.5mm} mit \hspace{1.5mm}}{\textbf{x} \in S\subseteq\mathbb{R}^{n}} 
\end{align}
mit dem Raum der zulässigen Lösungen
\begin{align}
S=\{\textbf{x} \in \mathbb{R}^{n}\hspace{0.8mm}\vert\hspace{0.8mm} \textbf{x}_{u} \leq \textbf{x} \leq \textbf{x}_{o},\hspace{0.8mm}f_{i}(\textbf{x})\leq 0,\hspace{0.8mm}i = 1,...,m\} \text{.}
\end{align}
Der in der Optimierung zu variierende Entwurfsvektor wird mit \textbf{x} bezeichnet und besitzt n Entwurfsvariablen $x_i,\hspace{2mm} \forall\hspace{1mm}i \in  \{1,...,n\}$.  Die systematische Modifikation der Entwurfsvariablen führt dazu, dass die Zielfunktion $f_{0}(\textbf{x}):\mathbb{R}^{n} \rightarrow \mathbb{R}$ im Optimierungsprozess minimiert wird. Direkte Grenzwerte für den Entwurfsvektor bilden hierbei die expliziten Nebenbedingungen (häufig auch Variablenschranken genannt) $\textbf{x}_{u}$ und $\textbf{x}_{o}$. Zusätzlich werden impliziten Nebenbedingungen (Restriktionsfunktionen) $f_{i}(\textbf{x})\leq 0 $  in der Optimierung berücksichtigt, wobei die Restriktionsfunktionen $f_{i}(\textbf{x}):\mathbb{R}^{n} \rightarrow \mathbb{R},\hspace{2mm} \forall\hspace{1mm}i \in  \{1,...,m\}$ gegenüber der Variablenschranken gezielt ausgewertet werden müssen. 


\subsection{Konventionelle Ansätze}
Wie in Abschnitt 2.1 erwähnt, ist das Ziel der konventionellen Topologieoptimierung mithilfe der Reduktion der Verzerrungsenergie eine Maximierung der Systemsteifigkeit zu bewerkstelligen. Unter der Annahme eines linear elastischen Materials und einer FE-Diskretisierung des Kontinuums ergibt sich die konkrete Zielfunktion 
\begin{align}
f_{0}(\textbf{x}) &= \dfrac{1}{2} * \textbf{u}^T(\textbf{x}) \hspace{1mm} \textbf{K}(\textbf{x}) \hspace{1mm} \textbf{u}(\textbf{x})\\
&= \dfrac{1}{2} * \sum_{i \in N_e}\textbf{u}_i^T(x_i) \hspace{1mm} * \rho(x_i)* \textbf{k}_0 \hspace{1mm} \textbf{u}_i(x_i)
\end{align}
aus der zusätzlichen Betrachtung der abstrahierten Darstellung (siehe Abschnitt 2.2). Während $\textbf{u}(\textbf{x})$ den Verschiebungsvektor der zu optimierenden Struktur bezeichnet, wird mit $\textbf{K}(\textbf{x})$ die über den Entwurfsvektor gesteuerte globale Steifigkeitsmatrix betitelt. Infolge der  Finiten-Element-Grundgleichung $\textbf{K}\textbf{u} = \textbf{f}$ besitzen beide Komponente eine Abhängigkeit in Hinsicht des Entwurfsvektors $\textbf{x}$. Die direkte Beziehung zwischen dem Entwurfsvektor $\textbf{x}$ und der Steifigkeitsmatrix $\textbf{K}(\textbf{x})$ folgt aus der Grundidee, die Entwurfsvariablen $x_i,\hspace{2mm} \forall\hspace{1mm}i \in  \{1,...,N_e\}$  an einer Dichtefunktion $\rho(x_i) : \mathbb{D} \rightarrow \mathbb{R}$ zu übergeben und während der Assemblierung (2.4) mit der Elementsteifigkeitsmatrix $\mathbf{k_0}$ zu multiplizieren. Die Definitionsmenge der dazugehörigen Dichtefunktion $\rho(x_i)$
\begin{align}
\mathbb{D}=\{x_i \in \mathbb{R}\hspace{0.8mm}\vert\hspace{0.8mm} 0 \leq x_i \leq 1\}\hspace{0.8mm}
\end{align}
wird dabei so festgelegt, dass der Wert $x=1.0$ ein Vollmaterial definiert, während $x=0$ ein Loch beschreibt. Schließlich folgt die Sensitivität der Zielfunktion durch die analytische Differenzierung der Verzerrungsenergie
\begin{align}
\dfrac{\partial f_0}{\partial x_i} = -\dfrac{1}{2} * \sum_{i \in N_e}\textbf{u}_i^T(x_i) \hspace{1mm} * \dfrac{\partial \rho}{\partial x_i}* \textbf{k}_0 \hspace{1mm} \textbf{u}_i(x_i)\text{.}
\end{align}
Eine ausführliche Herleitung von (2.6) kann dem Anhang (A.1 Sensitivitätsanalyse) entnommen werden. Um die numerische Stabilität während der Optimierung aufrecht zu erhalten ist es notwendig, die partielle Differenzierbarkeit der Zielfunktion zu gewährleisten. Gerade Strukturen in denen ein Loch modelliert wird oder Optimierungsschritte die vollständig das Material einzelner Elemente entfernen, führen dazu, dass die partielle Differenzierbarkeit der Zielfunktion verschwindet, da der Entwurfsvektor Einträge von null besitzt. Um die Optimierung dennoch zu realisieren, ist es hilfreich die Variablenschranken 
\begin{align}
\textbf{x}_u = 1*10^{-5} \hspace{2mm} \wedge \hspace{2mm} \textbf{x}_o = 1.0
\end{align}
einzuführen. In Hinblick auf die Zielfunktion (2.3) ist es sinnvoll eine weitere Eigenschaft zu begutachten. Die Verzerrungsenergie des Systems wird minimal, wenn der resultierende Verschiebungsvektor $\textbf{u}(\textbf{x})$ minimale Einträge besitzt, also die Steifigkeitsmatrix \textbf{K}(\textbf{x}) aus maximalen Komponenten besteht. Für den Fall, dass ein Kontinuum einzig mit Vollmaterial-Elementen diskretisiert wird, ist es aktuell nicht möglich, die gespeicherte Verzerrungsenergie durch eine Modifikation der Entwurfsvariablen zu reduzieren. Eben diese Überlegung motiviert zur Einführung einer Restriktionsfunktion
\begin{align}
f_1(\textbf{x}) = \dfrac{V(\textbf{x})}{V_0} - \varrho = 0.
\end{align}
Die Gleichung (2.8) basiert auf der Idee, dass ein vorgegebenes Verhältnis $\varrho$ zwischen dem, in jeder Iteration ermittelten Strukturvolumen 
\begin{align}
V(\textbf{x}) = \sum_{i \in N_e} x_i * v_{0i} 
\end{align}
und dem Vollmaterial-Strukturvolumen $V_0$ in der Optimierung eingehalten werden muss, um ein daraus resultierendes Zielvolumen zu erhalten. Wie in (2.9) zu sehen, wird zur Ermittlung des i-ten Strukturvolumens, der jeweilige Vektoreintrag mit dem dazugehörigen Grundvolumen $v_{0i}$ multipliziert und schließlich über alle Elemente $N_e$ summiert. 

\subsubsection{Finite Element-Methode}

Die Tragwerkswerksanalyse mithilfe der Methode der Finiten Elementen erfordert eine Diskretisierung des Kontinuums. Aufgrund der Möglichkeit Hohlräume als minimale Entwurfsvektoreinträge zu interpretieren ergibt sich folglich eine vereinfachte Modellgestaltung. Der Bauraum wird grundlegend als Rechteckumgebung, existierend aus gleichgroßen, 4-knotigen Elementen angesehen (vgl. Abbildung 2.5). Das hat den Vorteil, dass die dazugehörige Elementssteifigkeitsmatrix vor der Integration nicht aus dem physikalischen- in den isoparametrischen Raum übergeführt werden muss, sondern direkt analytisch integriert werden kann. Des Weiteren führt die einmalige, analytische Integration zu einer erheblichen Effizienzsteigerung gegenüber, der im Optimierungsprozess wiederholten, typischerweise vollzogenen Gauß-Quadratur.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.58\textwidth]{pictures/section_2/fem/discretization}
	 	
	\caption{Diskretisiertes Kontinuum}
\end{figure}
\vspace{-2mm} 
\hrule	

\vspace{0.5cm}
\subsubsection{Formulierung der Materialdichte}
Wie in Abschnitt 2.3 bereits angemerkt, existiert eine Abbildung $\rho(x)$, dessen Zuordnungsvorschrift die Elementsdichte des isotropen,  homogenen Materials anhand der Entwurfsvariablen $x_i$ ermittelt. Letztlich wird die Zuordnungsvorschrift so formuliert, dass die kontinuierliche Variation der Entwurfsvariablen für Werte zwischen null und eins bestraft wird. Somit ist es möglich, den für die optimale Struktur signifikanten Randbereich der Elementsdichte ($\rho_i = 1.0$ oder $\rho_i \approx 0$) ableitend aus den optimalen Entwurfsvektor zu erhalten. Da dieser Ansatz fundamental in der Topologieoptimierung ist, haben sich viele Wissenschaftler das Ziel gesetzt eine möglichst effiziente Formulierung der Dichtefunktion zu finden. Nachfolgend werden einige qualitative und repräsentante Varianten der Dichtefunktion vorgestellt. Die Definitionsmenge der jeweiligen Abbildungen ist gemäß (2.4) definiert und wird nicht wiederholt notiert.\\[0.5em]   

\textbf{SIMP} (\textit{Solid Isotropic Material with Penalization of intermediate densities})\\
Der SIMP-Ansatz (siehe \cite{Bendsoe_matDistr}, \cite{Bendse.1999} oder auch \cite{simp}) ist wohl der bis heute am weitesten verbreitete Ansatz zur Berechnung der Materialdichte
\begin{align}
\rho(x) = x^p * E_0
\end{align} 
und der analytischen Differenzierung
\begin{align}
\dfrac{\partial \rho}{\partial x} = p * x^{p-1} * E_0.  
\end{align} 
Der in der Gleichung (2.7 und 2.8) enthaltene Penalty-Faktor $p$, dient als Werkzeug zur direkten Bestrafung von Zwischenwerten der Entwurfsvariablen, die der Vollständigkeit halber anschließend mit den E-Modul des isotropen Vollmaterials $E_0$ (Standardmäßig $E_0=1.0$) multipliziert  werden. Während die Materialdichte für einen Wert von $p=1$ linear zugewiesen wird, steigt die Nichtlinearität der Zuweisung und somit die scharfe 0-1-Verteilung mit größer werdenden Penalty-Faktor $p$ (vgl. Abbildung 2.6). Grundsätzlich gilt die Idee, den Strafexponenten im Optimierungsprozess schrittweise zu steigern um eine scharfe Dichteverteiltung zu erzielen. Allerdings kann dies zu den nicht-vernachlässigbaren Problem führen, dass mit einem größer werdendem Penalty-Faktor die Oszillation der Zielfunktion im Optimum zunimmt und infolgedessen ein nichtkonvexes Problem entsteht. Dies motiviert, wie in \cite{Bendse.1999} erläuert, dazu den Bestrafungsexponenten $p$ hinsichtlich der Querkontraktionszahl $v_0$ zu bestimmen. Folglich ergibt sich für $v_0 = 1/3$ ein zielführender Penalty-Faktor von $p=3$ zur Bestrafung der Zwischenwerte bei einer bestehenden Konvexität der Zielfunktion.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.73\textwidth]{pictures/section_2/material_distribution/simp}
	 	
	\caption{SIMP - Einfluss des Penalty-Faktors}
\end{figure}
\vspace{-2mm} 
\hrule	

\vspace{1.25cm} 

\textbf{RAMP} (\textit{Rational Approximation of Material Properties})\\
Eine weitere Formulierung der Dichtefunktion 
\begin{align}
\rho(x) = \dfrac{x}{p * (1-x)+1}*E_0
\end{align}
und der Differentation
\begin{align}
\dfrac{\partial \rho}{\partial x} = \dfrac{p+1}{(p*(x-1)-1)^2}*E_0
\end{align}
ergibt sich aus den RAMP-Ansatz nach \cite{StolpeM.2001}. Der große Vorteil des RAMP-Ansatzes gegenüber SIMP liegt darin, dass der Grenzwert der Ableitung für kleine Entwurfsvariablen nicht gegen null strebt. Demzufolge bildet dieser Ansatz eine strikt konvexe Dichtefunktion, mit dessen Hilfe eine effizientere Optimierung gegenüber der SIMP-Methode realisiert wird. Der zielführende Penalty-Faktor entspricht mit diesem Ansatz $p=5$ und führt gegenüber des SIMP-Wertes $p=3$, wie in Abbildung 2.7 illustriert, zu einer schärferen Dichteverteilung im oberen Bereich.\\[0.5em]

\hrule
\begin{figure}[h]
	\centering
	 	\includegraphics[width=0.73\textwidth]{pictures/section_2/material_distribution/rampVSsimp}
	 	
	\caption{Dichteverteilung: RAMP vs. SIMP}
\end{figure}
\vspace{-2mm} 
\hrule	

\vspace{1.25cm}

%dieser Stelle auf \cite{Bendsoe_matDistr}, \cite{StolpeM.2001} und auch \cite{Ferrer.2019} verwiesen. Letztlich handelt es sich bei den genannten

\subsection{Optimalitätskriterienverfahren}
Obwohl der aktuelle Stand der Wissenschaft eine Vielzahl an mächtige Optimierungsverfahren zur Verfügung stellt, unterliegt die Topologieoptimierung in den meisten Fällen, nach wie vor, dem Optimalitätskriterienverfahren (\textit{Optimality Criteria Method}).  Gerade der kompakte Implementieraufwand bei einer ausreichend effizienten Konvergenzgeschwindkeit ist einer der wesentlichen Gründe warum dieses Verfahren, basierend auf den Arbeiten von \cite{DCOC-Part-1}, \cite{DCOC-Part-2}, \cite{Hassani} und schließlich erweitert durch \cite{sigmund.}, fortlaufend Anwendung in der Topologieoptimierung findet.  Unter der Berücksichtigung der in Abschnitt 2.3 erläuterten Ansätzen zur Optimierung, erfolgt die dem OC-Verfahren charakterisierende Lagrange-Funktion
\begin{align}
\mathcal{L}(\textbf{x}) = f_0(\textbf{x}) + \lambda * f_1(\textbf{x}) + \alpha * \left(\textbf{x}_u - \textbf{x} \right) + \beta * \left(\textbf{x} - \textbf{x}_o \right).
\end{align}
Zur kompakten Schreibweise wird in Anlehnung zu \cite{sigmund.} vorausgesetzt, dass die Gleichgewichtsbedingungen ohnehin durch die Lösung des FE-Problems erfüllt sind und infolgedessen in der Lagrange-Funktion nicht aufgeführt werden. Die Stationarität der Lagrange-Funktion erfordert das verschwinden der partiellen Ableitungen von (2.11) nach den Entwurfsvariablen $x_i$ in einem Extremum 
\begin{align}
\dfrac{\partial \mathcal{L}}{\partial x_i} = \dfrac{\partial f_0}{\partial x_i} + \lambda * \dfrac{\partial f_1}{\partial x_i} - \alpha + \beta = 0.
\end{align}
Eine zusatzliche Betrachtung der Karush-Kuhn-Tucker-Bedingungen (\textit{\textit{KKT-Conditions}}), also die Inaktivität von Ungleicheitsnebenbedingungen in einem lokalen Extrempunkt, gibt uns das Optimalitätskriterium
\begin{align}
\dfrac{\partial f_0}{\partial x_i} + \lambda * \dfrac{\partial f_1}{\partial x_i} = 0.
\end{align} 
Durch eine Umformung von (2.13) und eine weitere Voraussetzung, dass für (2.14) im Optimum $B_i^\ast = 1.0$ gelten muss, folgt der Updatefaktor
\begin{align}
B_i = \dfrac{-\dfrac{\partial f_0}{\partial x_i}}{\lambda * \dfrac{\partial f_1}{\partial x_i}}
\end{align}
mit dem, in einem Bisektionsverfahren zu ermittelnden Lagrange-Multiplikator $\lambda$.
Typischerweise verwendet das OC-Verfahren zur Ermittlung von $x_i^{j+1},\hspace{2mm}\forall\hspace{1mm}j \in  \{1,...,k\}$ mit $k=\text{Anzahl der Iterationen}$, ein heuristisches Update-Schema. Zur Eingrenzung der Iterationsschrittweite und Aufrechterhaltung der numerischen Stabilität während der Optimierung, wird ein move-limit $m=0.2$ und ein Dämpfungsexponent $\eta=0.5$ eingeführt. Die Optimierung wird bis zur Erfüllung, des in \cite{.e} und \cite{.d} verwendeten Abbruchkriteriums 
\begin{align}
\mid x_{max}^{j+1}-x_{max}^{j} \mid < \delta_{min}
\end{align} 
vollzogen. Letztlich wird die Iteration beendet, sobald die Differenz zwischen den, aus dem heuristischen Update erhaltenen größten Entwurfsvektoreintrag $x_{max}^{j+1}$ zum vorherigen Schritt $x_{max}^{j}$ eine bestimmte Differenz $\delta_{min} = 0.01$ unterschreitet. Das Kriterium nutzt die Eigenschaft, dass ein optimiertes Tragwerk über eine scharfe 0-1 Verteilung verfügt, also im Konvergenzbereich keine signifikanten Veränderungen entstehen werden, da die Maximaleinträge ohnehin gegen $x_i = 1.0$ streben und folglich eine Steigerung nicht möglich ist. Das OC-Verfahren besitzt demnach zwei Iterationsschleifen (siehe Abbildung 2.8). Die innere Schleife dient zur Ermittlung des Lagrangemultiplikators und zur Bestimmung des Entwurfsvektors.\\[0.5em].

\hrule
\begin{figure}[h]
	\centering 	\includegraphics[angle = 90, width=0.55\textwidth]{pictures/section_2/topologieoptimierung/flowchart_OC}	 	
	\caption{Flowchart: OC-Verfahren}
\end{figure}
\vspace{-2mm} 
\hrule
\vspace{4mm}

Tatsächlich dient der Lagrangemultiplikator nicht wie in den KKT-Bedingungen definiert zur Auffindung des Punktes in dem eine Kollinearität zwischen den Gradienten der Zielfunktion und dem der Nebenbedingungen herrscht, sondern zur Bestimmung einer Größe, um den Updatefaktor aus (2.14) zu skallieren. Der Lagrangemultiplikator wird unter Anwendung eines Bisektionsverfahrens solange variiert, bis der aus dem heuristischen Update-Schema (vgl. Abbildung 2.9) resultierende Entwurfsvektor das Zielvolumen bildet. In der äußeren Schleife wird  das Abbruchkriterium (siehe Gleichung 2.15) überprüft, die Steifigkeitsmatrix in Abhängigkeit des Entwurfsvektors gebildet und die Zielfunktion für den aktuellen Iterationsschritt berechnet.
\begin{figure}[!htbp]
  \centering
  \begin{struktogramm}(162,67)
    \assign[25]{Parameter:\newline $x_iTry = x_i^j*B_i^\eta$ \hspace{10mm} $upper=min\begin{cases}x_o\\x_i^j+m \end{cases}$ \hspace{5mm} $lower=max\begin{cases}x_u\\x_i^j-m \end{cases}$}
     \ifthenelse[17]{1}{2}{if \hspace{1mm} $x_iTry \leq lower$}{\sTrue}{\sFalse}
      \assign[27]{$x_i^{j+1} = lower$}
    \change
       \ifthenelse[15]{1}{1}{if \hspace{1mm} $x_iTry \geq upper$}{\sTrue}{\sFalse}
      \assign[12]{$x_i^{j+1} = upper$}
    \change
      \assign[12]{$x_i^{j+1} = x_iTry$}
    \ifend
    \ifend    
    \end{struktogramm}
   	\caption{Heuristisches Update-Schema} 	
\end{figure}
\newpage



