# Thesis Template
This is a LaTeX template for theses.

## How to
This document provides an overview over the template's functionality.

## Setting up the coverpage
You can change the titlepage's information by filling in your personal information as well as your thesis' title into `titleInfo.tex`.

## Content
You may either put all your content into `content/content.tex` or split your content into several `.tex` files and include them into `content/content.tex` using `\input{filename.tex}`.